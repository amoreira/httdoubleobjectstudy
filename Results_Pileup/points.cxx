#include "TCanvas.h"
#include "TGraphErrors.h"
#include "TLegend.h"
#include "TAxis.h"

#include <iostream>
#include <fstream>
#include <dirent.h> //for directory
#include <string>
#include <vector>
#include <algorithm> //upper_bond

#define CUT_DIGITS 4

typedef unsigned int UI ;

double range[2] = { 0. , 250. };

const string input_name[2] = {"average_values",".txt"} ;
const string directory = "./" ;

void startGraphs( TGraphErrors* g1 , TGraphErrors* g2 , TLegend* leg){    
  g1->SetMarkerStyle(kFullDotLarge);
  g2->SetMarkerStyle(kFullDotLarge);
  g1->SetMarkerColor(kBlue);
  g2->SetMarkerColor(kRed);
  g1->GetXaxis()->SetTitle("#mu");
  g1->GetYaxis()->SetTitle("#epsilon");
  g1->GetXaxis()->SetLimits(range[0],range[1]);
  g1->GetYaxis()->SetRangeUser(0.,1.1);

  leg->SetHeader("Legend","C");
  leg->AddEntry(g1,"Tracks","P") ;
  leg->AddEntry(g2,"Smeared Tracks","P") ;
}

//Get files of directory
bool getdir (string dir , vector<string> &files){
 
  DIR *dp;
  struct dirent *dirp;
  string aux ;
  
  //Check if it can open directory
  if((dp  = opendir(dir.c_str())) == NULL) {
    std::cout << "Could not open directory\n" << std::endl;
    return false;
  }
  
  //Get files
  while ((dirp = readdir(dp)) != NULL) {
    aux = string(dirp->d_name) ;
    
    //Check if it is wanted files
    if ( aux.length() >= input_name[0].length()+input_name[1].length() && 
	 !aux.compare(aux.length()-input_name[1].length(),input_name[1].length(),input_name[1]) &&
	 !aux.compare(0,input_name[0].length(),input_name[0]))
      files.push_back(aux);
  }
  closedir(dp);
  
  return true ;
}

class SmallContainer{

  const UI NB2 ;

  std::vector<string> *fileNames ;
  std::vector<UI> pileupValue ;
  std::vector<float> *y1 , *y2 , *e1 , *e2 ;

  void getValuesFromFile(std::ifstream& stream, float*val, std::string& title) ;

public:
  SmallContainer(UI N);
  ~SmallContainer();

  bool add(UI aveX , std::ifstream& stream) ;
  void copyXvalues( float* n );
  void copyYvalues( float* n , UI ind , std::string var );
  void show();

  UI size(){ return pileupValue.size() ; }
  std::string* GetTitle(UI var){ return &fileNames[var][0] ; }
};

class Container{
  std::vector<SmallContainer*> cont ;
  std::vector<float> dZ ;
  
  const UI NB2 ;

public:

  Container(UI n):NB2(n){} ;
  ~Container();
  
  bool add(float dz , UI aveX , std::ifstream& stream ) ; //Add new data of file
  void copyXvalues(UI ind, float * n){ cont[ind]->copyXvalues(n) ; }
  void copyYvalues( float* n , UI i , UI ind , std::string var ) { cont[i]->copyYvalues(n,ind,var); }
  void show();

  UI size(){ return dZ.size(); }
  UI size(UI i){ return cont[i]->size();}
  float getDZ(UI i){ return dZ.at(i); }
  std::string* GetTitle(UI ind , UI var){ return cont[ind]->GetTitle(var) ; }
};

void points(){
  
  std::string* title ;
  std::vector<std::string> files ;
  UI NB2 , NB4, aveX, i , j,  aux ; 
  float DZ , *x , *y1 , *y2 , *e1 , *e2 ;

  std::string aux1 , aux2 ;
  
  std::ifstream fileAverage;
  Container *container ;
  
  //Canvas for plot display
  TCanvas *c1 = new TCanvas("c1","Average points of the efficiency with pileup",800,500);
  
  //Create graphs with uncertanty
  TGraphErrors *g1, *g2 ;  

  //Legend
  TLegend *leg = new TLegend(0.75,0.7,0.98,0.45);
  
  //Get files
  if ( !getdir(directory,files) ) return ; //files contain the names of the files to use
  
  //Get info of files
  for ( i = 0 ; i < files.size() ; i++ ){
    fileAverage.open(files[i].c_str()) ;

    //Check number of branches
    fileAverage >> aux ; 
    if ( !i ){ 
      NB2 = aux ;
      container = new Container(NB2) ;
    }
    else if ( NB2 != aux ){
      std::cout<< "FATAL ERROR. NUMBER OF BRANCHES NOT EQUAL TO EACH FILE\n" ;
      return ;
    }
    
    fileAverage >> DZ >> aveX ;
    if ( !container->add(DZ, aveX , fileAverage) )
      std::cout << "Repeated pair dz-pileup. Data ignored for file " << files[i] << std::endl ;
      
    fileAverage.close();
    
  }
  
  NB4 = NB2/2 ;
  container->show();
  
  //All Information gathered at this point
  aux = container->size(0) ;
  x = new float[container->size(0)] ;
  y1 = new float[container->size(0)] ;
  e1 = new float[container->size(0)] ;
  y2 = new float[container->size(0)] ;
  e2 = new float[container->size(0)] ;
  
  for( i = 0 ; i < container->size() ; i++ ){
    if ( aux != container->size(i) ){ //Redefine dimension
      aux = container->size(i) ;
      delete[] x ; x = new float[aux] ;
      delete[] y1 ; y1 = new float[container->size(i)] ;
      delete[] e1 ; e1 = new float[container->size(i)] ;
      delete[] y2 ; y2 = new float[container->size(i)] ;
      delete[] e2 ; e2 = new float[container->size(i)] ;
    }
    
    //Get new values
    container->copyXvalues(i,x) ;

    aux1 = to_string(container->getDZ(i));
    aux2 = aux1.substr(0,aux1.size()-CUT_DIGITS) ; 

    for ( j = 0 ; j < NB2 ; j++ ){
      container->copyYvalues(y1,i,j,"y1") ;
      container->copyYvalues(e1,i,j,"e1") ;
      container->copyYvalues(y2,i,j,"y2") ;
      container->copyYvalues(e2,i,j,"e2") ;
      title = container->GetTitle(i,j) ;
      
      g1 = new TGraphErrors(aux,x,y1,0,e1) ;
      g2 = new TGraphErrors(aux,x,y2,0,e2) ;
      
      startGraphs(g1,g2,leg); //I couldn't find something to change only the points
      g1->SetTitle((*title).c_str()) ;

      g1->Draw("AP");
      g2->Draw("P");
      leg->Draw();
      c1->Update();

      c1->SaveAs(string("Points"+((!j%4)?string(""):string(1,char(pow(2,j%4)+'0')))+((j<NB4)?"-":"-Truth")+aux2+".png").c_str());

      leg->Clear();
      delete g1 ; delete g2 ;      
    }
  }
  
  delete[] x ;
  delete[] y1 ; delete[] e1 ;
  delete[] y2 ; delete[] e2 ;
  
  delete leg ;
  delete container ;
  delete c1 ;
}

//----SmallContainer----
SmallContainer::SmallContainer(UI n):NB2(n){
  fileNames = new std::vector<string> [NB2] ;
  y1 = new std::vector<float> [NB2] ;
  e1 = new std::vector<float> [NB2] ;
  y2 = new std::vector<float> [NB2] ;
  e2 = new std::vector<float> [NB2] ;
}

SmallContainer::~SmallContainer(){
  delete[] fileNames ;
  delete[] y1 ; delete[] y2 ;
  delete[] e1 ; delete[] e2 ;
}

void SmallContainer::getValuesFromFile(std::ifstream& stream, float*val, std::string& title){
  getline(stream,title) ; //To pass to next line
  getline(stream,title) ;
  stream >> val[0] >> val[1] >> val[2] >> val[3] ;
}

bool SmallContainer::add( UI aveX , std::ifstream& stream ){
  UI ind , i ;
  std::vector<UI>::iterator pos ;
  
  pos = std::upper_bound(pileupValue.begin(),pileupValue.end(),aveX); //Get pos for insertion

  std::string title ;
  float val[4] ;
  
  //If value already exists -> Error, it should only be one
  if( pos != pileupValue.begin() && aveX == *(pos-1) ) 
    return false ;

  ind = pos-pileupValue.begin() ;
 
  pileupValue.insert(pos,aveX);
  
  //It doesn't exist
  for( i = 0 ; i < NB2 ; i++ ){
    
    //Creates new memory for new data
    getValuesFromFile(stream, val, title) ;
    fileNames[i].insert(fileNames[i].begin()+ind,title) ;
    y1[i].insert(y1[i].begin()+ind,val[0]) ;
    e1[i].insert(e1[i].begin()+ind,val[1]) ;
    y2[i].insert(y2[i].begin()+ind,val[2]) ;
    e2[i].insert(e2[i].begin()+ind,val[3]) ;
    
  }
  
  return true ;
}

void SmallContainer::copyXvalues(float * n){
  for ( UI i = 0 ; i < size() ; i++ )
    n[i] = pileupValue[i] ;
}

void SmallContainer::copyYvalues( float* n , UI ind , std::string var ){
  if ( !var.compare("y1") ) for ( UI i = 0 ; i < size() ; i++ ) n[i] = y1[ind][i] ;
  else if ( !var.compare("e1") ) for ( UI i = 0 ; i < size() ; i++ ) n[i] = e1[ind][i] ;
  else if ( !var.compare("y2") ) for ( UI i = 0 ; i < size() ; i++ ) n[i] = y2[ind][i] ;  
  else if ( !var.compare("e2") ) for ( UI i = 0 ; i < size() ; i++ ) n[i] = e2[ind][i] ;
  else std::cout << "Error occorred in function copyYvalues()\n" ; 
}

void SmallContainer::show(){
  
  for ( UI i = 0 ; i < size() ; i++ ){
    std::cout << pileupValue[i] ;
    for ( UI j = 0 ; j < NB2 ; j++ )
      std::cout << fileNames[j][i] << std::endl << y1[j][i] << ' ' << e1[j][i] << ' ' << y2[j][i] << ' ' << e2[j][i] << std::endl ;
    std::cout << std::endl ;
  }
  
}

//----Container----
Container::~Container(){
  for ( UI i = 0 ; i < cont.size() ; i++ )
    delete cont[i] ;
}

bool Container::add(float dz , UI aveX , std::ifstream& stream ){
  UI ind ;
  std::vector<float>::iterator pos ;
  pos = std::upper_bound(dZ.begin(),dZ.end(),dz); //Get pos for insertion
  
  //If value already exists
  if( pos != dZ.begin() && dz == *(pos-1) ) 
    ind = pos-1-dZ.begin() ;
 
  else{//It doesn't exist
    ind = pos-dZ.begin() ;
    
    //Creates new memory for new data
    dZ.insert(pos,dz);
    cont.insert(cont.begin()+ind,0) ;
    cont[ind] = new SmallContainer(NB2);
  }

  //Add info to the containers
  return cont[ind]->add(aveX,stream) ;
  
}

void Container::show(){
  for( UI i = 0 ; i < size() ; i++ ){
    std::cout << "dZ = " << dZ[i] << std::endl ;
    cont[i]->show();
  }
}
