#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TGraph.h"
#include "TLegend.h"
#include "TAxis.h"

#include <iostream>
#include <fstream>
#include <string>

#include <vector>
#include <algorithm> //for upper_bond

#define SHOW_HIST
#define AFTER_JOB 0

#define MARGIN 0.05
#define CUT_DIGITS 4
#define DELTAZ_LIMIT 2
#define SMEARED_DELTAZ_LIMIT 10

typedef unsigned int UI ;

#if AFTER_JOB == 1
const char file[] = "../mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786.testFile.root" ;
#elif AFTER_JOB == 0
const char file[] = "../run/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786.testFile.root" ;
#elif AFTER_JOB == -1 
const char file[] = "/lstore/calo/ev19u055/Results_signalPile_0285/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786.testFile.root" ;
#else
#error Bad AFTER_JOB value
#endif

const string output_name[2]={"average_values",".txt"};

const float DEFAULT_DZ = 0.2 ;
const UI N_BRA = 16  , NB2 = N_BRA/2 , NB4 = NB2/2 ;

const string base_name[2] = {"deltaZ","smearedDeltaZ"} ;
const string change[NB2] = {"","1","2","4","Truth","1Truth","2Truth","4Truth"};
const string intName = "averageInteractionsPerCrossing" ;

//Copy values of an array of floats to an array of doubles
inline void change_points(double* rd,float* nd, UI N ){
  for (UI i = 0 ; i < N ; i++ ) rd[i] = (double)nd[i] ;
}

//Assumes dx = 1 
void add_empty_points(float* x, float*oldY , double* newY ,UI auxN ){
  float x0  ;
  const double dx = 1. ;
  if (auxN) x0 = x[0] ;
  auxN++ ;
  newY[0] = 0. ; newY[auxN] = 0. ;
  
  for( UI i = 1 , j = 0 ; i < auxN ; i++ , j++ ){
    while ( i < auxN && x0 != x[j] ) {newY[i++] = -1. ;  x0+=dx;}
    newY[i] = oldY[j] ;
    x0 += dx ;
  }
}

//Average of x = 200
float x_average(float*x , UI N){
  UI n = N ;
  float sum = 0 ;
  for( UI i = 0 ; i < N ; i++ ){
    if( x[i] < 0 ) n-- ;
    else{
      sum += x[i] ;
    }
  }

  if(n)return sum/(float)n ;
  else {cout << "n=0 for x\n"; return -1. ;}
}

//Average of y 
float y_average(float*x,float*y ,UI N){
  UI n = N ;
  float sum = 0 ;
  for( UI i = 0 ; i < N ; i++ ){
    if( x[i] < 0 ) n-- ;
    else{
      sum += y[i] ;
    }
  }

  if(n)return sum/(float)n ;
  else {cout << "n=0 for x\n"; return -1. ;}
}

//Error Average of y 
float errorY(float*x,float*y , float average,UI N){
  UI n = N ;
  float sum = 0 ;
  for( UI i = 0 ; i < N ; i++ ){
    if( x[i] < 0 ) n-- ;
    else{
      sum += fabsf(average - y[i]) ;
    }
  }

  if(n)return sum/(float)n ;
  else {cout << "n=0 for x\n"; return -1. ;}
}

//There must be a better way to do this with root library, but i din't find it
class BigContainer{
public:
  class MediumContainer ;
private:
  std::vector<MediumContainer*> pileupDeltaZ ; //Array of DeltaZ associated with each pileup value
  std::vector<float> pileupValue ; //pile-up values
  std::vector<float*> pileupEfficiency ; //Final results-first time empty
  
  const UI n_branches ;

public:

  BigContainer(UI N):n_branches(N){};
  ~BigContainer();

  void add(float ni, float* dz); //Add a new entry to Container
  void calculateEffi(float) ; //Calculate efficiencies with given data

  void show();//Auxiliar
  void showEfficiency() ;

  float* getX(){ return &pileupValue[0] ; }
  void getY(float *a, UI ind);

  UI size(){ return pileupValue.size(); }
  
  float min() { return pileupValue.front(); }
  float max() { return pileupValue.back() ; }

};

//Class corresponding to arrays of vectors.
class BigContainer::MediumContainer{
  
  std::vector<float>* deltaZs ;
  
  UI size(UI i){ return deltaZs[i].size(); }

public:

  MediumContainer(UI);
  ~MediumContainer();
  
  void add(UI,float) ;
  float calculateEffi(UI ind,float DZ);
  
  void show() ; //Auxiliar   
};


void pileup(float DZ = DEFAULT_DZ){  
  
  //Create container for storing info
  BigContainer container(N_BRA);

  UI N_EVENT ;
  float treeVal[N_BRA] , pileVal ;
  UI i , j ;
  float averageX, averageY1, averageY2, errorAverageY1, errorAverageY2 ;
  string finalTitle ;
  
  string var_names[N_BRA] ;
  const string aux1 = to_string(DZ) , aux2 = aux1.substr(0,aux1.size()-CUT_DIGITS) , 
    aux3 = " for #Delta Z = " + aux2 + " mm" , aux4 = "-" + aux2 + ".png" ;
  const string title = "Efficiency with pileup " , save = "Pileup" ;
  
  ofstream fileAverage(string(output_name[0]+aux2+output_name[1]).c_str()) ;
  if (!fileAverage) {
    std::cout << "File of output did not open\n" ;
    return ;
  }
  
  //Canvas for plot display
  TCanvas *c1 = new TCanvas("c1","Efficiency in pile-up events",800,500);
  
  //Where is the data
  TFile *_file0 = TFile::Open(file);
  if (!_file0) return ;
  
  //Tree in TFile
  TTree *tree = (TTree*) _file0->Get("eventTree") ;
  if (!tree) return ;
  
  //For the plots
  TH1F *h ;
#ifdef SHOW_HIST
  TH1F *h1 , *h2 ;
#else
  TGraph *g1 = new TGraph(), *g2 = new TGraph();
  
  g1->SetLineColor(kBlue) ;
  g2->SetLineColor(kRed);
  g1->GetXaxis()->SetTitle("#mu");
  g1->GetYaxis()->SetTitle("#epsilon");
  g1->GetYaxis()->SetRangeUser(0.,1.1);
#endif
  
  //Create legend 
  TLegend *leg = new TLegend(0.75,0.7,0.98,0.45);
  leg->SetHeader("Legend","C");  
  
  N_EVENT = tree->GetEntries() ;

  //Obtain names
  for( i = 0 ; i < NB2 ; i++ ){
    var_names[i] = base_name[0]+change[i] ;
    var_names[i+NB2] = base_name[1]+change[i] ;
  }

  //Uses less memory (i believe)
  //Enable necessary branches and gets wanted branches
  tree->SetBranchStatus("*",0); //disable all branches

  tree->SetBranchStatus(intName.c_str(),1);
  tree->SetBranchAddress(intName.c_str(),&pileVal);

  for ( i = 0 ; i < N_BRA; i++ ){
    tree->SetBranchStatus(var_names[i].c_str(),1);
    tree->SetBranchAddress(var_names[i].c_str(),treeVal+i);
  }
      
  //Get values of branches and add them to CONTAINER
  for( i = 0 ; i < N_EVENT ; i++ ){
    tree->GetEntry(i) ; 
    container.add(pileVal,treeVal) ;
  }
  
  container.show();
  
  //Calculate efficiencies444
  container.calculateEffi(DZ);
  
  container.showEfficiency();
  
  //There must be a better way of doing this, but i don't know how
  const float aux = container.max()-container.min() , range[2] = {container.min()-(float)MARGIN*aux,container.max()+(float)MARGIN*aux} ;
  const UI auxp1 = (UI)(aux+1) ;
  float *valuesX = container.getX() ; 
  float *valuesY1 , *valuesY2 ;
  
  averageX = x_average( valuesX , container.size());
  fileAverage << NB2 << ' ' << DZ << ' ' << round(averageX) << std::endl ;
  
  valuesY1 = new float[container.size()];
  valuesY2 = new float[container.size()];
  
#ifdef SHOW_HIST
  double * vY1 , *vY2 ;
  vY1 = new double[auxp1+2] ;
  vY2 = new double[auxp1+2] ;
  
  //Efficiency with pileup
  h1 = new TH1F("h1","",auxp1,container.min()-0.5,container.max()+0.5) ; 
  h2 = new TH1F("h2","",auxp1,container.min()-0.5,container.max()+0.5) ;
  h1->SetLineColor(kBlue);
  h2->SetLineColor(kRed);
  h1->SetXTitle("#mu");
  h1->SetYTitle("#epsilon");
  h1->SetStats(kFALSE);
  h1->GetYaxis()->SetRangeUser(0., 1.1);
  h2->GetYaxis()->SetRangeUser(0., 1.1);
  
  leg->AddEntry(h1,"Tracks","l");
  leg->AddEntry(h2,"Smeared Tracks","l");
#else
  g1->GetXaxis()->SetLimits(range[0],range[1]);
  g1->Set(container.size()) ;
  g2->Set(container.size()) ;
  leg->AddEntry(g1,"Tracks","l");
  leg->AddEntry(g2,"Smeared Tracks","l");

  change_points(g1->GetX(),valuesX,container.size());
  change_points(g2->GetX(),valuesX,container.size());
#endif
  
  //Obtain wanted plots
  //Distribution of pileup
  h = new TH1F("h","Pileup distribution",auxp1,container.min()-0.5,container.max()+0.5) ;
  h->SetLineColor(kBlue);
  h->SetXTitle("#mu");
  h->SetYTitle("Counts");
  h->SetStats(kFALSE); 
  h->SetTitle("Pileup distribution");
  tree->Draw(string(intName+">>h").c_str());
  c1->SaveAs("Distribution_of_pileup.png");
  
  for ( i = 0 ; i < NB2 ; i++ ){
    
    //Get Y values from container
    container.getY(valuesY1,i) ;
    container.getY(valuesY2,i+NB2) ;
    
    finalTitle = title+((i<NB4)?"":"(Truth)")+aux3;
    
#ifdef SHOW_HIST
    h1->SetTitle(finalTitle.c_str()) ;
    add_empty_points(valuesX,valuesY1,vY1,auxp1) ;
    add_empty_points(valuesX,valuesY2,vY2,auxp1) ;
    
    h1->SetContent(vY1);
    h2->SetContent(vY2);

    c1->cd();
    h1->Draw() ;
    c1->cd();
    h2->Draw("SAME") ;
    c1->Update();
#else
    g1->SetTitle(finalTitle.c_str()) ;
    change_points(g1->GetY(),valuesY1,container.size());
    change_points(g2->GetY(),valuesY2,container.size());
    
    g1->Draw("AC*");
    g2->Draw("C*");
#endif
    
    leg->Draw();
    c1->SaveAs(string(save+change[i]+aux4).c_str());
        
    averageY1 = y_average( valuesX , valuesY1 , container.size());
    averageY2 = y_average( valuesX , valuesY2 , container.size());
    errorAverageY1 = errorY( valuesX , valuesY1 , averageY1, container.size());
    errorAverageY2 = errorY( valuesX , valuesY2 , averageY2, container.size());  
    
    fileAverage << finalTitle << std::endl << averageY1 << ' ' << errorAverageY1 << ' ' 
		<< averageY2 << ' ' << errorAverageY2 << std::endl ;  
    
    std::cout << "Average of \"" << finalTitle << '\"' << std::endl 
	      << "x = " << averageX << std::endl 
	      << "Tracks    :\t" << "y = " << averageY1 << "+-" << errorAverageY1 << std::endl
	      << "Smeared T.:\t" << "y = " << averageY2 << "+-" << errorAverageY2 << std::endl ;
    
  }

  
  fileAverage.close();
  
#ifdef SHOW_HIST
  delete[] vY1 ;
  delete[] vY2 ;
#else
  delete g1 ;
  delete g2 ;
#endif
  
  delete[] valuesY1 ;
  delete[] valuesY2 ;  
  _file0->Close("R");

  delete c1 ;
    
}


//----BigContainer----
BigContainer::~BigContainer(){
  for( UI i = 0 ; i < pileupDeltaZ.size() ; i++ ) delete pileupDeltaZ[i] ;
  for( UI i = 0 ; i < pileupEfficiency.size() ; i++ ) delete[] pileupEfficiency[i] ;
}

//Add new value to container
void BigContainer::add(float ni, float* dz){
  
  bool DEFAULT = true ;
  std::vector<float>::iterator pos ;
  UI ind , aux = n_branches/2 ;
  
  ni = round(ni) ;

  //If there is a value different than the default value
  for ( UI i = 0 ; i < n_branches ; i++ ){
    
    if( dz[i] < 0 ) continue ;
    if( (i < aux && dz[i] > DELTAZ_LIMIT ) || ( i >= aux && dz[i] > SMEARED_DELTAZ_LIMIT ) ) continue ;
    
    //First time, finds if exists, and if it doesn't , creates one
    //Get index value
    if (DEFAULT){
      
      DEFAULT = false ;
      pos = std::upper_bound(pileupValue.begin(),pileupValue.end(),ni); //Get pos for insertion
      
      //Check if value already exists
      if( pos != pileupValue.begin() && ni == *(pos-1) ) {
	ind = pos-1-pileupValue.begin() ;
      }
      else{//It doesn't exist

	ind = pos-pileupValue.begin() ;
	pileupValue.insert(pos,ni);
	pileupDeltaZ.insert(pileupDeltaZ.begin()+ind,0) ;
	pileupDeltaZ[ind] = new MediumContainer(n_branches) ;
	
      }
    }
    
    //Insert values of event
    pileupDeltaZ[ind]->add(i,dz[i]) ;
  }
  
}

//Calculate efficiencies
void BigContainer::calculateEffi(float DZ){
  UI i , j ;
  
  //Delete previous results
  for ( i = 0 ; i < pileupEfficiency.size() ; i++ ) 
    delete[] pileupEfficiency[i] ;
  
  //Resize if needed
  if ( pileupEfficiency.size() != size() ) pileupEfficiency.resize(size()) ;
  
  for ( i = 0 ; i < size() ; i++ ){
    pileupEfficiency[i] = new float[n_branches] ; //Creates new memory
    
    for ( j = 0 ; j < n_branches ; j++ ){ 
      pileupEfficiency[i][j] = pileupDeltaZ[i]->calculateEffi(j,DZ) ;
    }
  }
  
}

//Temporary function
void BigContainer::show(){

  for( UI i = 0 ; i < size() ; i++ ){
    std::cout << "pile-up val " << pileupValue[i] << std::endl ;
    //pileupDeltaZ[i]->show();
  }
}

//Temporary function
void BigContainer::showEfficiency(){
  for( UI i = 0 ; i < size() ; i++){
    std::cout << "mu = " << pileupValue[i] << std::endl ;
    for( UI j = 0 ; j < n_branches ; j++ ) 
      std::cout << j << "=>" << pileupEfficiency[i][j] << '\t' ;
    std::cout << std::endl ;
  }
}


/*
  This method may originate problems in case of -1 results.
  I ignored that case because i didn't care
*/

void BigContainer::getY( float* a , UI ind ){
  
  //Assumes that a has the correct size
  for ( UI i = 0 ; i < size() ; i++ )
    a[i] = pileupEfficiency[i][ind] ;
  
}

//----MediumContainer----
BigContainer::MediumContainer::MediumContainer(UI N){
  deltaZs = new std::vector<float>[N] ;
}

BigContainer::MediumContainer::~MediumContainer(){
  delete[] deltaZs ;
}

void BigContainer::MediumContainer::add(UI i,float val){
  std::vector<float>::iterator pos ;
  UI ind ;
  
  pos = std::upper_bound(deltaZs[i].begin(),deltaZs[i].end(),val); //Get pos for insertion
  
  deltaZs[i].insert(pos,val) ;
}

float BigContainer::MediumContainer::calculateEffi(UI ind , float DZ){
  float integral1 = 0 , integral2 , integral , total ;
  UI i = 0 , i2 ;
  
  if ( !size(ind) ) {
    std::cout << "=====> Error, empty contaner. <=====" << std::endl ;
    return -1 ;
  }
  
  while ( i < size(ind) && deltaZs[ind][i] < DZ )  integral1 += deltaZs[ind][i++] ; 
  if ( i < size(ind) && integral1 ){ 
    i2 = i ;
    integral2 = integral1 + deltaZs[ind][i++] ;
    total = integral2 ;

    while ( i < size(ind) ) total += deltaZs[ind][i++] ;

    //Calculate result
    integral = integral1 + (integral2-integral1)*(DZ-deltaZs[ind][i2-1])/(deltaZs[ind][i2]-deltaZs[ind][i2-1]) ;
    return integral/total ;
  }
  else if ( !integral1 ) return 0. ;
  else return 1. ;
}

//Temporary function
void BigContainer::MediumContainer::show(){
  UI a = deltaZs[0].size() ;
  std::cout << a << std::endl ;
  for (UI i=0;i<a;i++) std::cout << i << '-'<<deltaZs[0].at(i) << ' ' ;
  std::cout << std::endl << std::endl ;

  a = deltaZs[NB2].size() ;
  for (UI i=0;i<a;i++) std::cout << deltaZs[NB2].at(i) << ' ' ;
  std::cout << std::endl ;
}

