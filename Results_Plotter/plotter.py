from ROOT import TFile, TH1F, TCanvas, TLegend, TGraph, TTree, gROOT, gStyle
import array
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-f", "--file", dest="fileName", help="file name")
(options, args) = parser.parse_args()


def treeToTH1F(var, tree, binning):
    bins = len(binning)-1
    name = var + "_hist"
    hist = TH1F(name, name, bins, binning)
    tree.Draw(var+">>"+name, var+"!=0")
    return hist

def treeToTH1FCut(var, cut, tree, binning):
    bins = len(binning)-1
    name = var + "_hist"
    hist = TH1F(name, name, bins, binning)
    tree.Draw(var+">>"+name, var+"!=0&&"+cut)
    return hist

def treeToTH1FBins(var, tree, bins, xmin, xmax):
    name = var + "_hist"
    hist = TH1F(name, name, bins, xmin, xmax)
    tree.Draw(var+">>"+name, var+"!=0")
    return hist

def treeToTH1FBinsCut(var, cut, tree, bins, xmin, xmax):
    name = var + "_hist"
    hist = TH1F(name, name, bins, xmin, xmax)
    tree.Draw(var+">>"+name, var+"!=0&&"+cut)
    return hist


def plotEfficiency(name, numerator, denominator, xaxisTitle):
    efficiency = denominator.Clone()
    efficiency.Sumw2()
    efficiency.SetName(name)
    efficiency.SetTitle(name)
    efficiency.Divide(numerator, denominator, 1., 1., "B")
    efficiency.SetXTitle(xaxisTitle)
    efficiency.SetYTitle("Efficiency")
    
    return efficiency

def plotEfficiencyBins(name, numerator, denominator, bins, xmin, xmax):
    efficiency = TH1F(name, name, bins, xmin, xmax)
    efficiency.Sumw2()
    efficiency.Divide(numerator, denominator, 1., 1., "B")
    
    efficiency.SetYTitle("Efficiency")
    return efficiency
#"/afs/cern.ch/user/l/lwilkins/cernBox/Data/serviceTask/user.lwilkins.mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD10.e1564_s3142_s3143_r9586_4_MYSTREAM/output.root"


treeFile = TFile(options.fileName, "READ")
tree = treeFile.Get("eventTree")

#ptBinning = array.array('d', [0,5,10,15,20,25,30,35,40,45,50,55,60,65,70,80,90,110,130,150,175,200])
ptBinning = array.array('d', [10,15,20,30,40,60,100])
etaBinning = array.array('d', [0,0.8,1.37,1.52,2.37,2.47])
leadingTruthPtLabel = "Leading truth p_{T} [GeV]"
leadingOfflinePtLabel = "Leading offline p_{T} [GeV]"
leadingEtaLabel = "Leading truth #eta"
subleadingTruthPtLabel = "Subleading truth p_{T} [GeV]"
subleadingOfflinePtLabel = "Subleading offline p_{T} [GeV]"
subleadingEtaLabel = "Subleading truth #eta"

efficiencyHists = []
ptHists = []
efficiencyOfflineHists = []
## Single electron efficiency hists
leadingTruthPt                  = treeToTH1F("leadingTruthPt", tree, ptBinning)
leadingTruthEta                 = treeToTH1FBins("leadingTruthEta", tree, 30, -3, 3)
leadingTruthEtaStripBin         = treeToTH1F("leadingTruthEta", tree, etaBinning)

truthPt                         = treeToTH1F("truthPt", tree, ptBinning)
truthE                          = treeToTH1F("truthE", tree, ptBinning)
truthEta                        = treeToTH1F("truthEta", tree, etaBinning)
offlinePt                       = treeToTH1F("offlinePt", tree, ptBinning)
offlineE                        = treeToTH1F("offlineE", tree, ptBinning)
offlineEta                      = treeToTH1F("offlineEta", tree, etaBinning)

leadingOfflinePt                = treeToTH1F("leadingOfflinePt", tree, ptBinning)
leadingOfflineEta               = treeToTH1FBins("leadingOfflineEta", tree, 30, -3, 3)
leadingOfflineEtaStripBin       = treeToTH1F("leadingOfflineEta", tree, etaBinning)
leadingOffline_offlinePt        = treeToTH1F("leadingOffline_offlinePt", tree, ptBinning)
leadingOfflineEta               = treeToTH1FBins("leadingOffline_offlineEta", tree, 30, -3, 3)
leadingL0EM10Truth_truthPt      = treeToTH1F("leadingL0EM10Truth_truthPt", tree, ptBinning)
leadingL0EM10Truth_truthEta     = treeToTH1FBins("leadingL0Truth_truthEta", tree, 30, -3, 3)
leadingL0EM20Truth_truthPt      = treeToTH1F("leadingL0EM20Truth_truthPt", tree, ptBinning)
leadingL0EM20Truth_truthEta     = treeToTH1FBins("leadingL0EM20Truth_truthEta", tree, 30, -3, 3)
leadingL0EM10Offline_truthPt    = treeToTH1F("leadingL0EM10Offline_truthPt", tree, ptBinning)
leadingL0EM10Offline_truthEta   = treeToTH1FBins("leadingL0EM10Offline_truthEta", tree, 30, -3, 3)
leadingL0EM20Offline_truthPt    = treeToTH1F("leadingL0EM20Offline_truthPt", tree, ptBinning)
leadingL0EM20Offline_truthEta   = treeToTH1FBins("leadingL0EM20Offline_truthEta", tree, 30, -3, 3)
leadingL0EM10Offline_offlinePt  = treeToTH1F("leadingL0EM10Offline_offlinePt", tree, ptBinning)
leadingL0EM10Offline_offlineEta = treeToTH1FBins("leadingL0EM10Offline_offlineEta", tree, 30, -3, 3)
leadingL0EM20Offline_offlinePt  = treeToTH1F("leadingL0EM20Offline_offlinePt", tree, ptBinning)
leadingL0EM20Offline_offlineEta = treeToTH1FBins("leadingL0EM20Offline_offlineEta", tree,30, -3, 3)

ptHists.append(leadingTruthPt)
ptHists.append(leadingOfflinePt)
#Offline efficiency
leadingOfflineEfficiencyPt  = plotEfficiency("leadingOfflineEfficiencyPt", leadingOfflinePt, leadingTruthPt, leadingTruthPtLabel)
leadingOfflineEfficiencyEta = plotEfficiency("leadingOfflineEfficiencyEta", leadingOfflineEta, leadingTruthEta, leadingEtaLabel)
leadingOfflineEfficiencyEtaA= plotEfficiency("leadingOfflineEfficiencyEtaA", leadingOfflineEtaStripBin, leadingTruthEtaStripBin, leadingEtaLabel)
offlineEfficiencyPt         = plotEfficiency("offlineEfficiency", offlinePt, truthPt, leadingTruthPtLabel)
offlineEfficiencyEta        = plotEfficiency("offlineEfficiencyEta", offlineEta, truthEta, leadingEtaLabel)
offlineEfficiencyE          = plotEfficiency("offlineEffeciencyE", offlineE, truthE, leadingTruthPtLabel)
efficiencyHists.append(leadingOfflineEfficiencyPt)
efficiencyHists.append(leadingOfflineEfficiencyEta)
efficiencyHists.append(leadingOfflineEfficiencyEtaA)


efficiencyHists.append(offlineEfficiencyPt)
efficiencyHists.append(offlineEfficiencyEta)
efficiencyHists.append(offlineEfficiencyE)


# #L0EM10 single - offline
leadingL0EM10OfflineEfficiency_truthPt = plotEfficiency("leadingL0EM10OfflineEfficiency_truthPt", leadingL0EM10Offline_truthPt, leadingOfflinePt, leadingTruthPtLabel)
leadingL0EM10OfflineEfficiency_truthEta = plotEfficiency("leadingL0EM10OfflineEfficiency_truthEta", leadingL0EM10Offline_truthEta, leadingOfflineEta, leadingEtaLabel)
leadingL0EM20OfflineEfficiency_truthPt = plotEfficiency("leadingL0EM20OfflineEfficiency_truthPt", leadingL0EM20Offline_truthPt, leadingOfflinePt, leadingTruthPtLabel)
leadingL0EM20OfflineEfficiency_truthEta = plotEfficiency("leadingL0EM20OfflineEfficiency_truthEta", leadingL0EM20Offline_truthEta, leadingOfflineEta, leadingEtaLabel)
efficiencyHists.append(leadingL0EM10OfflineEfficiency_truthPt)
efficiencyHists.append(leadingL0EM10OfflineEfficiency_truthEta)
efficiencyHists.append(leadingL0EM20OfflineEfficiency_truthPt)
print "is it before"
leadingL0EM10OfflineEfficiency_offlinePt = plotEfficiency("leadingL0EM10OfflineEfficiency_offlinePt", leadingL0EM10Offline_offlinePt, leadingOffline_offlinePt, leadingOfflinePtLabel)
leadingL0EM20OfflineEfficiency_offlinePt = plotEfficiency("leadingL0EM20OfflineEfficiency_offlinePt", leadingL0EM20Offline_offlinePt, leadingOffline_offlinePt, leadingOfflinePtLabel)
efficiencyHists.append(leadingL0EM10OfflineEfficiency_offlinePt)
efficiencyHists.append(leadingL0EM20OfflineEfficiency_offlinePt)
print "or after"
#L0EM10 single - truth
leadingL0EM10TruthEfficiency_truthPt = plotEfficiency("leadingL0EM10TruthEfficiency_truthPt", leadingL0EM10Truth_truthPt, leadingTruthPt, leadingTruthPtLabel)
leadingL0EM10TruthEfficiency_truthEta = plotEfficiency("leadingL0EM10TruthEfficiency_truthEta", leadingL0EM10Truth_truthEta, leadingTruthEta, leadingEtaLabel)
leadingL0EM20TruthEfficiency_truthPt = plotEfficiency("leadingL0EM20TruthEfficiency_truthPt", leadingL0EM20Truth_truthPt, leadingTruthPt, leadingTruthPtLabel)
leadingL0EM20TruthEfficiency_truthEta = plotEfficiency("leadingL0EM20TruthEfficiency_truthEta", leadingL0EM20Truth_truthEta, leadingTruthEta, leadingEtaLabel)
efficiencyHists.append(leadingL0EM10TruthEfficiency_truthPt)
efficiencyHists.append(leadingL0EM10TruthEfficiency_truthEta)
efficiencyHists.append(leadingL0EM20TruthEfficiency_truthPt)
efficiencyHists.append(leadingL0EM20TruthEfficiency_truthEta)
##Double electron efficiency hists

subleadingTruthPt       = treeToTH1F("subleadingTruthPt", tree, ptBinning)
subleadingTruthEta      = treeToTH1FBins("subleadingTruthEta", tree, 30, -3, 3)
subleadingOfflinePt     = treeToTH1F("subleadingOfflinePt", tree, ptBinning)
subleadingOffline_offlinePt     = treeToTH1F("subleadingOffline_offlinePt", tree, ptBinning)
subleadingOfflineEta    = treeToTH1FBins("subleadingOfflineEta", tree, 30, -3, 3)
subleadingOffline_truthPt = treeToTH1F("subleadingOffline_offlinePt", tree, ptBinning)
subleadingL0Truth_truthPt     = treeToTH1F("subleadingL0Truth_truthPt", tree, ptBinning)
subleadingL0Truth_truthEta     = treeToTH1FBins("subleadingL0Truth_truthEta", tree, 30, -3, 3)
subleadingL0Offline_truthPt   = treeToTH1F("subleadingL0Offline_truthPt", tree, ptBinning)
subleadingL0Offline_truthEta  = treeToTH1FBins("subleadingL0Offline_truthEta", tree, 30, -3, 3)
subleadingL0Offline_offlinePt   = treeToTH1F("subleadingL0Offline_offlinePt", tree, ptBinning)
subleadingL0Offline_offlineEta  = treeToTH1FBins("subleadingL0Offline_offlineEta", tree, 30, -3, 3)

subleadingL02EM10Truth_truthPt   = treeToTH1F("subleadingL02EM10Truth_truthPt", tree, ptBinning)
subleadingL02EM10Truth_truthEta  = treeToTH1FBins("subleadingL02EM10Truth_truthEta", tree, 30, -3, 3)


leadingL1L0Truth_truthPt      = treeToTH1F("leadingL1L0Truth_truthPt", tree, ptBinning)
leadingL1L0Truth_truthEta     = treeToTH1FBins("leadingL1L0Truth_truthPt", tree, 30, -3, 3)
leadingL1L0Truth_trackPt      = treeToTH1F("leadingL1L0Truth_trackPt", tree, ptBinning)
leadingL1L0Truth_trackEta     = treeToTH1FBins("leadingL1L0Truth_trackPt", tree, 30, -3, 3)

leadingL1L0Offline_truthPt    = treeToTH1F("leadingL1L0Offline_truthPt", tree, ptBinning)
leadingL1L0Offline_truthEta    = treeToTH1FBins("leadingL1L0Offline_truthEta", tree, 30, -3, 3)
leadingL1L0Offline_trackPt    = treeToTH1F("leadingL1L0Offline_trackPt", tree, ptBinning)
leadingL1L0Offline_trackEta    = treeToTH1FBins("leadingL1L0Offline_trackEta", tree, 30, -3, 3)
leadingL1L0Offline_offlinePt    = treeToTH1F("leadingL1L0Offline_offlinePt", tree, ptBinning)
leadingL1L0Offline_offlineEta    = treeToTH1FBins("leadingL1L0Offline_offlineEta", tree, 30, -3, 3)

subleadingL1L0Truth_truthPt      = treeToTH1F("subleadingL1L0Truth_truthPt", tree, ptBinning)
subleadingL1L0Truth_truthEta     = treeToTH1FBins("subleadingL1L0Truth_truthPt", tree, 30, -3, 3)
subleadingL1L0Truth_trackPt      = treeToTH1F("subleadingL1L0Truth_trackPt", tree, ptBinning)
subleadingL1L0Truth_trackEta     = treeToTH1FBins("subleadingL1L0Truth_trackPt", tree, 30, -3, 3)

subleadingL1L0Offline_truthPt    = treeToTH1F("subleadingL1L0Offline_truthPt", tree, ptBinning)
subleadingL1L0Offline_truthEta    = treeToTH1FBins("subleadingL1L0Offline_truthEta", tree, 30, -3, 3)
subleadingL1L0Offline_trackPt    = treeToTH1F("subleadingL1L0Offline_trackPt", tree, ptBinning)
subleadingL1L0Offline_trackEta    = treeToTH1FBins("subleadingL1L0Offline_trackEta", tree, 30, -3, 3)
subleadingL1L0Offline_offlinePt    = treeToTH1F("subleadingL1L0Offline_offlinePt", tree, ptBinning)
subleadingL1L0Offline_offlineEta    = treeToTH1FBins("subleadingL1L0Offline_offlineEta", tree, 30, -3, 3)
subleadingL02EM10Offline_truthPt    = treeToTH1F("subleadingL02EM10Offline_truthPt", tree, ptBinning)
subleadingL02EM10Offline_truthEta    = treeToTH1FBins("subleadingL02EM10Offline_truthEta", tree, 30, -3, 3)
subleadingL02EM10Offline_offlinePt    = treeToTH1F("subleadingL02EM10Offline_offlinePt", tree, ptBinning)
#Offline efficiency 
subleadingOfflineEfficiencyPt  = plotEfficiency("subleadingOfflineEfficiencyPt", subleadingOfflinePt, subleadingTruthPt, subleadingTruthPtLabel)
subleadingOfflineEfficiencyEta = plotEfficiency("subleadingOfflineEfficiencyEta", subleadingOfflineEta, subleadingTruthEta, subleadingEtaLabel)

efficiencyHists.append(subleadingOfflineEfficiencyPt)
efficiencyHists.append(subleadingOfflineEfficiencyEta)

#L0 efficiency wrt truth
subleadingL0TruthEfficiencyPt = plotEfficiency("subleadingL0TruthEfficiencyPt", subleadingL0Truth_truthPt, subleadingTruthPt, subleadingTruthPtLabel)
subleadingL0TruthEfficiencyEta = plotEfficiency("subleadingL0TruthEfficiencyEta", subleadingL0Truth_truthEta, subleadingTruthEta, subleadingEtaLabel)
subleadingL02EM10TruthEfficiencyPt = plotEfficiency("subleadingL02EM10TruthEfficiencyPt", subleadingL02EM10Truth_truthPt, subleadingTruthPt, subleadingTruthPtLabel)
subleadingL02EM10TruthEfficiencyEta = plotEfficiency("subleadingL02EM10TruthEfficiencyEta", subleadingL02EM10Truth_truthPt, subleadingTruthEta, subleadingEtaLabel)


efficiencyHists.append(subleadingL0TruthEfficiencyPt)
efficiencyHists.append(subleadingL0TruthEfficiencyEta)
efficiencyHists.append(subleadingL02EM10TruthEfficiencyPt)
efficiencyHists.append(subleadingL02EM10TruthEfficiencyEta)

#L0 efficiency wrt offline
subleadingL0OfflineEfficiency_truthPt = plotEfficiency("subleadingL0OfflineEfficiency_truthPt", subleadingL0Offline_truthPt, subleadingOfflinePt, subleadingTruthPtLabel)
subleadingL0OfflineEfficiency_truthEta = plotEfficiency("subleadingL0Offlineefficiency_truthEta", subleadingL0Offline_truthEta, subleadingOfflineEta, subleadingEtaLabel)
subleadingL0OfflineEfficiency_offlinePt = plotEfficiency("subleadingL0OfflineEfficiency_offlinePt", subleadingL0Offline_truthPt, subleadingOfflinePt, subleadingTruthPtLabel)
subleadingL0OfflineEfficiency_offlineEta = plotEfficiency("subleadingL0Offlineefficiency_offlineEta", subleadingL0Offline_truthEta, subleadingOfflineEta, subleadingEtaLabel)
subleadingL02EM10OfflineEfficiency_truthPt = plotEfficiency("subleadingL02EM10OfflineEfficiency_truthPt", subleadingL02EM10Offline_truthPt, subleadingOfflinePt, subleadingTruthPtLabel)
subleadingL02EM10OfflineEfficiency_truthEta = plotEfficiency("subleadingL02EM10Offlineefficiency_truthEta", subleadingL02EM10Offline_truthEta, subleadingOfflineEta, subleadingEtaLabel)
subleadingL02EM10OfflineEfficiency_offlinePt = plotEfficiency("subleadingL02EM10OfflineEfficiency_offlinePt", subleadingL02EM10Offline_offlinePt, subleadingOffline_offlinePt, subleadingOfflinePtLabel)
efficiencyHists.append(subleadingL02EM10OfflineEfficiency_offlinePt)
efficiencyHists.append(subleadingL0OfflineEfficiency_truthPt)
efficiencyHists.append(subleadingL0OfflineEfficiency_truthEta)
efficiencyHists.append(subleadingL02EM10OfflineEfficiency_truthPt)
efficiencyHists.append(subleadingL02EM10OfflineEfficiency_truthEta)

#L1 efficiency wrt L0(truth-matched) - leading pT


# #L1 efficiency wrt L0(truth-matched) - subleading pT
# subleadingL1L0TruthEfficiencyPt = plotEfficiency("subleadingL1L0TruthEfficiencyPt", subleadingL1L0TruthPt, subleadingL0TruthPt, subleadingTruthPtLabel)
# efficiencyHists.append(subleadingL1L0TruthEfficiencyPt)
# #L1 efficiency wrt L0(offline-matched) - subleading pT
# subleadingL1L0OfflineEfficiencyPt = plotEfficiency("subleadingL1L0OfflineEfficiencyPt", subleadingL1L0OfflinePt, subleadingL0OfflinePt, subleadingTruthPtLabel)
# efficiencyHists.append(subleadingL1L0OfflineEfficiencyPt)

canvas = TCanvas("canvas","", 600, 400)

for hists in efficiencyHists:
    canvas.Clear()
    gStyle.SetOptStat(0)
    xmax =  hists.GetBinLowEdge(hists.GetNbinsX()) + hists.GetBinWidth(hists.GetNbinsX())
    xmin = hists.GetBinLowEdge(1) 
    
    titles = ";" + hists.GetXaxis().GetTitle()+";Efficiency"
    canvas.DrawFrame(xmin,0,xmax,1.05, titles)
    hists.Draw("e same")
    canvas.Print(hists.GetName()+".png")


# for hists in ptHists:
#     canvas.Clear()
#     gStyle.SetOptStat(0)
#     canvas.SetLogy(1)
#     xmax =  hists.GetBinLowEdge(hists.GetNbinsX()) + hists.GetBinWidth(hists.GetNbinsX())
#     xmin = hists.GetBinLowEdge(1) 
#     maxbin = 0
#     for i in xrange(1,hists.GetNbinsX()):
#         if hists.GetBinContent(i) > maxbin:
#             maxbin = hists.GetBinContent(i)

#     titles = ";" + hists.GetXaxis().GetTitle()+";pt [GeV]"
#     canvas.DrawFrame(xmin,1,xmax,maxbin*1.1, titles)
#     hists.Draw("same")
#     canvas.Print(hists.GetName()+".png")





# canvas.Clear()

# legend = TLegend(0.7, 0.65,0.85,0.75)
# legend.SetMargin(0.1)

# gStyle.SetOptStat(0)
# xmax =  leadingL0EM10TruthEfficiency_truthPt.GetBinLowEdge(leadingL0EM10TruthEfficiency_truthPt.GetNbinsX()) + leadingL0EM10TruthEfficiency_truthPt.GetBinWidth(leadingL0EM10TruthEfficiency_truthPt.GetNbinsX())
# xmin = leadingL0EM10TruthEfficiency_truthPt.GetBinLowEdge(1) 
    
# titles = ";" + leadingL0EM10TruthEfficiency_truthPt.GetXaxis().GetTitle()+";Efficiency"
# canvas.DrawFrame(xmin,0,xmax,1.05, titles)

# leadingL0EM10TruthEfficiency_truthPt.SetMarkerColor(2)
# leadingL0EM10TruthEfficiency_truthPt.SetMarkerStyle(2)
# leadingL0EM10OfflineEfficiency_truthPt.SetMarkerColor(4)
# leadingL0EM10TruthEfficiency_truthPt.Draw("p e")
# leadingL0EM10OfflineEfficiency_truthPt.Draw("p e same")

# legend.AddEntry(leadingL0EM10TruthEfficiency_truthPt, "EM10 wrt truth", "pL")
# legend.AddEntry(leadingL0EM10OfflineEfficiency_truthPt, "EM10 wrt offline", "pL")
# legend.Draw()
# canvas.Print("leadingL0EM10Efficiency.png")


# canvas.Clear()

# legend = TLegend(0.7, 0.65,0.85,0.75)
# legend.SetMargin(0.1)

# gStyle.SetOptStat(0)
# xmax =  leadingL0EM20TruthEfficiency_truthPt.GetBinLowEdge(leadingL0EM20TruthEfficiency_truthPt.GetNbinsX()) + leadingL0EM20TruthEfficiency_truthPt.GetBinWidth(leadingL0EM20TruthEfficiency_truthPt.GetNbinsX())
# xmin = leadingL0EM20TruthEfficiency_truthPt.GetBinLowEdge(1) 
    
# titles = ";" + leadingL0EM20TruthEfficiency_truthPt.GetXaxis().GetTitle()+";Efficiency"
# canvas.DrawFrame(xmin,0,xmax,1.05, titles)

# leadingL0EM20TruthEfficiency_truthPt.SetMarkerColor(2)
# leadingL0EM20TruthEfficiency_truthPt.SetMarkerStyle(2)
# leadingL0EM20OfflineEfficiency_truthPt.SetMarkerColor(4)
# leadingL0EM20TruthEfficiency_truthPt.Draw("p e")
# leadingL0EM20OfflineEfficiency_truthPt.Draw("p e same")

# legend.AddEntry(leadingL0EM20TruthEfficiency_truthPt, "EM20 wrt truth", "pL")
# legend.AddEntry(leadingL0EM20OfflineEfficiency_truthPt, "EM20 wrt offline", "pL")
# legend.Draw()
# canvas.Print("leadingL0EM20Efficiency.png")



# canvas.Clear()

# legend = TLegend(0.6, 0.55,0.75,0.65)
# legend.SetMargin(0.1)

# gStyle.SetOptStat(0)
# xmax =  subleadingL02EM10TruthEfficiencyPt.GetBinLowEdge(subleadingL02EM10TruthEfficiencyPt.GetNbinsX()) + subleadingL02EM10TruthEfficiencyPt.GetBinWidth(subleadingL02EM10TruthEfficiencyPt.GetNbinsX())
# xmin = subleadingL02EM10TruthEfficiencyPt.GetBinLowEdge(1) 
    
# titles = ";" + subleadingL02EM10TruthEfficiencyPt.GetXaxis().GetTitle()+";Efficiency"
# canvas.DrawFrame(xmin,0,xmax,1.05, titles)

# subleadingL02EM10TruthEfficiencyPt.SetMarkerColor(2)
# subleadingL02EM10TruthEfficiencyPt.SetMarkerStyle(2)
# subleadingL02EM10OfflineEfficiency_truthPt.SetMarkerColor(4)
# subleadingL02EM10TruthEfficiencyPt.Draw("p e")
# subleadingL02EM10OfflineEfficiency_truthPt.Draw("p e same")

# legend.AddEntry(subleadingL02EM10TruthEfficiencyPt, "2EM10 wrt truth", "pL")
# legend.AddEntry(subleadingL02EM10OfflineEfficiency_truthPt, "2EM10 wrt offline", "pL")
# legend.Draw()
# canvas.Print("leadingL02EM10Efficiency.png")



# deltaZ = treeToTH1FBinsCut("deltaZ", "1", tree, 50, 0., .5)
# deltaZ.Draw("hist")
# canvas.Print("deltaZ.png")
# canvas.Clear()



# colors = [1, 2, 3, 4, 6]
# canvas.DrawFrame(0,0,0.6,1.1, "; #Delta z cut [mm]")
# # canvas.SetLogx()
# dzCuts = [50, 40, 30, 20, 10, 7, 5, 3, 2, 1]
# dzCutsArr = array.array('d',[])
# efficiencyArr = array.array('d',[])
# for cut in dzCuts:
#     sig = deltaZ.Integral(0,cut)
#     tot = deltaZ.Integral(0,deltaZ.GetNbinsX()+1)
#     print sig/tot
#     efficiencyArr.append(sig/tot)
#     dzCutsArr.append(cut*0.01)


# graph = TGraph(len(dzCuts),dzCutsArr, efficiencyArr)
# graph.SetMarkerStyle(2)
# graph.Draw("p e")
# canvas.Print("deltaZeff.png")
