//Teoric Z mass - 91.19 Gev/c2 
#define JOB_RES -1

#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TLegend.h"

#include <string>
#include <iostream>

#if JOB_RES == 1
const char file[] = "../JOB_results/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786.testFile.root" ;
#elif JOB_RES == -1
const char file[] = "../mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786.testFile.root" ;
#else
const char file[] = "../run/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786.testFile.root" ;
#endif

typedef unsigned int UI ;

const UI NBINS = 50 ;
const double range[2] = { 0. , 150. } ;

const string var_name[] = {"ZMassTrack","smearedZMassTrack_offline"};
const string names[]={"Tracks","Smeared Tracks"};
const UI NVAR = (sizeof(var_name)/sizeof(var_name[0])) ;
const int color[NVAR] = { kBlue , kRed } ;

void zmass(){
  UI i ;
  string h_name[NVAR] ;

  //Canvas for plot display
  TCanvas *c1 = new TCanvas("c1","Z Mass",800,500);
  
  //Where is the data
  TFile *_file0 = TFile::Open(file);
  if (!_file0) return ;
  
  //Tree in TFile
  TTree *tree = (TTree*) _file0->Get("eventTree") ;
  if (!tree) return ;

  //Get histograms name
  for( i = 0 ; i < NVAR ; i++ ) h_name[i] = string("hist")+string(1,(char)(i+'1')) ;

  //For histograms
  TH1F* hist[NVAR] ;
  for( i = 0 ; i < NVAR ; i++ ){ 
    hist[i] = new TH1F(h_name[i].c_str(),var_name[i].c_str(),NBINS,range[0],range[1]);
    hist[i]->SetXTitle("Mass (GeV/c^{2})");
    hist[i]->SetYTitle("Counts (normalized)");
  }
  
  //Create legend 
  TLegend *leg = new TLegend(0.65,0.8,0.88,0.55);
  leg->SetHeader("Legend","C");
  for( i = 0 ; i < NVAR ; i++ ) leg->AddEntry(hist[i],names[i].c_str(),"l");

  //Creates histograms
  for ( i = 0 ; i < NVAR ; i++ ){
    hist[i]->SetTitle(var_name[i].c_str());
    tree->Draw(string(var_name[i]+">>"+h_name[i]).c_str());
    c1->SaveAs(string(var_name[i]+".png").c_str());
  }

  //Create geral histogram
  hist[0]->SetTitle("Z Mass Results") ;
  hist[0]->SetStats(kFALSE);
  hist[0]->Scale(1/hist[0]->Integral());
  hist[0]->SetLineColor(color[0]);
  hist[0]->SetLineWidth(2);
  c1->SetGrid();
  hist[0]->Draw("hist ") ;
  for ( i = 1 ; i < NVAR ; i++ ){ 
    hist[i]->SetStats(kFALSE); 
    hist[i]->SetLineColor(color[i]); 
    hist[i]->Scale(1/hist[i]->Integral());
    hist[i]->SetLineWidth(2);
    hist[i]->Draw("SAME hist") ; 
  }
  leg->Draw();
  c1->SaveAs("ZMass.png") ;

  _file0->Close("R") ;
  delete leg ;  
  delete c1 ;
}
