//Teoric Z mass - 91.19 Gev/c2 
#define JOB_RES 1

#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TLegend.h"

#include <string>
#include <iostream>

#if JOB_RES == 1
const char file[] = "mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786.testFile.root" ;
#elif JOB_RES == -1
const char file[] = "../mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786.testFile.root" ;
#else
const char file[] = "../run/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786.testFile.root" ;
#endif

typedef unsigned int UI ;

const UI NBINS = 75 ;
const double range[2] = { 0. , 0.15 } ;

const string var_name[] = {"dRMinTrackCluster","smearedDRMinTruthTrack_offline_single"};
const string names[]={"Tracks","Smeared Tracks"};
const UI NVAR = 2 ;
const int color[NVAR] = { kBlue , kRed } ;

void dR(){
  UI i ;
  string h_name[NVAR] = {"Tracks","SmearedTracks"} ;

  //Canvas for plot display
  TCanvas *c1 = new TCanvas("c1","dR",800,500);
  c1->SetGrid();
  
  //Where is the data
  TFile *_file0 = TFile::Open(file);
  if (!_file0) return ;
  
  //Tree in TFile
  TTree *tree = (TTree*) _file0->Get("eventTree") ;
  if (!tree) return ;

  //For histograms
  TH1F* h1 = new TH1F(h_name[0].c_str(),"",NBINS,range[0],range[1]), *h2 = new TH1F(h_name[1].c_str(),"",NBINS,range[0],range[1]) ; 
  
  //Create legend 
  TLegend *leg = new TLegend(0.46,0.84,0.68,0.62);
  leg->SetHeader("Legend","C");
  leg->AddEntry(h1,names[0].c_str(),"l");
  leg->AddEntry(h2,names[1].c_str(),"l");
  
  h1->SetTitle("Minimum dR between Tracks and Clusters") ;
  h1->SetXTitle("#Delta R");
  h1->SetYTitle("Counts");
  h1->SetLineColor(color[0]);
  h1->SetLineWidth(2);
  h1->Draw();
  h2->SetLineColor(color[1]);
  h2->SetLineWidth(2);

  tree->Draw(string(var_name[0]+">>"+h_name[0]).c_str());
  c1->Update(); //this will force the generation of the "stats" box
  TPaveStats *ps1 = (TPaveStats*)h1->GetListOfFunctions()->FindObject("stats");
  ps1->SetX1NDC(0.7); ps1->SetX2NDC(0.89);
  ps1->SetY1NDC(0.7); ps1->SetY2NDC(0.89);
  c1->Modified();

  tree->Draw(string(var_name[1]+">>"+h_name[1]).c_str());
  c1->Update();
  TPaveStats *ps2 = (TPaveStats*)h2->GetListOfFunctions()->FindObject("stats");
  ps2->SetX1NDC(0.7); ps2->SetX2NDC(0.89);
  ps2->SetY1NDC(0.49); ps2->SetY2NDC(0.68);
  c1->Modified();
  
  h1->Draw();
  h2->Draw("SAME");

  leg->Draw();  

  c1->SaveAs(string(string("dR")+".png").c_str());
  
  //_file0->Close("R");
  //delete leg ;
}
