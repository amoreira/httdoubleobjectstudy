#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TLegend.h"

#include <iostream>

#define AFTER_JOB 0

typedef unsigned int UI ;

#if AFTER_JOB == 1
const char _file[] = "../mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786.testFile.root" ;
#elif AFTER_JOB == 0
const char _file[] = "../run/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786.testFile.root" ;
#else
#error Bad AFTER_JOB value
#endif

void ncluster(){
  
  //Canvas for plot display
  TCanvas *_c1 = new TCanvas("_c1","Valid Clusters distribution",800,500);
  
  //Where is the data
  TFile *__file0 = TFile::Open(_file);
  if (!__file0) return ;
  
  //Tree in TFile
  TTree *_tree = (TTree*) __file0->Get("eventTree") ;
  if (!_tree) return ;

  TH1F* _h1 = new TH1F(), *_h2 = new TH1F() , *_h3 = new TH1F() ;

  _h1->SetName("_h1");
  _h2->SetName("_h2");
  _h3->SetName("_h3");
  _tree->Draw("nCluster10>>h1");
  _c1->SaveAs("Cluster10.png") ;
  _tree->Draw("nCluster20>>h2");
  _c1->SaveAs("Cluster20.png") ;
  _tree->Draw("nElectrons>>h3");
  _c1->SaveAs("nElectrons.png");
  
  __file0->Close("R") ;
  delete _c1 ;

}
