//https://www.niser.ac.in/sercehep2017/notes/RootTutorial_TTree.pdf
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TH2F.h"
#include "TH1F.h"
#include "TPad.h"

#include <iostream>
#include <string>
#include <vector>

#define AFTER_JOB 0

#if AFTER_JOB == 1
const char file[] = "../mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786.testFile.root" ;
#elif AFTER_JOB == 0
const char file[] = "../run/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786.testFile.root" ;
#else
#error Bad AFTER_JOB value
#endif

const float CUT = 0.1 ;
const float total_range[2] = { 0. , 8. } ;
const float dx = 0.05 ;
const unsigned int NBINS = (total_range[1]-total_range[0])/dx;

void color2Dgraph(){
  unsigned int i , j , k , l , m , aux1 , aux2 ;
  std::vector<float> *drTrkTrh = 0 , *drTrkClr = 0 ;
  int nElectrons , nTracks , nClusters ;
  long long sum01 = 0 , sumT = 0 ;
  
  //Canvas for plot display
  TCanvas *c1 = new TCanvas("c1","#delta R strudy",1000,600);
  TPad *pad1 = new TPad("pad1","Pad for Track Truth",0.0,0.0,0.5,0.5),
    *pad2 = new TPad("pad2","Pad for Track Cluster",0.0,0.5,0.5,1.0),
    *pad3 = new TPad("pad3","Pad for Color plot",0.5,0.0,1.0,1.0);

  pad1->Draw();
  pad2->Draw();
  pad3->Draw();

  //Where is the data
  TFile *_file0 = TFile::Open(file);
  if (!_file0) return ;
  
  //Tree in TFile
  TTree *tree = (TTree*) _file0->Get("eventTree") ;
  if (!tree) return ;
  
  tree->SetBranchAddress("dRTrackTruth",&drTrkTrh);
  tree->SetBranchAddress("dRTrackCluster",&drTrkClr);
  tree->SetBranchAddress("nElectrons",&nElectrons);
  
  //Color plot
  TH1F *hTT = new TH1F("hTT","dR of Truths and Tracks",NBINS,total_range[0],total_range[1]),
    *hTC = new TH1F("hTC","dR of Truths and Clusters",NBINS,total_range[0],total_range[1]);
  TH2F *h2D = new TH2F("h2D","Color Plot of dR(track-truth) and DR(track-cluster)",NBINS,
		       total_range[0],total_range[1],NBINS,total_range[0],total_range[1]);
  
  hTT->SetStats(0) ;
  hTT->GetXaxis()->SetTitle("dRTrackTruth");
  hTT->GetYaxis()->SetTitle("Counts");
  hTC->SetStats(0) ;
  hTC->GetYaxis()->SetTitle("Counts");
  hTC->GetXaxis()->SetTitle("dRTrackCluster");
  h2D->SetStats(0) ;
  h2D->GetXaxis()->SetTitle("dRTrackTruth");
  h2D->GetYaxis()->SetTitle("dRTrackCluster");
  
  for ( i = 0 ; i < tree->GetEntries() ; i++ ){
    tree->GetEntry(i);
    sumT += drTrkTrh->size()*drTrkClr->size();
    if(nElectrons){
      nTracks = drTrkTrh->size()/nElectrons ;
      nClusters = drTrkClr->size()/nTracks ;
    }
    for( j = 0 , aux1 = 0 ; j < drTrkTrh->size() ; j += nTracks ){
      for( k = 0 , l = 0 , aux2 = 0 ; k < nTracks ; k++ , l += nClusters , aux1++ )
	for( m = 0 ; m < nClusters ; m++ , aux2++ ){ 
	  h2D->Fill(drTrkTrh->at(aux1),drTrkClr->at(aux2));
	  hTT->Fill(drTrkTrh->at(aux1));
	  hTC->Fill(drTrkClr->at(aux2));
	  if ( drTrkTrh->at(aux1) < CUT && drTrkClr->at(aux2) < CUT ) 
	    sum01++ ;
	}
    }
  }
  
  std::cout << "Percentage in 0.1 square = " << float((long double)sum01/(long double)sumT)*100. << '%' << std::endl;
  pad1->cd();
  hTT->Draw();
  pad2->cd();
  hTC->Draw();
  pad3->cd();
  h2D->Draw("colz");
  c1->Update();
  c1->SaveAs("Color2Dgraph.png");

  _file0->Close("R");
  delete c1 ;
}
