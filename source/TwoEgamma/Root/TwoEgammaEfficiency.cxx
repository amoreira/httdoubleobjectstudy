// TwoEgamma includes
#include "../src/TwoEgammaEfficiency.h"
#include <math.h>
#include <string>
<<<<<<< HEAD
#include <map>
=======
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TRandom3.h"
#include "TTree.h"
#include "TObject.h"
#include "TLorentzVector.h"
<<<<<<< HEAD
=======
#include "../src/TTSmear.h"
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905

#include "xAODEgamma/ElectronContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTrigCalo/TrigEMClusterContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertexContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "GaudiKernel/ITHistSvc.h"
#include "AthenaBaseComps/AthAlgorithm.h"

TwoEgammaEfficiency::TwoEgammaEfficiency( const std::string& name, ISvcLocator* pSvcLocator ) : AthAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty ); //example property declaration
  declareProperty("RequireOffline", m_requireOffline);
  declareProperty("OfflinePtCut", m_offlinePtCut);
  declareProperty("OfflineTruthDrCut", m_offlineTruthDrCut);
  declareProperty("TrackConeDr", m_trackConeDr);
  declareProperty("TrackPtCut", m_trackPtCut);
  declareProperty("ElectronQuality", m_electronQuality);
  declareProperty("OfflineOnly", m_offlineOnly);
}


TwoEgammaEfficiency::~TwoEgammaEfficiency() {}


StatusCode TwoEgammaEfficiency::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
<<<<<<< HEAD
  
=======

>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  
  ATH_MSG_INFO("Offline pT cut = "<<m_offlinePtCut);


  ServiceHandle<ITHistSvc> histSvc("THistSvc",name()); 
  CHECK( histSvc.retrieve() );
  m_eventTree = new TTree("eventTree", "eventTree");
  CHECK( histSvc->regTree("/MYSTREAM/eventTree",m_eventTree) );

<<<<<<< HEAD
=======
  m_eventTree->Branch("minDr_eta", &m_minDr_eta);
  m_eventTree->Branch("minDr_phi", &m_minDr_phi);
  m_eventTree->Branch("minDr_pt", &m_minDr_pt);

>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  m_eventTree->Branch("leadingTruthPt", &m_leadingTruthPt);
  m_eventTree->Branch("leadingTruthEta", &m_leadingTruthEta);
  m_eventTree->Branch("leadingOfflinePt", &m_leadingOfflinePt);
  m_eventTree->Branch("leadingOfflineEta", &m_leadingOfflineEta);
  m_eventTree->Branch("leadingOffline_offlinePt", &m_leadingOffline_offlinePt);
  m_eventTree->Branch("leadingOffline_offlineEta", &m_leadingOffline_offlineEta);
  //L0 efficiency wrt truth
<<<<<<< HEAD
  //m_eventTree->Branch("leadingL0Truth_truthPt", &m_leadingL0Truth_truthPt);
  //m_eventTree->Branch("leadingL0Truth_truthEta", &m_leadingL0Truth_truthEta);
=======
  m_eventTree->Branch("leadingL0Truth_truthPt", &m_leadingL0Truth_truthPt);
  m_eventTree->Branch("leadingL0Truth_truthEta", &m_leadingL0Truth_truthEta);
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  m_eventTree->Branch("leadingL0EM10Truth_truthPt", &m_leadingL0EM10Truth_truthPt);
  m_eventTree->Branch("leadingL0EM10Truth_truthEta", &m_leadingL0EM10Truth_truthEta);
  m_eventTree->Branch("leadingL0EM20Truth_truthPt", &m_leadingL0EM20Truth_truthPt);
  m_eventTree->Branch("leadingL0EM20Truth_truthEta", &m_leadingL0EM20Truth_truthEta);
<<<<<<< HEAD
  //m_eventTree->Branch("leadingL02EM10Truth_truthPt", &m_leadingL02EM10Truth_truthPt);
  //m_eventTree->Branch("leadingL02EM10Truth_truthEta", &m_leadingL02EM10Truth_truthEta);
  //L0 efficiency wrt offline
  //m_eventTree->Branch("leadingL0Offline_truthPt", &m_leadingL0Offline_truthPt);
  //m_eventTree->Branch("leadingL0Offline_truthEta", &m_leadingL0Offline_truthEta);
  //m_eventTree->Branch("leadingL0Offline_offlinePt", &m_leadingL0Offline_offlinePt);
  //m_eventTree->Branch("leadingL0Offline_offlineEta", &m_leadingL0Offline_offlineEta);
=======
  m_eventTree->Branch("leadingL02EM10Truth_truthPt", &m_leadingL02EM10Truth_truthPt);
  m_eventTree->Branch("leadingL02EM10Truth_truthEta", &m_leadingL02EM10Truth_truthEta);
  //L0 efficiency wrt offline
  m_eventTree->Branch("leadingL0Offline_truthPt", &m_leadingL0Offline_truthPt);
  m_eventTree->Branch("leadingL0Offline_truthEta", &m_leadingL0Offline_truthEta);
  m_eventTree->Branch("leadingL0Offline_offlinePt", &m_leadingL0Offline_offlinePt);
  m_eventTree->Branch("leadingL0Offline_offlineEta", &m_leadingL0Offline_offlineEta);
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  m_eventTree->Branch("leadingL0EM10Offline_truthPt", &m_leadingL0EM10Offline_truthPt);
  m_eventTree->Branch("leadingL0EM10Offline_truthEta", &m_leadingL0EM10Offline_truthEta);
  m_eventTree->Branch("leadingL0EM20Offline_truthPt", &m_leadingL0EM20Offline_truthPt);
  m_eventTree->Branch("leadingL0EM20Offline_truthEta", &m_leadingL0EM20Offline_truthEta);
<<<<<<< HEAD
  //m_eventTree->Branch("leadingL02EM10Offline_truthPt", &m_leadingL02EM10Offline_truthPt);
  //m_eventTree->Branch("leadingL02EM10Offline_truthEta", &m_leadingL02EM10Offline_truthEta);
=======
  m_eventTree->Branch("leadingL02EM10Offline_truthPt", &m_leadingL02EM10Offline_truthPt);
  m_eventTree->Branch("leadingL02EM10Offline_truthEta", &m_leadingL02EM10Offline_truthEta);
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  m_eventTree->Branch("leadingL0EM10Offline_offlinePt", &m_leadingL0EM10Offline_offlinePt);
  m_eventTree->Branch("leadingL0EM10Offline_offlineEta", &m_leadingL0EM10Offline_offlineEta);
  m_eventTree->Branch("leadingL0EM20Offline_offlinePt", &m_leadingL0EM20Offline_offlinePt);
  m_eventTree->Branch("leadingL0EM20Offline_offlineEta", &m_leadingL0EM20Offline_offlineEta);
 
  //Double electron histograms
  //Offline efficiency
  m_eventTree->Branch("subleadingTruthPt", &m_subleadingTruthPt);
  m_eventTree->Branch("subleadingTruthEta", &m_subleadingTruthEta);
  m_eventTree->Branch("subleadingOfflinePt", &m_subleadingOfflinePt);
  m_eventTree->Branch("subleadingOfflineEta", &m_subleadingOfflineEta);
  m_eventTree->Branch("subleadingOffline_offlinePt", &m_subleadingOffline_offlinePt);
  m_eventTree->Branch("subleadingOffline_offlineEta", &m_subleadingOffline_offlineEta);
  //L0 efficiency wrt truth
<<<<<<< HEAD
  //m_eventTree->Branch("subleadingL0Truth_truthPt", &m_subleadingL0Truth_truthPt);
  //m_eventTree->Branch("subleadingL0Truth_truthEta", &m_subleadingL0Truth_truthEta);
  m_eventTree->Branch("subleadingL02EM10Truth_truthPt", &m_subleadingL02EM10Truth_truthPt);
  m_eventTree->Branch("subleadingL02EM10Truth_truthEta", &m_subleadingL02EM10Truth_truthEta);
  m_eventTree->Branch("subleadingL02EM20Truth_truthPt", &m_subleadingL02EM20Truth_truthPt);
  m_eventTree->Branch("subleadingL02EM20Truth_truthEta", &m_subleadingL02EM20Truth_truthEta);
  //L0 efficiency wrt offline
  //m_eventTree->Branch("subleadingL0Offline_truthPt", &m_subleadingL0Offline_truthPt);
  //m_eventTree->Branch("subleadingL0Offline_truthEta", &m_subleadingL0Offline_truthEta);
  //m_eventTree->Branch("subleadingL0Offline_offlinePt", &m_subleadingL0Offline_offlinePt);
  //m_eventTree->Branch("subleadingL0Offline_offlineEta", &m_subleadingL0Offline_offlineEta);
  m_eventTree->Branch("subleadingL0EM10Offline_truthPt", &m_subleadingL0EM10Offline_truthPt);
  m_eventTree->Branch("subleadingL0EM10Offline_truthEta", &m_subleadingL0EM10Offline_truthEta);
=======
  m_eventTree->Branch("subleadingL0Truth_truthPt", &m_subleadingL0Truth_truthPt);
  m_eventTree->Branch("subleadingL0Truth_truthEta", &m_subleadingL0Truth_truthEta);
  m_eventTree->Branch("subleadingL02EM10Truth_truthPt", &m_subleadingL02EM10Truth_truthPt);
  m_eventTree->Branch("subleadingL02EM10Truth_truthEta", &m_subleadingL02EM10Truth_truthPt);
  m_eventTree->Branch("subleadingL02EM20Truth_truthPt", &m_subleadingL02EM20Truth_truthPt);
  m_eventTree->Branch("subleadingL02EM20Truth_truthEta", &m_subleadingL02EM20Truth_truthPt);
  //L0 efficiency wrt offline
  m_eventTree->Branch("subleadingL0Offline_truthPt", &m_subleadingL0Offline_truthPt);
  m_eventTree->Branch("subleadingL0Offline_truthEta", &m_subleadingL0Offline_truthEta);
  m_eventTree->Branch("subleadingL0Offline_offlinePt", &m_subleadingL0Offline_offlinePt);
  m_eventTree->Branch("subleadingL0Offline_offlineEta", &m_subleadingL0Offline_offlineEta);
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  m_eventTree->Branch("subleadingL02EM10Offline_truthPt", &m_subleadingL02EM10Offline_truthPt);
  m_eventTree->Branch("subleadingL02EM10Offline_truthEta", &m_subleadingL02EM10Offline_truthEta);
  m_eventTree->Branch("subleadingL02EM10Offline_offlinePt", &m_subleadingL02EM10Offline_offlinePt);
  m_eventTree->Branch("subleadingL02EM10Offline_offlineEta", &m_subleadingL02EM10Offline_offlineEta);
  m_eventTree->Branch("subleadingL02EM20Offline_truthPt", &m_subleadingL02EM20Offline_truthPt);
  m_eventTree->Branch("subleadingL02EM20Offline_truthEta", &m_subleadingL02EM20Offline_truthEta);
  m_eventTree->Branch("subleadingL02EM20Offline_offlinePt", &m_subleadingL02EM20Offline_offlinePt);
  m_eventTree->Branch("subleadingL02EM20Offline_offlineEta", &m_subleadingL02EM20Offline_offlineEta);
  //L1 efficiency wrt L0(truth-matched) - leading pT
  m_eventTree->Branch("leadingL1L0Truth_truthPt", &m_leadingL1L0Truth_truthPt);
  m_eventTree->Branch("leadingL1L0Truth_truthEta", &m_leadingL1L0Truth_truthEta);
  m_eventTree->Branch("leadingL1L0Truth_trackPt", &m_leadingL1L0Truth_trackPt);
  m_eventTree->Branch("leadingL1L0Truth_trackEta", &m_leadingL1L0Truth_trackEta);
  
  
<<<<<<< HEAD
  //m_eventTree->Branch("leadingL1L0Truth1_truthPt", &m_leadingL1L0Truth1_truthPt);
  //m_eventTree->Branch("leadingL1L0Truth1_truthEta", &m_leadingL1L0Truth1_truthEta); 
  //m_eventTree->Branch("leadingL1L0Truth2_truthPt", &m_leadingL1L0Truth2_truthPt);
  //m_eventTree->Branch("leadingL1L0Truth2_truthEta", &m_leadingL1L0Truth2_truthEta); 
  //m_eventTree->Branch("leadingL1L0Truth4_truthPt", &m_leadingL1L0Truth4_truthPt);
  //m_eventTree->Branch("leadingL1L0Truth4_truthEta", &m_leadingL1L0Truth4_truthEta); 

  //m_eventTree->Branch("leadingL1L0Truth1_trackPt", &m_leadingL1L0Truth1_trackPt);
  //m_eventTree->Branch("leadingL1L0Truth1_trackEta", &m_leadingL1L0Truth1_trackEta);
  //m_eventTree->Branch("leadingL1L0Truth2_trackPt", &m_leadingL1L0Truth2_trackPt);
  //m_eventTree->Branch("leadingL1L0Truth2_trackEta", &m_leadingL1L0Truth2_trackEta);
  //m_eventTree->Branch("leadingL1L0Truth4_trackPt", &m_leadingL1L0Truth4_trackPt);
  //m_eventTree->Branch("leadingL1L0Truth4_trackEta", &m_leadingL1L0Truth4_trackEta);
=======
  m_eventTree->Branch("leadingL1L0Truth1_truthPt", &m_leadingL1L0Truth1_truthPt);
  m_eventTree->Branch("leadingL1L0Truth1_truthEta", &m_leadingL1L0Truth1_truthEta); 
  m_eventTree->Branch("leadingL1L0Truth2_truthPt", &m_leadingL1L0Truth2_truthPt);
  m_eventTree->Branch("leadingL1L0Truth2_truthEta", &m_leadingL1L0Truth2_truthEta); 
  m_eventTree->Branch("leadingL1L0Truth4_truthPt", &m_leadingL1L0Truth4_truthPt);
  m_eventTree->Branch("leadingL1L0Truth4_truthEta", &m_leadingL1L0Truth4_truthEta); 

  m_eventTree->Branch("leadingL1L0Truth1_trackPt", &m_leadingL1L0Truth1_trackPt);
  m_eventTree->Branch("leadingL1L0Truth1_trackEta", &m_leadingL1L0Truth1_trackEta);
  m_eventTree->Branch("leadingL1L0Truth2_trackPt", &m_leadingL1L0Truth2_trackPt);
  m_eventTree->Branch("leadingL1L0Truth2_trackEta", &m_leadingL1L0Truth2_trackEta);
  m_eventTree->Branch("leadingL1L0Truth4_trackPt", &m_leadingL1L0Truth4_trackPt);
  m_eventTree->Branch("leadingL1L0Truth4_trackEta", &m_leadingL1L0Truth4_trackEta);
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905


  m_eventTree->Branch("leadingL1L0TruthSmeared4_truthPt", &m_leadingL1L0TruthSmeared4_truthPt);
  m_eventTree->Branch("leadingL1L0TruthSmeared4_truthEta", &m_leadingL1L0TruthSmeared4_truthEta); 
<<<<<<< HEAD
  //m_eventTree->Branch("leadingL1L0TruthSmeared4_trackPt", &m_leadingL1L0TruthSmeared4_trackPt);
  //m_eventTree->Branch("leadingL1L0TruthSmeared4_trackEta", &m_leadingL1L0TruthSeared4_trackEta);
=======
  m_eventTree->Branch("leadingL1L0TruthSmeared4_trackPt", &m_leadingL1L0TruthSmeared4_trackPt);
  m_eventTree->Branch("leadingL1L0TruthSmeared4_trackEta", &m_leadingL1L0TruthSeared4_trackEta);
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  
  
  
  
  //L1 efficiency wrt L0(offline-matched) - leading pT
  m_eventTree->Branch("leadingL1L0Offline_truthPt", &m_leadingL1L0Offline_truthPt);
  m_eventTree->Branch("leadingL1L0Offline_truthEta", &m_leadingL1L0Offline_truthEta);
  m_eventTree->Branch("leadingL1L0Offline_trackPt", &m_leadingL1L0Offline_trackPt);
  m_eventTree->Branch("leadingL1L0Offline_trackEta", &m_leadingL1L0Offline_trackEta);
  m_eventTree->Branch("leadingL1L0Offline_offlinePt", &m_leadingL1L0Offline_offlinePt);
  m_eventTree->Branch("leadingL1L0Offline_offlineEta", &m_leadingL1L0Offline_offlineEta);

<<<<<<< HEAD
  //m_eventTree->Branch("leadingL1L0Offline1_truthPt", &m_leadingL1L0Offline1_truthPt);
  //m_eventTree->Branch("leadingL1L0Offline1_truthEta", &m_leadingL1L0Offline1_truthEta); 
  //m_eventTree->Branch("leadingL1L0Offline2_truthPt", &m_leadingL1L0Offline2_truthPt);
  //m_eventTree->Branch("leadingL1L0Offline2_truthEta", &m_leadingL1L0Offline2_truthEta); 
  //m_eventTree->Branch("leadingL1L0Offline4_truthPt", &m_leadingL1L0Offline4_truthPt);
  //m_eventTree->Branch("leadingL1L0Offline4_truthEta", &m_leadingL1L0Offline4_truthEta); 

  //m_eventTree->Branch("leadingL1L0Offline1_trackPt", &m_leadingL1L0Offline1_trackPt);
  //m_eventTree->Branch("leadingL1L0Offline1_trackEta", &m_leadingL1L0Offline1_trackEta);
  //m_eventTree->Branch("leadingL1L0Offline2_trackPt", &m_leadingL1L0Offline2_trackPt);
  //m_eventTree->Branch("leadingL1L0Offline2_trackEta", &m_leadingL1L0Offline2_trackEta);
  //m_eventTree->Branch("leadingL1L0Offline4_trackPt", &m_leadingL1L0Offline4_trackPt);
  //m_eventTree->Branch("leadingL1L0Offline4_trackEta", &m_leadingL1L0Offline4_trackEta);


  //m_eventTree->Branch("leadingL1L0OfflineSmeared4_truthPt", &m_leadingL1L0OfflineSmeared4_truthPt);
  //m_eventTree->Branch("leadingL1L0OfflineSmeared4_truthEta", &m_leadingL1L0OfflineSmeared4_truthEta); 
  //m_eventTree->Branch("leadingL1L0OfflineSmeared4_trackPt", &m_leadingL1L0OfflineSmeared4_trackPt);
  //m_eventTree->Branch("leadingL1L0OfflineSmeared4_trackEta", &m_leadingL1L0OfflineSeared4_trackEta);
=======
  m_eventTree->Branch("leadingL1L0Offline1_truthPt", &m_leadingL1L0Offline1_truthPt);
  m_eventTree->Branch("leadingL1L0Offline1_truthEta", &m_leadingL1L0Offline1_truthEta); 
  m_eventTree->Branch("leadingL1L0Offline2_truthPt", &m_leadingL1L0Offline2_truthPt);
  m_eventTree->Branch("leadingL1L0Offline2_truthEta", &m_leadingL1L0Offline2_truthEta); 
  m_eventTree->Branch("leadingL1L0Offline4_truthPt", &m_leadingL1L0Offline4_truthPt);
  m_eventTree->Branch("leadingL1L0Offline4_truthEta", &m_leadingL1L0Offline4_truthEta); 

  m_eventTree->Branch("leadingL1L0Offline1_trackPt", &m_leadingL1L0Offline1_trackPt);
  m_eventTree->Branch("leadingL1L0Offline1_trackEta", &m_leadingL1L0Offline1_trackEta);
  m_eventTree->Branch("leadingL1L0Offline2_trackPt", &m_leadingL1L0Offline2_trackPt);
  m_eventTree->Branch("leadingL1L0Offline2_trackEta", &m_leadingL1L0Offline2_trackEta);
  m_eventTree->Branch("leadingL1L0Offline4_trackPt", &m_leadingL1L0Offline4_trackPt);
  m_eventTree->Branch("leadingL1L0Offline4_trackEta", &m_leadingL1L0Offline4_trackEta);


  m_eventTree->Branch("leadingL1L0OfflineSmeared4_truthPt", &m_leadingL1L0OfflineSmeared4_truthPt);
  m_eventTree->Branch("leadingL1L0OfflineSmeared4_truthEta", &m_leadingL1L0OfflineSmeared4_truthEta); 
  m_eventTree->Branch("leadingL1L0OfflineSmeared4_trackPt", &m_leadingL1L0OfflineSmeared4_trackPt);
  m_eventTree->Branch("leadingL1L0OfflineSmeared4_trackEta", &m_leadingL1L0OfflineSeared4_trackEta);
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905




  //L1 efficiency wrt L0(truth-matched) - subleading pT
  m_eventTree->Branch("subleadingL1L0Truth_truthPt", &m_subleadingL1L0Truth_truthPt);
  m_eventTree->Branch("subleadingL1L0Truth_truthEta", &m_subleadingL1L0Truth_truthEta);
  m_eventTree->Branch("subleadingL1L0Truth_trackPt", &m_subleadingL1L0Truth_trackPt);
  m_eventTree->Branch("subleadingL1L0Truth_trackEta", &m_subleadingL1L0Truth_trackEta);


  m_eventTree->Branch("L1L0Truth_truthPt", &m_L1L0Truth_truthPt);
  m_eventTree->Branch("L1L0Truth_truthEta", &m_L1L0Truth_truthEta);
  m_eventTree->Branch("L1L0Truth1_truthPt", &m_L1L0Truth1_truthPt);
  m_eventTree->Branch("L1L0Truth1_truthEta", &m_L1L0Truth1_truthEta);
  m_eventTree->Branch("L1L0Truth2_truthPt", &m_L1L0Truth2_truthPt);
  m_eventTree->Branch("L1L0Truth2_truthEta", &m_L1L0Truth2_truthEta);
  m_eventTree->Branch("L1L0Truth4_truthPt", &m_L1L0Truth4_truthPt);
  m_eventTree->Branch("L1L0Truth4_truthEta", &m_L1L0Truth4_truthEta);
<<<<<<< HEAD
  m_eventTree->Branch("L1L0Truthsmeared_truthPt", &m_L1L0Truthsmeared_truthPt);
  m_eventTree->Branch("L1L0Truthsmeared_truthEta", &m_L1L0Truthsmeared_truthEta);
  m_eventTree->Branch("L1L0Truth1smeared_truthPt", &m_L1L0Truth1smeared_truthPt);
  m_eventTree->Branch("L1L0Truth1smeared_truthEta", &m_L1L0Truth1smeared_truthEta);
  m_eventTree->Branch("L1L0Truth2smeared_truthPt", &m_L1L0Truth2smeared_truthPt);
  m_eventTree->Branch("L1L0Truth2smeared_truthEta", &m_L1L0Truth2smeared_truthEta);
  m_eventTree->Branch("L1L0Truth4smeared_truthPt", &m_L1L0Truth4smeared_truthPt);
  m_eventTree->Branch("L1L0Truth4smeared_truthEta", &m_L1L0Truth4smeared_truthEta);

  //m_eventTree->Branch("L02EM10Truth_truthPt", &m_L02EM10Truth_truthPt);
  //m_eventTree->Branch("L02EM10Truth_truthEta", &m_L02EM10Truth_truthEta);


  //L1 efficiency wrt L0(offline-matched) - subleading pT
=======
  m_eventTree->Branch("L1L0Truth4smeared_truthPt", &m_L1L0Truth4smeared_truthPt);
  m_eventTree->Branch("L1L0Truth4smeared_truthEta", &m_L1L0Truth4smeared_truthEta);

  m_eventTree->Branch("L02EM10Truth_truthPt", &m_L02EM10Truth_truthPt);
  m_eventTree->Branch("L02EM10Truth_truthEta", &m_L02EM10Truth_truthEta);


//L1 efficiency wrt L0(offline-matched) - subleading pT
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  m_eventTree->Branch("subleadingL1L0Offline_truthPt", &m_subleadingL1L0Offline_truthPt); 
  m_eventTree->Branch("subleadingL1L0Offline_truthEta", &m_subleadingL1L0Offline_truthEta);
  m_eventTree->Branch("subleadingL1L0Offline_trackPt", &m_subleadingL1L0Offline_trackPt);
  m_eventTree->Branch("subleadingL1L0Offline_trackEta", &m_subleadingL1L0Offline_trackEta);
  m_eventTree->Branch("subleadingL1L0Offline_offlinePt", &m_subleadingL1L0Offline_offlinePt);
  m_eventTree->Branch("subleadingL1L0Offline_offlineEta", &m_subleadingL1L0Offline_offlineEta);

  m_eventTree->Branch("L1L0Offline_offlinePt", &m_L1L0Offline_offlinePt);
  m_eventTree->Branch("L1L0Offline_offlineEta", &m_L1L0Offline_offlineEta);
  m_eventTree->Branch("L1L0Offline1_offlinePt", &m_L1L0Offline1_offlinePt);
  m_eventTree->Branch("L1L0Offline1_offlineEta", &m_L1L0Offline1_offlineEta);
  m_eventTree->Branch("L1L0Offline2_offlinePt", &m_L1L0Offline2_offlinePt);
  m_eventTree->Branch("L1L0Offline2_offlineEta", &m_L1L0Offline2_offlineEta);
  m_eventTree->Branch("L1L0Offline4_offlinePt", &m_L1L0Offline4_offlinePt);
  m_eventTree->Branch("L1L0Offline4_offlineEta", &m_L1L0Offline4_offlineEta);
<<<<<<< HEAD
  m_eventTree->Branch("L1L0Offlinesmeared_offlinePt", &m_L1L0Offlinesmeared_offlinePt);
  m_eventTree->Branch("L1L0Offlinesmeared_offlineEta", &m_L1L0Offlinesmeared_offlineEta);
  m_eventTree->Branch("L1L0Offline1smeared_offlinePt", &m_L1L0Offline1smeared_offlinePt);
  m_eventTree->Branch("L1L0Offline1smeared_offlineEta", &m_L1L0Offline1smeared_offlineEta);
  m_eventTree->Branch("L1L0Offline2smeared_offlinePt", &m_L1L0Offline2smeared_offlinePt);
  m_eventTree->Branch("L1L0Offline2smeared_offlineEta", &m_L1L0Offline2smeared_offlineEta);
=======
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  m_eventTree->Branch("L1L0Offline4smeared_offlinePt", &m_L1L0Offline4smeared_offlinePt);
  m_eventTree->Branch("L1L0Offline4smeared_offlineEta", &m_L1L0Offline4smeared_offlineEta);

  m_eventTree->Branch("L02EM10Offline_offlinePt", &m_L02EM10Offline_offlinePt);
  m_eventTree->Branch("L02EM10Offline_offlineEta", &m_L02EM10Offline_offlineEta);


  m_eventTree->Branch("subleadingL1L0Offline1_truthPt", &m_subleadingL1L0Offline1_truthPt); 
  m_eventTree->Branch("subleadingL1L0Offline1_truthEta", &m_subleadingL1L0Offline1_truthEta);
  m_eventTree->Branch("subleadingL1L0Offline2_truthPt", &m_subleadingL1L0Offline2_truthPt); 
  m_eventTree->Branch("subleadingL1L0Offline2_truthEta", &m_subleadingL1L0Offline2_truthEta);
  m_eventTree->Branch("subleadingL1L0Offline4_truthPt", &m_subleadingL1L0Offline4_truthPt); 
  m_eventTree->Branch("subleadingL1L0Offline4_truthEta", &m_subleadingL1L0Offline4_truthEta);


<<<<<<< HEAD
  //m_eventTree->Branch("subleadingL1L0Offline1_trackPt", &m_subleadingL1L0Offline1_trackPt);
  //m_eventTree->Branch("subleadingL1L0Offline1_trackEta", &m_subleadingL1L0Offline1_trackEta);
  //m_eventTree->Branch("subleadingL1L0Offline2_trackPt", &m_subleadingL1L0Offline2_trackPt);
  //m_eventTree->Branch("subleadingL1L0Offline2_trackEta", &m_subleadingL1L0Offline2_trackEta);
  //m_eventTree->Branch("subleadingL1L0Offline4_trackPt", &m_subleadingL1L0Offline4_trackPt);
  //m_eventTree->Branch("subleadingL1L0Offline4_trackEta", &m_subleadingL1L0Offline4_trackEta);


  m_eventTree->Branch("subleadingL1L0OfflineSmeared4_truthPt", &m_subleadingL1L0OfflineSmeared_truthPt);
  m_eventTree->Branch("subleadingL1L0OfflineSmeared4_truthEta", &m_subleadingL1L0OfflineSmeared_truthEta);
  m_eventTree->Branch("subleadingL1L0OfflineSmeared4_truthPt", &m_subleadingL1L0OfflineSmeared1_truthPt);
  m_eventTree->Branch("subleadingL1L0OfflineSmeared4_truthEta", &m_subleadingL1L0OfflineSmeared1_truthEta);
  m_eventTree->Branch("subleadingL1L0OfflineSmeared4_truthPt", &m_subleadingL1L0OfflineSmeared2_truthPt);
  m_eventTree->Branch("subleadingL1L0OfflineSmeared4_truthEta", &m_subleadingL1L0OfflineSmeared2_truthEta);
  m_eventTree->Branch("subleadingL1L0OfflineSmeared4_truthPt", &m_subleadingL1L0OfflineSmeared4_truthPt);
  m_eventTree->Branch("subleadingL1L0OfflineSmeared4_truthEta", &m_subleadingL1L0OfflineSmeared4_truthEta); 
  //m_eventTree->Branch("subleadingL1L0OfflineSmeared4_trackPt", &m_subleadingL1L0OfflineSmeared4_trackPt);
  //m_eventTree->Branch("subleadingL1L0OfflineSmeared4_trackEta", &m_subleadingL1L0OfflineSeared4_trackEta);
=======
  m_eventTree->Branch("subleadingL1L0Offline1_trackPt", &m_subleadingL1L0Offline1_trackPt);
  m_eventTree->Branch("subleadingL1L0Offline1_trackEta", &m_subleadingL1L0Offline1_trackEta);
  m_eventTree->Branch("subleadingL1L0Offline2_trackPt", &m_subleadingL1L0Offline2_trackPt);
  m_eventTree->Branch("subleadingL1L0Offline2_trackEta", &m_subleadingL1L0Offline2_trackEta);
  m_eventTree->Branch("subleadingL1L0Offline4_trackPt", &m_subleadingL1L0Offline4_trackPt);
  m_eventTree->Branch("subleadingL1L0Offline4_trackEta", &m_subleadingL1L0Offline4_trackEta);




  m_eventTree->Branch("subleadingL1L0OfflineSmeared4_truthPt", &m_subleadingL1L0OfflineSmeared4_truthPt);
  m_eventTree->Branch("subleadingL1L0OfflineSmeared4_truthEta", &m_subleadingL1L0OfflineSmeared4_truthEta); 
  m_eventTree->Branch("subleadingL1L0OfflineSmeared4_trackPt", &m_subleadingL1L0OfflineSmeared4_trackPt);
  m_eventTree->Branch("subleadingL1L0OfflineSmeared4_trackEta", &m_subleadingL1L0OfflineSeared4_trackEta);
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905


  m_eventTree->Branch("subleadingL1L0Truth1_truthPt", &m_subleadingL1L0Truth1_truthPt); 
  m_eventTree->Branch("subleadingL1L0Truth1_truthEta", &m_subleadingL1L0Truth1_truthEta);
  m_eventTree->Branch("subleadingL1L0Truth2_truthPt", &m_subleadingL1L0Truth2_truthPt); 
  m_eventTree->Branch("subleadingL1L0Truth2_truthEta", &m_subleadingL1L0Truth2_truthEta);
  m_eventTree->Branch("subleadingL1L0Truth4_truthPt", &m_subleadingL1L0Truth4_truthPt); 
  m_eventTree->Branch("subleadingL1L0Truth4_truthEta", &m_subleadingL1L0Truth4_truthEta);
<<<<<<< HEAD
  //m_eventTree->Branch("subleadingL1L0TruthSmeared4_truthPt", &m_subleadingL1L0TruthSmeared4_truthPt);
  //m_eventTree->Branch("subleadingL1L0TruthSmeared4_truthEta", &m_subleadingL1L0TruthSmeared4_truthEta); 


  m_eventTree->Branch("trackClusterDeltaR", &m_trackClusterDeltaR);
  m_eventTree->Branch("trackClusterSmearedDeltaR", &m_trackClusterSmearedDeltaR);
  //m_eventTree->Branch("truthOfflineDeltaR", &m_truthOfflineDeltaR);
  //m_eventTree->Branch("clusterOfflineDeltaR", &m_clusterOfflineDeltaR);
  //m_eventTree->Branch("clusterTruthDeltaR", &m_clusterTruthDeltaR);
=======
  m_eventTree->Branch("subleadingL1L0TruthSmeared4_truthPt", &m_subleadingL1L0TruthSmeared4_truthPt);
  m_eventTree->Branch("subleadingL1L0TruthSmeared4_truthEta", &m_subleadingL1L0TruthSmeared4_truthEta); 


  m_eventTree->Branch("trackClusterDeltaR", &m_trackClusterDeltaR);
  m_eventTree->Branch("truthOfflineDeltaR", &m_truthOfflineDeltaR);
  m_eventTree->Branch("clusterOfflineDeltaR", &m_clusterOfflineDeltaR);
  m_eventTree->Branch("clusterTruthDeltaR", &m_clusterTruthDeltaR);
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  m_eventTree->Branch("deltaZ", &m_deltaZ);
  m_eventTree->Branch("deltaZ1", &m_deltaZ1);
  m_eventTree->Branch("deltaZ2", &m_deltaZ2);
  m_eventTree->Branch("deltaZ4", &m_deltaZ4);
<<<<<<< HEAD
  m_eventTree->Branch("smearedDeltaZ", &m_smearedDeltaZ);
  m_eventTree->Branch("smearedDeltaZ1", &m_smearedDeltaZ1);
  m_eventTree->Branch("smearedDeltaZ2", &m_smearedDeltaZ2);
=======
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  m_eventTree->Branch("smearedDeltaZ4", &m_smearedDeltaZ4);

  m_eventTree->Branch("deltaZTruth", &m_deltaZTruth); 
  m_eventTree->Branch("deltaZ1Truth", &m_deltaZ1Truth);
  m_eventTree->Branch("deltaZ2Truth", &m_deltaZ2Truth);
  m_eventTree->Branch("deltaZ4Truth", &m_deltaZ4Truth);
<<<<<<< HEAD
  m_eventTree->Branch("smearedDeltaZTruth", &m_smearedDeltaZTruth);
  m_eventTree->Branch("smearedDeltaZ1Truth", &m_smearedDeltaZ1Truth);
  m_eventTree->Branch("smearedDeltaZ2Truth", &m_smearedDeltaZ2Truth);
  m_eventTree->Branch("smearedDeltaZ4Truth", &m_smearedDeltaZ4Truth);

  m_eventTree->Branch("truthPt", &m_truthPt);
  //m_eventTree->Branch("truthE", &m_truthE);
  m_eventTree->Branch("truthEta", &m_truthEta);

  //m_eventTree->Branch("offlinePt", &m_offlinePt);
  //m_eventTree->Branch("offlineE", &m_offlineE);
  //m_eventTree->Branch("offlineEta", &m_offlineEta);

  m_eventTree->Branch("offlinePt_Loose", &m_offlinePt_Loose);
  //m_eventTree->Branch("offlineE_Loose", &m_offlineE_Loose);
  m_eventTree->Branch("offlineEta_Loose", &m_offlineEta_Loose);

  //m_eventTree->Branch("offlinePt_Medium", &m_offlinePt_Medium);
  //m_eventTree->Branch("offlineE_Medium", &m_offlineE_Medium);
  //m_eventTree->Branch("offlineEta_Medium", &m_offlineEta_Medium);

  //m_eventTree->Branch("offlinePt_Tight", &m_offlinePt_Tight);
  //m_eventTree->Branch("offlineE_Tight", &m_offlineE_Tight);
  //m_eventTree->Branch("offlineEta_Tight", &m_offlineEta_Tight);

  //m_fromZcount = 0;
  //m_electronQualityPass = 0;
  m_eventTree->Branch("dR_failing2EM10", &m_dR_failing2EM10);
  m_eventTree->Branch("subleadingSingleTruthPt", &m_subleadingSingleTruthPt);
  m_eventTree->Branch("averageInteractionsPerCrossing", &m_avIntPerCrossing);

#if SIGNAL_TEST == 1
  m_eventTree->Branch("ZMassTrack", &m_ZMassTrack);
  m_eventTree->Branch("ZMassTrackCluster", &m_ZMassTrackCluster);
  m_eventTree->Branch("smearedZMassTrack_truth", &m_smearedZMassTrack_truth);
  m_eventTree->Branch("smearedZMassTrack_offline", &m_smearedZMassTrack_offline);
  m_eventTree->Branch("smearedZMassTrack_same", &m_smearedZMassTrack_same);
#endif
  
  //More data
#if GET_DELTAR == 1
  m_eventTree->Branch("dRTrackTruth",&m_dRTrackTruth);
  m_eventTree->Branch("dRTrackCluster",&m_dRTrackCluster);
#endif

  m_eventTree->Branch("dRMinTruthTrack",&m_dRMinTruthTrack);
  m_eventTree->Branch("dRMinTrackCluster",&m_dRMinTrackCluster);
  m_eventTree->Branch("dRMatchedTruthTrack",&m_dRMatchedTruthTrack);
  m_eventTree->Branch("dRMatchedTrackCluster",&m_dRMatchedTrackCluster);

  m_eventTree->Branch("smearedDRMinTruthTrack_truth",&m_smearedDRMinTruthTrack_truth);
  m_eventTree->Branch("smearedDRMinTruthTrack_offline",&m_smearedDRMinTruthTrack_offline);
  m_eventTree->Branch("smearedDRMinTruthTrack_truth_single",&m_smearedDRMinTruthTrack_truth_single);
  m_eventTree->Branch("smearedDRMinTruthTrack_offline_single",&m_smearedDRMinTruthTrack_offline_single);

  m_eventTree->Branch("smearedDRMatchedTruthTrack_truth",&m_smearedDRMatchedTruthTrack_truth);
  m_eventTree->Branch("smearedDRMatchedTruthTrack_offline",&m_smearedDRMatchedTruthTrack_offline);
  m_eventTree->Branch("smearedDRMatchedTruthTrack_truth_single",&m_smearedDRMatchedTruthTrack_truth_single);
  m_eventTree->Branch("smearedDRMatchedTruthTrack_offline_single",&m_smearedDRMatchedTruthTrack_offline_single);

  m_eventTree->Branch("Pt",&m_Pt);
  m_eventTree->Branch("Eta",&m_Eta);
  m_eventTree->Branch("Phi",&m_Phi);
  m_eventTree->Branch("smearedZ0",&m_smearedZ0);
  m_eventTree->Branch("smearedPt",&m_smearedPt);
  m_eventTree->Branch("smearedEta",&m_smearedEta);
  m_eventTree->Branch("smearedPhi",&m_smearedPhi);
  m_eventTree->Branch("Z0",&m_Z0);

  m_eventTree->Branch("nCluster20",&m_nCluster20);
  m_eventTree->Branch("nCluster10",&m_nCluster10);
  m_eventTree->Branch("nCluster",&m_nCluster);
  m_eventTree->Branch("nElectrons", &m_nElectrons);

  m_nEvents = 0;
  m_nPassingOffline = 0 ;
  m_nPassningL1 = 0 ;
  resetVariables();

  m_nTruth = 0 ;
  m_nReco = 0 ;
  m_nTruthMatched = 0 ;
  m_nClusterMatched = 0 ;
  m_nClusterMatched10 = 0 ;
  m_nClusterMatched20 = 0 ;
  
=======
  m_eventTree->Branch("smearedDeltaZ4Truth", &m_smearedDeltaZ4Truth);

 m_eventTree->Branch("truthPt", &m_truthPt);
  m_eventTree->Branch("truthE", &m_truthE);
  m_eventTree->Branch("truthEta", &m_truthEta);

  m_eventTree->Branch("offlinePt", &m_offlinePt);
  m_eventTree->Branch("offlineE", &m_offlineE);
  m_eventTree->Branch("offlineEta", &m_offlineEta);

  m_eventTree->Branch("offlinePt_Loose", &m_offlinePt_Loose);
  m_eventTree->Branch("offlineE_Loose", &m_offlineE_Loose);
  m_eventTree->Branch("offlineEta_Loose", &m_offlineEta_Loose);

  m_eventTree->Branch("offlinePt_Medium", &m_offlinePt_Medium);
  m_eventTree->Branch("offlineE_Medium", &m_offlineE_Medium);
  m_eventTree->Branch("offlineEta_Medium", &m_offlineEta_Medium);

  m_eventTree->Branch("offlinePt_Tight", &m_offlinePt_Tight);
  m_eventTree->Branch("offlineE_Tight", &m_offlineE_Tight);
  m_eventTree->Branch("offlineEta_Tight", &m_offlineEta_Tight);

  m_eventTree->Branch("minDr_all", &m_minDr_all);
  m_eventTree->Branch("minDr_loose", &m_minDr_loose);
  m_eventTree->Branch("minDr_pt_all", &m_minDr_pt_all);
  m_eventTree->Branch("minDr_pt_loose", &m_minDr_pt_loose);
  m_eventTree->Branch("minDr_res_all", &m_minDr_res_all);
  m_eventTree->Branch("minDr_res_loose", &m_minDr_res_loose);

  m_eventTree->Branch("dR1", &m_dR1);
  m_eventTree->Branch("dR2", &m_dR2);
  m_eventTree->Branch("dR3", &m_dR3);
  m_eventTree->Branch("dZ", &m_dZ);
  m_fromZcount = 0;
  m_electronQualityPass = 0;
  m_nElectrons = 0;
  m_eventTree->Branch("dR_failing2EM10", &m_dR_failing2EM10);
  m_eventTree->Branch("subleadingSingleTruthPt", &m_subleadingSingleTruthPt);


>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  return StatusCode::SUCCESS;
}

StatusCode TwoEgammaEfficiency::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  ATH_MSG_INFO ("Number of  events = "<<m_nEvents);
  ATH_MSG_INFO ("Number of  events passing offline = "<<m_nPassingOffline);
<<<<<<< HEAD
  //ATH_MSG_INFO ("Number of  events passing offline(single) = "<<m_nPassingOfflineSingle);
  //ATH_MSG_INFO ("Number of  events passning 1L0 = "<<m_nPassing1L0) ;
  //ATH_MSG_INFO ("Number of  events passning 2L0 = "<<m_nPassing2L0) ;
  ATH_MSG_INFO ("Number of  events passning L1T = "<<m_nPassningL1) ;
  //ATH_MSG_INFO ("L0EM0 = "<<(m_nPassing1L0/m_nPassingOfflineSingle)*100<<"%") ;
  //ATH_MSG_INFO ("L02EM10 = "<<(m_nPassing2L0/m_nPassingOffline)*100<<"%") ;
  ATH_MSG_INFO ("L1T(offline) = "<<(m_nPassningL1/m_nPassingOffline)*100<<"%") ; 
  //ATH_MSG_INFO ("L1T(L0) = "<<(m_nPassningL1/m_nPassing2L0)*100<<"%") ; 
  //ATH_MSG_INFO ("L1T(offline)dz<0.4 = "<<(m_nPassningL1Z0_0p4/m_nPassingOffline)*100<<"%") ;
  //ATH_MSG_INFO ("L1T(offline)dz<0.3 = "<<(m_nPassningL1Z0_0p3/m_nPassingOffline)*100<<"%") ;
  //ATH_MSG_INFO ("L1T(offline)dz<0.2 = "<<(m_nPassningL1Z0_0p2/m_nPassingOffline)*100<<"%") ;
  //ATH_MSG_INFO ("L1T(offline)dz<0.1 = "<<(m_nPassningL1Z0_0p1/m_nPassingOffline)*100<<"%") ;

  //ATH_MSG_INFO ("electrons from Z = "<<m_fromZcount);
  //ATH_MSG_INFO ("electrons passing quality cut = "<<m_electronQualityPass<< "/"<<m_nElectrons);
  ATH_MSG_INFO ("Electron ID efficiency = "<<m_nReco<< "/"<<m_nTruth<<" = "<<((float)m_nReco/m_nTruth)*100<<"%");
  ATH_MSG_INFO ("Electrons matched = "<<m_nTruthMatched<< "/"<<m_nReco<<" = "<<((float)m_nTruthMatched/m_nReco)*100<<"%");
  ATH_MSG_INFO ("Clusters matched with et > 1e4 MeV = "<<m_nClusterMatched10<< "/"<<m_nClusterMatched<<" = "<<((float)m_nClusterMatched10/m_nClusterMatched)*100<<"%");
  ATH_MSG_INFO ("Clusters matched with et > 2e4 MeV = "<<m_nClusterMatched20<< "/"<<m_nClusterMatched <<" = "<<((float)m_nClusterMatched20/m_nClusterMatched)*100<<"%");  
  ATH_MSG_INFO ("N trk and smearing trk (Truth) = " <<m_nEqualTruth << "/" << m_nTotalTruth << " = " << float((float)m_nEqualTruth/(float)m_nTotalTruth)*100. <<"%");  
  ATH_MSG_INFO ("N trk and smearing trk (Offline) = " <<m_nEqualOffline << "/" << m_nTotalOffline << " = " << float((float)m_nEqualOffline/(float)m_nTotalOffline)*100. <<"%");
  ATH_MSG_INFO ("N trk and smearing trk (TruthSingle) = " <<m_nEqualTruthSingle << "/" << m_nTotalTruthSingle << " = " << float((float)m_nEqualTruthSingle/(float)m_nTotalTruthSingle)*100. <<"%");
  ATH_MSG_INFO ("N trk and smearing trk (OfflineSingle) = " <<m_nEqualOfflineSingle << "/" << m_nTotalOfflineSingle << " = " << float((float)m_nEqualOfflineSingle/(float)m_nTotalOfflineSingle)*100. <<"%");
=======
  ATH_MSG_INFO ("Number of  events passing offline(single) = "<<m_nPassingOfflineSingle);
  ATH_MSG_INFO ("Number of  events passning 1L0 = "<<m_nPassing1L0) ;
  ATH_MSG_INFO ("Number of  events passning 2L0 = "<<m_nPassing2L0) ;
  ATH_MSG_INFO ("Number of  events passning L1T = "<<m_nPassningL1) ;
  ATH_MSG_INFO ("L0EM0 = "<<(m_nPassing1L0/m_nPassingOfflineSingle)*100<<"%") ;
  ATH_MSG_INFO ("L02EM10 = "<<(m_nPassing2L0/m_nPassingOffline)*100<<"%") ;
  ATH_MSG_INFO ("L1T(offline) = "<<(m_nPassningL1/m_nPassingOffline)*100<<"%") ; 
  ATH_MSG_INFO ("L1T(L0) = "<<(m_nPassningL1/m_nPassing2L0)*100<<"%") ; 
  ATH_MSG_INFO ("L1T(offline)dz<0.4 = "<<(m_nPassningL1Z0_0p4/m_nPassingOffline)*100<<"%") ;
  ATH_MSG_INFO ("L1T(offline)dz<0.3 = "<<(m_nPassningL1Z0_0p3/m_nPassingOffline)*100<<"%") ;
  ATH_MSG_INFO ("L1T(offline)dz<0.2 = "<<(m_nPassningL1Z0_0p2/m_nPassingOffline)*100<<"%") ;
  ATH_MSG_INFO ("L1T(offline)dz<0.1 = "<<(m_nPassningL1Z0_0p1/m_nPassingOffline)*100<<"%") ;

  ATH_MSG_INFO ("electrons from Z = "<<m_fromZcount);
  ATH_MSG_INFO ("electrons passing quality cut = "<<m_electronQualityPass<< "/"<<m_nElectrons);

  ATH_MSG_INFO ("Eelctron ID efficiency = "<<m_nReco<< "/"<<m_nTruth<<" = "<<((float)m_nReco/m_nTruth)*100<<"[\%]");
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  
  m_eventTree->Write();
  return StatusCode::SUCCESS;
}

struct greaterEt {
    template<class T>
    bool operator()(T const& a, T const& b) const { return a->et() > b->et(); }
};

struct greaterPt {
    template<class T>
    bool operator()(T const& a, T const& b) const { return a->pt() > b->pt(); }
};

struct greaterTruthPt {
    template<class T>
    bool operator()(T const& a, T const& b) const { return a.truth->pt() > b.truth->pt(); }
};



StatusCode TwoEgammaEfficiency::execute() {  
  //ATH_MSG_DEBUG ("Executing " << name() << "...");
  m_nEvents++;
<<<<<<< HEAD
  
  std:: cout << "N Events: " << m_nEvents << std::endl ;
=======
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905

  const double m_EEtThr = 0; // Min ET, 5 GeV (was 20)
  const double m_rEtaThr =  0.745;
  const double m_EratioThr = 0.41;

  
  //Getting and filling containers
  const xAOD::TrackParticleContainer* tracks;
  if ( evtStore()->retrieve(tracks,"InDetTrackParticles").isFailure() ) {
    ATH_MSG_ERROR("did not find track particle container");
    return StatusCode::SUCCESS;
  }
<<<<<<< HEAD
  /*const xAOD::ElectronContainer* electrons;
  if ( evtStore()->retrieve(electrons,"Electrons").isFailure() ) {
    ATH_MSG_ERROR("did not find electrons container");
    return StatusCode::SUCCESS;
    }*/
  const xAOD::TruthParticleContainer* truth;
  if ( evtStore()->retrieve(truth,"TruthParticles").isFailure() ) {
    ATH_MSG_ERROR("did not find truth container");
    return StatusCode::SUCCESS;
  }

  //Getting pile-up info
  const xAOD::EventInfo* evtinfo ;
  if ( evtStore()->retrieve(evtinfo,"EventInfo").isFailure() ) {
    ATH_MSG_ERROR("did not find EventInfo");
    return StatusCode::SUCCESS;
  }
  
  //Get value associated with pile up for the event
  m_avIntPerCrossing = evtinfo->averageInteractionsPerCrossing(); 

  //std::cout << "Hi bro - " << evtinfo->actualInteractionsPerCrossing() << ' ' << evtinfo-> averageInteractionsPerCrossing() << ' ' << evtinfo->bcid() <<  std::endl ;
  
  //std::cout << "Size of tracks: " << tracks->size() << " of truth: " << truth->size() << " of electrons: " << electrons->size() << std::endl ;
  
=======
  const xAOD::ElectronContainer* electrons;
    if ( evtStore()->retrieve(electrons,"Electrons").isFailure() ) {
    ATH_MSG_ERROR("did not find electrons container");
    return StatusCode::SUCCESS;
  }
  const xAOD::TruthParticleContainer* truth;
    if ( evtStore()->retrieve(truth,"TruthParticles").isFailure() ) {
        ATH_MSG_ERROR("did not find electrons container");
        return StatusCode::SUCCESS;
  }
  
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  std::vector<const xAOD::TrigEMCluster*> egammaCandidate_Et;
  std::vector<const xAOD::TrigEMCluster*> egammaCandidate_ERatio;
  std::vector<const xAOD::TrigEMCluster*> egammaCandidate_rEta;
  std::vector<const xAOD::TrigEMCluster*> egammaPassed_Et;
  std::vector<const xAOD::TrigEMCluster*> egammaPassed_ERatio;
  std::vector<const xAOD::TrigEMCluster*> egammaPassed_rEta;
  std::vector<const xAOD::TrigEMCluster*> egammaPassed_ERatio_single;
  std::vector<const xAOD::TrigEMCluster*> egammaPassed_EM10;
  std::vector<const xAOD::TrigEMCluster*> egammaPassed_2EM10;
  std::vector<const xAOD::TrigEMCluster*> egammaPassed_EM20;
  std::vector<const xAOD::TrigEMCluster*> egammaPassed_2EM20;
  std::vector<const xAOD::TrigEMCluster*> egammaPassedOfflineMatched_ERatio;
  std::vector<const xAOD::TrigEMCluster*> egammaPassedOfflineMatched_ERatio_single;
  std::vector<const xAOD::Electron*> offlineElectrons;
  std::vector<const xAOD::Electron*> offlineElectrons_single;
  std::vector<const xAOD::Electron*> offlineElectronsMatched_single;
  std::vector<const xAOD::Electron*> offlineElectronsMatched;
  std::vector<const xAOD::TrackParticle*> matchedTracks;
  std::vector<const xAOD::TruthParticle*> truthMatched;
  std::vector<const xAOD::TruthParticle*> truthFromZ;

  std::map<const xAOD::TruthParticle*, const xAOD::Electron*> truth_offline_map;
  std::vector<truthOfflineContainer> truthMatchedOffline;


<<<<<<< HEAD
  //int nZ = 0;
  //int truthcount = 0;
  
  //std::map<int,int> mymap ;
  
=======
  int nZ = 0;
  int truthcount = 0;
  

>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  for(auto t: *truth){
    bool passTruth = false;
    bool passReco = false;
    if ( t->status() != 1 ) continue;
<<<<<<< HEAD
    
    /*if (m_nEvents==1){
      if(t->absPdgId()) mymap[t->absPdgId()]++ ;
      else mymap[t->absPdgId()] = 1 ;
      }*/
    if(fabs(t->absPdgId())!=11) continue;
#if SIGNAL_TEST == 1
    if(!isFromZ(t)) continue;
#endif
    if(fabsf(t->eta()) > 1.37 && fabsf(t->eta()) < 1.52 ) continue;
    if(fabsf(t->eta()) > 2.37) continue;

    passTruth = true;
    float electron_efficiency = getElectronEfficiency(t->pt(), t->eta(), "LHLoose");
    float random = m_random.Uniform();

    if(random < electron_efficiency ) { passReco = true;  }
=======
    if(fabs(t->absPdgId())!=11) continue;
    if(!isFromZ(t)) continue;
    if(fabsf(t->eta()) > 1.37 && fabsf(t->eta()) < 1.52 ) continue;
    if(fabsf(t->eta()) > 2.37) continue;
    // m_truthPt.push_back(t->pt()/1e3);
    passTruth = true;
    float electron_efficiency = getElectronEfficiency(t->pt(), t->eta(), "LHLoose");
    float random = m_random.Uniform();
    //<<"Efficiency = "<<electron_efficiency<<" random number = "<<random<<" pT = "<<t->pt()/1e3<<" eta = "<<t->eta()<<std::endl;
    if(random < electron_efficiency ) passReco = true;
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
    if(passTruth && !passReco){
      m_truthPt.push_back(t->pt()/1e3);
      m_truthEta.push_back(t->eta());

      continue;
    }
<<<<<<< HEAD


    //float min_dR = 999999.;
    float max_pt = -1.;
    //float max_pt_loose=-1;
    //float max_pt_medium=-1;
    //float max_pt_tight=-1;
    //float matchedElectronPt = -1;
    //float matchedElectronE = -1;
    //float matchedElectronEta = -1;
    //float matchedElectronPtReco = -1;
    //float matchedElectronEReco = -1;
    //float matchedElectronEtaReco = -1;

    //const xAOD::Electron* tempMatchedElectron_loose=0;
    const xAOD::TrackParticle* tempMatchedTrack = 0;
    const xAOD::TruthParticle * tempTruth_loose = 0;  
    //const xAOD::Electron* tempMatchedElectron_medium=0;
    //const xAOD::TruthParticle * tempTruth_medium = 0; 
    //const xAOD::Electron* tempMatchedElectron_tight=0;
    //const xAOD::TruthParticle * tempTruth_tight = 0; 
=======
    //std::cout<<"Truth passed. reco  = "<<passReco<<std::endl;

    m_nTruth++;

    float min_dR = 999999.;
    float max_pt = -1.;
    float max_pt_loose=-1;
    float max_pt_medium=-1;
    float max_pt_tight=-1;
    float mindR_all=999999;
    float mindR_loose=999999;
    float mindR_medium=999999;
    float mindR_tight=999999;
    float matchedElectronPt = -1;
    float matchedElectronE = -1;
    float matchedElectronEta = -1;
    float matchedElectronPtReco = -1;
    float matchedElectronEReco = -1;
    float matchedElectronEtaReco = -1;

    const xAOD::Electron* tempMatchedElectron_loose=0;
    const xAOD::TrackParticle* tempMatchedTrack  =0;
    const xAOD::TruthParticle * tempTruth_loose = 0;  
    const xAOD::Electron* tempMatchedElectron_medium=0;
    const xAOD::TruthParticle * tempTruth_medium = 0; 
    const xAOD::Electron* tempMatchedElectron_tight=0;
    const xAOD::TruthParticle * tempTruth_tight = 0; 
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
    bool passMatch = false;
    for(auto tr: *tracks){
      float t_pt = tr->pt()/1e3;
      float t_eta = tr->eta();
      float t_phi = tr->phi();

      float dR = deltaR2(t->eta(), t_eta, t->phi(), t_phi);
      if(dR < 0.1  && t_pt > max_pt){
        max_pt = t_pt;
        tempTruth_loose = t;
        tempMatchedTrack = tr;
        passMatch = true;
      }
    }
    if(passTruth && passReco && !passMatch){
      continue;
    }
    else {
      m_truthPt.push_back(tempTruth_loose->pt()/1e3);
      m_truthEta.push_back(tempTruth_loose->eta());
<<<<<<< HEAD
      m_offlinePt_Loose.push_back(tempMatchedTrack->pt()/1e3);
      m_offlineEta_Loose.push_back(tempMatchedTrack->eta());
=======
      m_offlinePt_Loose.push_back(tempTruth_loose->pt()/1e3);
      m_offlineEta_Loose.push_back(tempTruth_loose->eta());
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
    }


   }
<<<<<<< HEAD
  
=======

>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  if(!m_offlineOnly){

    const xAOD::TrigEMClusterContainer* scluster(nullptr);
    if ( evtStore()->retrieve(scluster,"SCluster").isFailure() ){
      ATH_MSG_ERROR("did not find super cluster container");
      return StatusCode::SUCCESS;
    }
    const xAOD::TrigEMClusterContainer* lASP(nullptr);
    if ( evtStore()->retrieve(lASP,"LArLayer1Vars").isFailure() ){
      ATH_MSG_ERROR("did not find Eratio cluster container");
      return StatusCode::SUCCESS;
    }
<<<<<<< HEAD
    
=======

>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
    for(auto t: *truth){
      bool passTruth = false;
      bool passReco = false;

      if ( t->status() != 1 ) continue;
<<<<<<< HEAD
      if(fabs(t->absPdgId())!=11) continue ;
#if SIGNAL_TEST == 1
      if(!isFromZ(t)) continue;
#endif
=======
      if(fabs(t->absPdgId())!=11) continue;
      if(!isFromZ(t)) continue;
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
      if(fabsf(t->eta()) > 1.37 && fabsf(t->eta()) < 1.52 ) continue;
      if(fabsf(t->eta()) > 2.37) continue;
      if(t->pt() < 800) continue;
      passTruth = true;
<<<<<<< HEAD
      
      float electron_efficiency = getElectronEfficiency(t->pt(), t->eta(), m_electronQuality);
      float random = m_random.Uniform();

      m_nTruth++; 

      if(random < electron_efficiency ) { passReco = true; m_nReco++; }
=======

      float electron_efficiency = getElectronEfficiency(t->pt(), t->eta(), m_electronQuality);
      float random = m_random.Uniform();
      if(random < electron_efficiency ) passReco = true;
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
      truthOfflineContainer t_container;
      if(passTruth && !passReco){
        t_container.truth = t; //truth e from Z
        truthMatchedOffline.push_back(t_container);
        continue;
      }

<<<<<<< HEAD
      m_nElectrons++;

      float min_dR = 999999. ;
      float max_dR = 999999. ;
=======

>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
      float max_pt = -1.;
      bool passMatch = false;
      const xAOD::TrackParticle *matchedTrack;
      for(auto tr: *tracks){
        float dR = deltaR2(t->eta(), tr->eta(), t->phi(), tr->phi());
<<<<<<< HEAD
#if GET_DELTAR == 1
	m_dRTrackTruth.push_back(dR) ;
#endif
=======
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
        if(dR < m_offlineTruthDrCut && tr->pt() > max_pt){
          max_pt = tr->pt();
          passMatch = true;
          matchedTrack = tr;
<<<<<<< HEAD
	  max_dR = dR ;
        }
	if( dR < min_dR ) min_dR = dR ;
      }
      m_dRMinTruthTrack.push_back(min_dR) ; //Get Minimum dR
=======
        }
      }

>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
      if(passTruth && passReco && !passMatch){
        continue;
      }
      else {
        t_container.track = matchedTrack;
        t_container.truth = t;
<<<<<<< HEAD
	m_nTruthMatched++ ;
      }
      m_dRMatchedTruthTrack.push_back(max_dR) ;
=======
      }
    
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
      truthMatchedOffline.push_back(t_container);

    }
    
    //std::cout<<"truth matched size = "<<truthMatchedOffline.size()<<std::endl;
    for( const auto cl : *scluster ){ //finding clusters which pass the L0 triggers
<<<<<<< HEAD
      //const float pt = cl->et()/1e3;
=======
      const float pt = cl->et()/1e3;
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
      if ( cl->et() < m_EEtThr ) continue;

      float rEta=-999.0;
      float eRatio=-999.0;

      if ( cl->e277() > 0.1 ) rEta= cl->e237() / cl->e277();
      if ( rEta < m_rEtaThr ) continue;
      egammaCandidate_rEta.push_back(cl);
      
      for( auto er : *lASP ) {
        if ( fabsf( er->eta() - cl->eta() ) > 0.035 ) continue;
        if ( deltaPhi1( er->phi(), cl->phi() ) > 0.1 ) continue;
        if ( er->emaxs1() + er->e2tsts1() > 0.1 ) eRatio = ( er->emaxs1() - er->e2tsts1() ) / (er->emaxs1() + er->e2tsts1() );
      }
    
      if ( eRatio < m_EratioThr ) continue;

      if (fabs(cl->eta()) > 2.47)continue; //set this eta cut in JO
      
      egammaCandidate_ERatio.push_back( cl );
<<<<<<< HEAD
      
      //Added to study energy of clusters
      m_nCluster++;
      if( cl->et()/1e3 > 10. ) {
	m_nCluster10++ ; 
	if( cl->et()/1e3 > 20. ) m_nCluster20++; 
      }
    }
    
    //Sorting the L0 candidated to find the leading and subleading - these are the two passing the 2EM10
    std::sort(egammaCandidate_ERatio.begin(), egammaCandidate_ERatio.end(), greaterEt()); //sorting the cluster candidates
    std::sort(egammaCandidate_rEta.begin(), egammaCandidate_rEta.end(), greaterEt());
    std::sort(egammaCandidate_Et.begin(), egammaCandidate_Et.end(), greaterEt());//Not implement

=======
    }

    //Sorting the L0 candidated to find the leading and subleading - these are the two passing the 2EM10
    std::sort(egammaCandidate_ERatio.begin(), egammaCandidate_ERatio.end(), greaterEt()); //sorting the cluster candidates
    std::sort(egammaCandidate_rEta.begin(), egammaCandidate_rEta.end(), greaterEt());
    std::sort(egammaCandidate_Et.begin(), egammaCandidate_Et.end(), greaterEt());
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
    double eRatioLead = 0, eRatioSublead = 0, eRatioRetaLead = 0, eRatioRetaSublead = 0, eRatioEtLead = 0, eRatioEtSublead = 0;

    if (egammaCandidate_ERatio.size() >= 1) eRatioLead = egammaCandidate_ERatio.at(0)->et()/1e3;
    if (egammaCandidate_ERatio.size() >= 2) eRatioSublead = egammaCandidate_ERatio.at(1)->et()/1e3;

    if (egammaCandidate_rEta.size() >= 1) eRatioRetaLead = egammaCandidate_rEta.at(0)->et()/1e3;
    if (egammaCandidate_rEta.size() >= 2) eRatioRetaSublead = egammaCandidate_rEta.at(1)->et()/1e3;

    if (egammaCandidate_Et.size() >= 1) eRatioEtLead = egammaCandidate_Et.at(0)->et()/1e3;
    if (egammaCandidate_Et.size() >= 2) eRatioEtSublead = egammaCandidate_Et.at(1)->et()/1e3;


    //std::cout<<"Number of cluster candidates before cut = "<<egammaCandidate_ERatio.size()<<std::endl;
    //std::cout<<"Clusters:"<<std::endl;
    

    //Are they both over 10GeV?
    
    //putting the clusters into 3 containers - EM10, EM20, 2EM10

    if( eRatioLead > 20. )egammaPassed_ERatio.push_back(egammaCandidate_ERatio.at(0));
    if (eRatioSublead > 10. )egammaPassed_ERatio.push_back(egammaCandidate_ERatio.at(1));
    
    if( eRatioRetaLead > 20. )egammaPassed_rEta.push_back(egammaCandidate_rEta.at(0));
    if (eRatioRetaSublead > 10. )egammaPassed_rEta.push_back(egammaCandidate_rEta.at(1));
    
    if( eRatioRetaLead > 20. )egammaPassed_rEta.push_back(egammaCandidate_rEta.at(0));
    if (eRatioRetaSublead > 10. )egammaPassed_rEta.push_back(egammaCandidate_rEta.at(1));
    
<<<<<<< HEAD
    //Uses Eratio
=======
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
    if(eRatioLead > 10){
      egammaPassed_EM10.push_back(egammaCandidate_ERatio.at(0));
      if(eRatioSublead > 0) egammaPassed_EM10.push_back(egammaCandidate_ERatio.at(1));
    }
    if(eRatioSublead > 10){
      egammaPassed_2EM10.push_back(egammaCandidate_ERatio.at(0));
      egammaPassed_2EM10.push_back(egammaCandidate_ERatio.at(1));
    }
    if(eRatioLead > 20){
      egammaPassed_EM20.push_back(egammaCandidate_ERatio.at(0));
      if(eRatioSublead > 0) egammaPassed_EM20.push_back(egammaCandidate_ERatio.at(1));
    } 
    if(eRatioSublead > 20){
      egammaPassed_2EM20.push_back(egammaCandidate_ERatio.at(0));
      egammaPassed_2EM20.push_back(egammaCandidate_ERatio.at(1));
    } 

<<<<<<< HEAD
    //std::cout<<"Number cluster candidates after cuts = "<<egammaPassed_ERatio.size()<<std::endl;

#if GET_DELTAR == 1
    //Get dR of Track and Cluster
    for(const auto tr:*tracks)
      for(auto &cl: egammaPassed_EM10){
	m_dRTrackCluster.push_back(deltaR(cl->eta(), tr->eta(), cl->phi(), tr->phi()));
      }
#endif
     

    //Sort by momentum of truth
    std::sort(truthMatchedOffline.begin(), truthMatchedOffline.end(), greaterTruthPt());
    
    //In case truths have the same track, pass to next event
    //Before passing MatchCluster
    if ( truthMatchedOffline.size()>1 && truthMatchedOffline.at(0).track && truthMatchedOffline.at(0).track == truthMatchedOffline.at(1).track ){
      truthMatchedOffline.at(0).track = 0 ;
      truthMatchedOffline.at(1).track = 0 ;
    }
    
    for(auto &tmo: truthMatchedOffline){ //matching clusters to offline electrons 
      if(tmo.truth==0)continue;
      //float min_dR_truth   = 999999.;
      //float min_dR_offline = 999999.;
      //float max_pt_truth   = -1.;
      //float max_pt_offline = -1.;
=======


  //std::cout<<"Number cluster candidates after cuts = "<<egammaPassed_ERatio.size()<<std::endl;


    for(auto &tmo: truthMatchedOffline){ //matching clusters to offline electrons 
      if(tmo.truth==0)continue;
      float min_dR_truth   = 999999.;
      float min_dR_offline = 999999.;
      float max_pt_truth   = -1.;
      float max_pt_offline = -1.;
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
      bool verbose=false;
      MatchCluster(tmo, egammaPassed_EM10, "EM10", verbose);
      MatchCluster(tmo, egammaPassed_EM20, "EM20", verbose);
      MatchCluster(tmo, egammaPassed_2EM10, "2EM10", verbose);
      MatchCluster(tmo, egammaPassed_2EM20, "2EM20", verbose);
<<<<<<< HEAD
      
      if( !tmo.cluster ) continue ;
      m_nClusterMatched++ ;
      if ( tmo.cluster && tmo.cluster->et() > 1e4 ) {
	m_nClusterMatched10++ ;
	if ( tmo.cluster->et() > 2e4 ) m_nClusterMatched20++ ; 
      }
    }
=======

    }
    std::sort(truthMatchedOffline.begin(), truthMatchedOffline.end(), greaterTruthPt());
    
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905

    if(truthMatchedOffline.size()> 1  && truthMatchedOffline.at(1).truth->pt()/1e3 > 50 && (!truthMatchedOffline.at(1).clusterMatched2EM10Offline || !truthMatchedOffline.at(0).clusterMatched2EM10Offline)){
      const xAOD::TruthParticle *truth_1 = truthMatchedOffline.at(0).truth;
      const xAOD::TruthParticle *truth_2 = truthMatchedOffline.at(1).truth;
      m_dR_failing2EM10 = deltaR2(truth_1->eta(), truth_2->eta(), truth_1->phi(), truth_2->phi());
      std::cout<<"Not passed 2EM10"<<std::endl;
      std::cout<<"dR(1,2) = "<<m_dR_failing2EM10<<std::endl;
      std::cout<<"pT(1) = "<<truth_1->pt()/1e3<<" pT(2) = "<<truth_2->pt()/1e3<<std::endl;

    }

    if(truthMatchedOffline.size()> 1 && truthMatchedOffline.at(1).truth->pt()/1e3 > 50 && ((truthMatchedOffline.at(0).clusterMatchedEM10Offline && !truthMatchedOffline.at(0).clusterMatched2EM10Offline)||(truthMatchedOffline.at(1).clusterMatchedEM10Offline && !truthMatchedOffline.at(1).clusterMatched2EM10Offline) ))  {
      std::cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<std::endl;
      const xAOD::TruthParticle *truth_1 = truthMatchedOffline.at(0).truth;
      const xAOD::TruthParticle *truth_2 = truthMatchedOffline.at(1).truth;
      const xAOD::TrackParticle *track_1 = truthMatchedOffline.at(0).track;
      const xAOD::TrackParticle *track_2 = truthMatchedOffline.at(1).track;
      std::cout<<"Truth: pT(1) = "<<truth_1->pt()/1e3<<" pT(2) = "<<truth_2->pt()/1e3<<std::endl;
      std::cout<<"Truth: eta(1) = "<<truth_1->eta()<<" eta(2) = "<<truth_2->eta()<<std::endl;
<<<<<<< HEAD
      if (track_1 && track_2){
	std::cout<<"Track: pT(1) = "<<track_1->pt()/1e3<<" pT(2) = "<<track_2->pt()/1e3<<std::endl;
	std::cout<<"Track: eta(1) = "<<track_1->eta()<<" eta(2) = "<<track_2->eta()<<std::endl;
      }
=======
      std::cout<<"Track: pT(1) = "<<track_1->pt()/1e3<<" pT(2) = "<<track_2->pt()/1e3<<std::endl;
      std::cout<<"Track: eta(1) = "<<track_1->eta()<<" eta(2) = "<<track_2->eta()<<std::endl;
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
      std::cout<<"EM10 (1) = "<<truthMatchedOffline.at(0).clusterMatchedEM10Offline<<" EM10 (2) = "<<truthMatchedOffline.at(0).clusterMatchedEM10Offline<<std::endl;
      std::cout<<"2EM10 (1) = "<<truthMatchedOffline.at(0).clusterMatched2EM10Offline<<" 2EM10 (2) = "<<truthMatchedOffline.at(0).clusterMatched2EM10Offline<<std::endl;
      std::cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<std::endl;
    }

    if(truthMatchedOffline.size()>0){
      float tr_pt  = truthMatchedOffline.at(0).truth->pt()/1e3;
      float tr_eta = truthMatchedOffline.at(0).truth->eta();


      m_leadingTruthPt  = tr_pt;
      m_leadingTruthEta = tr_eta;
    
      if(truthMatchedOffline.at(0).track!=0){ //is there a matched offline?
        m_nPassingOffline++;
        //if(truthMatchedOffline.at(0).track->pt()/1e3 > 20 && truthMatchedOffline.at(0).track->pt()/1e3 < 50)std::cout<<truthMatchedOffline.at(0).track->pt()/1e3<<std::endl;
        m_leadingOfflinePt = tr_pt;
        m_leadingOfflineEta = tr_eta;
        m_leadingOffline_offlinePt = truthMatchedOffline.at(0).track->pt()/1e3;
        m_leadingOffline_offlineEta = truthMatchedOffline.at(0).track->eta();
        //std::cout<<"offline pt = "<<truthMatchedOffline.at(0).track->pt()/1e3<<" clusterMatchedEM10Truth = "<<truthMatchedOffline.at(0).clusterMatchedEM10Truth<< std::endl;
      }

      if(truthMatchedOffline.at(0).cluster!=0){
        if(truthMatchedOffline.at(0).clusterMatchedEM10Truth){
          m_leadingL0EM10Truth_truthPt = tr_pt;
          m_leadingL0EM10Truth_truthEta = tr_eta;
        }
        if(truthMatchedOffline.at(0).clusterMatchedEM20Truth){
          m_leadingL0EM20Truth_truthPt = tr_pt;
          m_leadingL0EM20Truth_truthEta = tr_eta;
        }
        if(truthMatchedOffline.at(0).clusterMatchedEM10Offline){
          m_leadingL0EM10Offline_truthPt = tr_pt;
          m_leadingL0EM10Offline_truthEta = tr_eta;
          m_leadingL0EM10Offline_offlinePt = truthMatchedOffline.at(0).track->pt()/1e3;
          m_leadingL0EM10Offline_offlineEta = truthMatchedOffline.at(0).track->eta();
        }
        if(truthMatchedOffline.at(0).clusterMatchedEM20Offline){
          m_leadingL0EM20Offline_truthPt = tr_pt;
          m_leadingL0EM20Offline_truthEta = tr_eta;
          m_leadingL0EM20Offline_offlinePt = truthMatchedOffline.at(0).track->pt()/1e3;
          m_leadingL0EM20Offline_offlineEta = truthMatchedOffline.at(0).track->eta();
        }
      }
    }

    if(truthMatchedOffline.size()>1){
<<<<<<< HEAD
      //float tr_lead_pt     = truthMatchedOffline.at(0).truth->pt()/1e3;
      //float tr_lead_eta    = truthMatchedOffline.at(0).truth->eta();
=======
      float tr_lead_pt     = truthMatchedOffline.at(0).truth->pt()/1e3;
      float tr_lead_eta    = truthMatchedOffline.at(0).truth->eta();
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
      float tr_sublead_pt  = truthMatchedOffline.at(1).truth->pt()/1e3;
      float tr_sublead_eta = truthMatchedOffline.at(1).truth->eta();

      m_subleadingTruthPt  = tr_sublead_pt;
      m_subleadingTruthEta = tr_sublead_eta;

      if(truthMatchedOffline.at(1).cluster!=0){
        m_subleadingSingleTruthPt = tr_sublead_pt;
        if(truthMatchedOffline.at(0).clusterMatchedEM10Offline){
          m_subleadingL0EM10Offline_truthPt = tr_sublead_pt;
<<<<<<< HEAD
	  m_subleadingL0EM10Offline_truthEta = tr_sublead_eta;
=======

>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
        }

      }

      if(truthMatchedOffline.at(1).track!=0 && truthMatchedOffline.at(0).track!=0) { //is there a matched offline?
          m_subleadingOfflinePt  = tr_sublead_pt;
          m_subleadingOfflineEta = tr_sublead_eta;
          m_subleadingOffline_offlinePt = truthMatchedOffline.at(1).track->pt()/1e3;
          m_subleadingOffline_offlineEta = truthMatchedOffline.at(1).track->eta();
      }

      if(truthMatchedOffline.at(0).cluster!=0 && truthMatchedOffline.at(1).cluster!=0){
        
        if(truthMatchedOffline.at(1).clusterMatched2EM10Offline && truthMatchedOffline.at(0).clusterMatched2EM10Offline){
          m_subleadingL02EM10Offline_truthPt = tr_sublead_pt;
          m_subleadingL02EM10Offline_truthEta = tr_sublead_eta;
          m_subleadingL02EM10Offline_offlinePt  = truthMatchedOffline.at(1).track->pt()/1e3;
          m_subleadingL02EM10Offline_offlineEta = truthMatchedOffline.at(1).track->eta();
        }
        if(truthMatchedOffline.at(0).clusterMatched2EM10Offline && truthMatchedOffline.at(1).clusterMatched2EM10Offline){
          m_L02EM10Offline_offlinePt.push_back(truthMatchedOffline.at(0).track->pt()/1e3);
          m_L02EM10Offline_offlinePt.push_back(truthMatchedOffline.at(1).track->pt()/1e3);
<<<<<<< HEAD
	  m_L02EM10Offline_offlineEta.push_back(truthMatchedOffline.at(0).track->eta());
	  m_L02EM10Offline_offlineEta.push_back(truthMatchedOffline.at(1).track->eta());
=======
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
        }
        if(truthMatchedOffline.at(0).clusterMatched2EM10Truth && truthMatchedOffline.at(1).clusterMatched2EM10Truth){
          m_subleadingL02EM10Truth_truthPt = tr_sublead_pt;
          m_subleadingL02EM10Truth_truthEta = tr_sublead_eta;
        }

        if(truthMatchedOffline.at(1).clusterMatched2EM20Offline && truthMatchedOffline.at(0).clusterMatched2EM20Offline){
          m_subleadingL02EM20Offline_truthPt = tr_sublead_pt;
          m_subleadingL02EM20Offline_truthEta = tr_sublead_eta;
          m_subleadingL02EM20Offline_offlinePt  = truthMatchedOffline.at(1).track->pt()/1e3;
          m_subleadingL02EM20Offline_offlineEta = truthMatchedOffline.at(1).track->eta();
        }
        
        if(truthMatchedOffline.at(0).clusterMatched2EM20Truth && truthMatchedOffline.at(1).clusterMatched2EM20Truth){
            m_subleadingL02EM20Truth_truthPt = tr_sublead_pt;
            m_subleadingL02EM20Truth_truthEta = tr_sublead_eta;
          }
      }
    }
<<<<<<< HEAD
    
=======

>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
    //Track matching
    for(auto &tmo: truthMatchedOffline){
      if(tmo.cluster==0) continue; //only look at events with a matched cluster

<<<<<<< HEAD
      //float max_pt_truth                  = 0;
      //float max_pt_offline                = 0;
      float smeared_max_pt_truth          = 0;
      float smeared_max_pt_offline        = 0;
      //float max_pt_truth_single           = 0;
      //float max_pt_offline_single         = 0;
      float smeared_max_pt_truth_single   = 0;
      float smeared_max_pt_offline_single = 0;

      //Obtains track for get smeared track
      xAOD::TrackParticle *smearedTrk_truth = 0 , *smearedTrk_offline = 0 , 
	*smearedTrk_truth_single = 0 , *smearedTrk_offline_single = 0 ;

      //For the study of the appropriate dR
      float smeared_min_dR_truth = 999999. , smeared_min_dR_offline = 999999. , 
	smeared_min_dR_truth_single = 999999. , smeared_min_dR_offline_single = 999999. ;
      float smeared_matched_dR_truth = -1. , smeared_matched_dR_offline = -1. , 
	smeared_matched_dR_truth_single = -1. , smeared_matched_dR_offline_single = -1. ;
=======
      float max_pt_truth                  = 0;
      float max_pt_offline                = 0;
      float smeared_max_pt_truth          = 0;
      float smeared_max_pt_offline        = 0;
      float max_pt_truth_single           = 0;
      float max_pt_offline_single         = 0;
      float smeared_max_pt_truth_single   = 0;
      float smeared_max_pt_offline_single = 0;

>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
      for(auto trk: *tracks){
        bool matchedTruth                = false;
        bool matchedOffline              = false;
        bool smearedMatchedTruth         = false;
        bool smearedMatchedOffline       = false;
        bool matchedTruthSingle          = false;
        bool matchedOfflineSingle        = false;
        bool smearedMatchedTruthSingle   = false;
        bool smearedMatchedOfflineSingle = false;

<<<<<<< HEAD
        float trkPt = trk->pt()*0.001;
        float trkEta = trk->eta();
        float trkPhi = trk->phi();
        float trkZ0  = trk->z0();
	
=======
        float trkPt = trk->pt()* 0.001;
        float trkEta = trk->eta();
        float trkPhi = trk->phi();
        float trkZ0  = trk->z0();

>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
        static TRandom3 R;
        R.SetSeed( (int)(trk->pt() + 0.5) ); // in MeV
        double smearIPt = 0, smearEta = 0, smearPhi = 0, smearZ0 = 0;
        smearEta = R.Gaus( TTSmear::L1TTmean_reta_vs_eta(trkEta), TTSmear::L1TTsigma_reta(trkPt, trkEta) );
        smearPhi = R.Gaus( TTSmear::L1TTmean_rphi_vs_eta(trkEta), TTSmear::L1TTsigma_rphi(trkPt, trkEta) );
        smearIPt = R.Gaus( TTSmear::L1TTmean_ript_vs_eta(trkEta), TTSmear::L1TTsigma_ript(trkPt, trkEta) );
        smearZ0  = R.Gaus( TTSmear::L1TTmean_rzed_vs_eta(trkEta), TTSmear::L1TTsigma_rzed(trkPt, trkEta) );
<<<<<<< HEAD
	
=======
      
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
        float smearedTrkEta = trkEta + smearEta;
        float smearedTrkPhi = trkPhi + smearPhi;
        float smearedTrkPt  = 1. / ( smearIPt + (1./trkPt) );
        float smearedTrkZ0  = trkZ0 + smearZ0;
<<<<<<< HEAD
	//std::cout << trkPt <<' ' << smearedTrkPt << ' ' << tmo.truth->eta() << ' ' << trkEta << ' ' << smearedTrkEta << ' ' << tmo.truth->phi() << ' ' << trkPhi << ' ' << smearedTrkPhi << ' ' << R.Gaus( TTSmear::L1TTmean_rphi_vs_eta(trkEta), TTSmear::L1TTsigma_rphi(trkPt, trkEta) ) << std::endl ;
	
	float dR = deltaR2(tmo.truth->eta(), trkEta, tmo.truth->phi(), trkPhi);
        float smearedDR = deltaR2(tmo.truth->eta(), smearedTrkEta, tmo.truth->phi(), smearedTrkPhi); //Changed

        m_trackClusterDeltaR.push_back(dR);
	m_trackClusterSmearedDeltaR.push_back(smearedDR);
	
	if ( tmo.clusterMatched2EM10Truth ){
	  if(dR<smeared_min_dR_truth) smeared_min_dR_truth = dR ;
	  if(smearedTrkPt>smeared_max_pt_truth && smearedDR < m_trackConeDr && smearedTrkPt>m_trackPtCut ){
	    smeared_max_pt_truth = smearedTrkPt;
	    smearedMatchedTruth  = true;
	    smearedTrk_truth = (xAOD::TrackParticle*) trk ;
	    smeared_matched_dR_truth = dR ;
	  }
	}
	if(tmo.clusterMatched2EM10Offline){
	  if(dR<smeared_min_dR_offline) smeared_min_dR_offline = dR ;
	  if(smearedTrkPt>smeared_max_pt_offline && smearedDR<m_trackConeDr && smearedTrkPt>m_trackPtCut ){
	    smeared_max_pt_offline=smearedTrkPt;
	    smearedMatchedOffline = true;
	    smearedTrk_offline = (xAOD::TrackParticle*)trk ;
	    smeared_matched_dR_offline = dR ;
	  }
	}
	if(tmo.clusterMatchedEM10Truth){
	  if(dR<smeared_min_dR_truth_single) smeared_min_dR_truth_single = dR ;
	  if(smearedTrkPt>smeared_max_pt_truth_single && smearedDR<m_trackConeDr && smearedTrkPt>m_trackPtCut ){
	    smeared_max_pt_truth_single=smearedTrkPt;
	    smearedMatchedTruthSingle = true;
	    smearedTrk_truth_single = (xAOD::TrackParticle*) trk ;
	    smeared_matched_dR_truth_single = dR ;	    
	  }
	}
	if(tmo.clusterMatchedEM10Offline){
	  if(dR<smeared_min_dR_offline_single) smeared_min_dR_offline_single = dR ;
	  if(smearedTrkPt>smeared_max_pt_offline_single && smearedDR<m_trackConeDr && smearedTrkPt>m_trackPtCut ){
	    smeared_max_pt_offline_single=smearedTrkPt;
	    smearedMatchedOfflineSingle = true;
	    smearedTrk_offline_single = (xAOD::TrackParticle*) trk ;
	    smeared_matched_dR_offline_single = dR ;
	  }
	}
=======

 

        float dR = deltaR(tmo.cluster->eta(), trkEta, tmo.cluster->phi(), trkPhi);
        float smearedDR = deltaR(tmo.cluster->eta(), smearedTrkEta, tmo.cluster->phi(), smearedTrkPhi);

        m_trackClusterDeltaR.push_back(dR);

      

        

        if(smearedTrkPt>smeared_max_pt_truth && smearedDR<m_trackConeDr && smearedTrkPt>m_trackPtCut && tmo.clusterMatched2EM10Truth){
          smeared_max_pt_truth = smearedTrkPt;
          smearedMatchedTruth  = true;
        }
        if(smearedTrkPt>smeared_max_pt_offline && smearedDR<m_trackConeDr && smearedTrkPt>m_trackPtCut && tmo.clusterMatched2EM10Offline){
          smeared_max_pt_offline=smearedTrkPt;
          smearedMatchedOffline = true;
        }
        if(smearedTrkPt>smeared_max_pt_truth_single && smearedDR<m_trackConeDr && smearedTrkPt>m_trackPtCut && tmo.clusterMatchedEM10Truth){
          smeared_max_pt_truth_single=smearedTrkPt;
          smearedMatchedTruthSingle = true;
        }
        if(smearedTrkPt>smeared_max_pt_offline_single && smearedDR<m_trackConeDr && smearedTrkPt>m_trackPtCut && tmo.clusterMatchedEM10Offline){
          smeared_max_pt_offline_single=smearedTrkPt;
          smearedMatchedOfflineSingle = true;
        }
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905

        if(tmo.clusterMatched2EM10Truth){
          matchedTruth  = true;
        }
        if(tmo.clusterMatched2EM10Offline){
          matchedOffline = true;
        }
        if(tmo.clusterMatchedEM10Truth){
          matchedTruthSingle = true;
        }
        if(tmo.clusterMatchedEM10Offline){
          matchedOfflineSingle = true;
        }


        if(smearedMatchedOfflineSingle || smearedMatchedTruthSingle || smearedMatchedOffline || smearedMatchedTruth) {
          tmo.smearedTrackMatchedOffline       = smearedMatchedOffline;
          tmo.smearedTrackMatchedTruth         = smearedMatchedTruth;
          tmo.smearedTrackMatchedSingleOffline = smearedMatchedOfflineSingle;
          tmo.smearedTrackMatchedSingleTruth   = smearedMatchedTruthSingle;
          tmo.trackMatchedOffline              = matchedOffline;
          tmo.trackMatchedTruth                = matchedTruth;
          tmo.trackMatchedSingleOffline        = matchedOfflineSingle;
          tmo.trackMatchedSingleTruth          = matchedTruthSingle;
          tmo.smearedTrackPt                   = smearedTrkPt;
          tmo.smearedTrackEta                  = smearedTrkEta;
          tmo.smearedTrackPhi                  = smearedTrkPhi;
          tmo.smearedTrackZ0                   = smearedTrkZ0;
<<<<<<< HEAD

	  tmo.smearedTrack_truth               = smearedTrk_truth ;
	  tmo.smearedTrack_offline             = smearedTrk_offline ;
	  tmo.smearedTrack_truth_single        = smearedTrk_truth_single ;
	  tmo.smearedTrack_offline_single      = smearedTrk_offline_single ;
        }
        
      }
      
      if (tmo.track){
	m_Z0.push_back(tmo.track->z0()) ;
	m_Eta.push_back(tmo.track->eta()) ;
	m_Phi.push_back(tmo.track->phi()) ;
	m_Pt.push_back(tmo.track->pt()/1e3) ;
      }
      if (tmo.smearedTrackMatchedOffline ){
	m_smearedZ0.push_back(tmo.track->z0()) ;
	m_smearedEta.push_back(tmo.track->eta()) ;
	m_smearedPhi.push_back(tmo.track->phi()) ;
	m_smearedPt.push_back(tmo.track->pt()/1e3) ;
      }
	     
      //Check if the tracks are the same as the smearing tracks
      if ( !tmo.track ) continue ; //if it didn't find a track in the first place
      
      if ( tmo.clusterMatched2EM10Truth ) {
	m_nTotalTruth++ ;
	if ( tmo.smearedTrack_truth == tmo.track ) m_nEqualTruth++ ;
      }
      if ( tmo.clusterMatched2EM10Offline ){
	m_nTotalOffline++;
	if ( tmo.smearedTrack_offline == tmo.track ) m_nEqualOffline++ ;
      }
      if ( tmo.clusterMatchedEM10Truth ){
	m_nTotalTruthSingle++;
	if ( tmo.smearedTrack_truth_single == tmo.track ) m_nEqualTruthSingle++ ;
      }
      if ( tmo.clusterMatchedEM10Offline ){
	m_nTotalOfflineSingle++;
	if ( tmo.smearedTrack_offline_single == tmo.track ) m_nEqualOfflineSingle++ ;
      }

      //Get smeared dR
      if( tmo.smearedTrackMatchedTruth ){
	m_smearedDRMinTruthTrack_truth.push_back(smeared_min_dR_truth) ;
	m_smearedDRMatchedTruthTrack_truth.push_back(smeared_matched_dR_truth) ;
      }
      if( tmo.smearedTrackMatchedOffline ){
	m_smearedDRMinTruthTrack_offline.push_back(smeared_min_dR_offline) ;
	m_smearedDRMatchedTruthTrack_offline.push_back(smeared_matched_dR_offline) ;
      }
      if( tmo.smearedTrackMatchedSingleTruth ){
	m_smearedDRMinTruthTrack_truth_single.push_back(smeared_min_dR_truth_single) ;
	m_smearedDRMatchedTruthTrack_truth_single.push_back(smeared_matched_dR_truth_single) ;
      }
      if( tmo.smearedTrackMatchedSingleOffline ){
	m_smearedDRMinTruthTrack_offline_single.push_back(smeared_min_dR_offline_single) ;
	m_smearedDRMatchedTruthTrack_offline_single.push_back(smeared_matched_dR_offline_single) ;
      }

    }


    //In case truths have the same track for the smearing - code not efficient
    //Truth
    if ( truthMatchedOffline.size()>1 && truthMatchedOffline.at(1).smearedTrack_truth && truthMatchedOffline.at(0).smearedTrack_truth == truthMatchedOffline.at(1).smearedTrack_truth ){
      truthMatchedOffline.at(0).smearedTrack_truth = 0 ; 
      truthMatchedOffline.at(1).smearedTrack_truth = 0 ;
      truthMatchedOffline.at(0).smearedTrackMatchedTruth = false ;
      truthMatchedOffline.at(1).smearedTrackMatchedTruth = false ;
    }
    
    //Offline
    if ( truthMatchedOffline.size()>1 && truthMatchedOffline.at(1).smearedTrack_offline && truthMatchedOffline.at(0).smearedTrack_offline == truthMatchedOffline.at(1).smearedTrack_offline ){
      truthMatchedOffline.at(0).smearedTrack_offline = 0 ;
      truthMatchedOffline.at(1).smearedTrack_offline = 0 ;
      truthMatchedOffline.at(0).smearedTrackMatchedOffline = false ;
      truthMatchedOffline.at(1).smearedTrackMatchedOffline = false ;
    }    
    
=======
        }
        
      }
    }


>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
    if(truthMatchedOffline.size()>1){
      if(truthMatchedOffline.at(0).track!=0 && truthMatchedOffline.at(1).track!=0){
        float truth_subleading_pt           = truthMatchedOffline.at(1).truth->pt()/1e3;
        float truth_subleading_eta          = truthMatchedOffline.at(1).truth->eta();
        float truth_leading_pt              = truthMatchedOffline.at(0).truth->pt()/1e3;
        float truth_leading_eta             = truthMatchedOffline.at(0).truth->eta();
        float track_subleading_pt           = truthMatchedOffline.at(1).track->pt()/1e3;
        float track_subleading_eta          = truthMatchedOffline.at(1).track->eta();
        float smeared_track_subleading_pt   = truthMatchedOffline.at(1).smearedTrackPt;
        float smeared_track_subleading_eta  = truthMatchedOffline.at(1).smearedTrackEta;

<<<<<<< HEAD
#if SIGNAL_TEST == 1	
	m_ZMassTrack = getZMassTrack(truthMatchedOffline.at(0),truthMatchedOffline.at(1)) ;
#endif

	//Obtain values of deltaZ of smeared tracks for different pt
        if(truthMatchedOffline.at(0).smearedTrackMatchedTruth && truthMatchedOffline.at(1).smearedTrackMatchedTruth){
          float smearedTrk1_z = truthMatchedOffline.at(0).smearedTrackZ0;
          float smearedTrk2_z = truthMatchedOffline.at(1).smearedTrackZ0;
	  m_smearedDeltaZTruth = fabsf(smearedTrk1_z - smearedTrk2_z);

#if SIGNAL_TEST == 1 
	  m_smearedZMassTrack_truth = getSmearedZMassTrack(&truthMatchedOffline.at(0),&truthMatchedOffline.at(1),"Truth") ;
#endif

	  m_L1L0Truthsmeared_truthPt.push_back(truth_leading_pt);
	  m_L1L0Truthsmeared_truthPt.push_back(truth_subleading_pt);
	  m_L1L0Truthsmeared_truthEta.push_back(truth_leading_eta);
	  m_L1L0Truthsmeared_truthEta.push_back(truth_subleading_eta);

          if(smeared_track_subleading_pt > 1){
            m_smearedDeltaZ1Truth = m_smearedDeltaZTruth ;
            m_L1L0Truth1smeared_truthPt.push_back(truth_leading_pt);
            m_L1L0Truth1smeared_truthPt.push_back(truth_subleading_pt);
            m_L1L0Truth1smeared_truthEta.push_back(truth_leading_eta);
            m_L1L0Truth1smeared_truthEta.push_back(truth_subleading_eta);
          }
	  if(smeared_track_subleading_pt > 2){
            m_smearedDeltaZ2Truth = m_smearedDeltaZTruth ;
            m_L1L0Truth2smeared_truthPt.push_back(truth_leading_pt);
            m_L1L0Truth2smeared_truthPt.push_back(truth_subleading_pt);
            m_L1L0Truth2smeared_truthEta.push_back(truth_leading_eta);
            m_L1L0Truth2smeared_truthEta.push_back(truth_subleading_eta);
          }
          if(smeared_track_subleading_pt > 4){
            m_smearedDeltaZ4Truth = m_smearedDeltaZTruth ;
=======
        if(truthMatchedOffline.at(0).smearedTrackMatchedTruth && truthMatchedOffline.at(1).smearedTrackMatchedTruth){
          float smearedTrk1_z = truthMatchedOffline.at(0).smearedTrackZ0;
          float smearedTrk2_z = truthMatchedOffline.at(1).smearedTrackZ0;
          
          
          if(smeared_track_subleading_pt > 4){
            m_smearedDeltaZ4Truth = fabsf(smearedTrk1_z - smearedTrk2_z);
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
            m_L1L0Truth4smeared_truthPt.push_back(truth_leading_pt);
            m_L1L0Truth4smeared_truthPt.push_back(truth_subleading_pt);
            m_L1L0Truth4smeared_truthEta.push_back(truth_leading_eta);
            m_L1L0Truth4smeared_truthEta.push_back(truth_subleading_eta);
          }

        }


<<<<<<< HEAD
	//Obtain values of deltaZ of tracks for different pt (not smeared)
=======

>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
        if(truthMatchedOffline.at(0).trackMatchedTruth && truthMatchedOffline.at(1).trackMatchedTruth){
          float trk1_z = truthMatchedOffline.at(0).track->z0();
          float trk2_z = truthMatchedOffline.at(1).track->z0();
          m_deltaZTruth = fabsf(trk1_z - trk2_z);
<<<<<<< HEAD
	  
	  //F.C. - Removed repeated lines
=======


>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
          m_subleadingL1L0Truth_truthPt  = truth_subleading_pt;
          m_subleadingL1L0Truth_truthEta = truth_subleading_eta;
          m_subleadingL1L0Truth_trackPt  = track_subleading_pt;
          m_subleadingL1L0Truth_trackEta = track_subleading_eta;
<<<<<<< HEAD
          
          m_L1L0Truth_truthPt.push_back(truth_leading_pt);
          m_L1L0Truth_truthPt.push_back(truth_subleading_pt);
          m_L1L0Truth_truthEta.push_back(truth_leading_eta);
          m_L1L0Truth_truthEta.push_back(truth_subleading_eta);

=======
        
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
          if(track_subleading_pt>1) {
            m_deltaZ1Truth = fabsf(trk1_z - trk2_z);
            m_subleadingL1L0Truth1_truthPt = truth_subleading_pt;
            m_subleadingL1L0Truth1_truthEta = truth_subleading_eta;
            m_L1L0Truth1_truthPt.push_back(truth_leading_pt);
            m_L1L0Truth1_truthPt.push_back(truth_subleading_pt);
            m_L1L0Truth1_truthEta.push_back(truth_leading_eta);
            m_L1L0Truth1_truthEta.push_back(truth_subleading_eta);
          }

          if(track_subleading_pt>2){
            m_deltaZ2Truth = fabsf(trk1_z - trk2_z);
            m_subleadingL1L0Truth2_truthPt = truth_subleading_pt;
            m_subleadingL1L0Truth2_truthEta = truth_subleading_eta;
            m_L1L0Truth2_truthPt.push_back(truth_leading_pt);
            m_L1L0Truth2_truthPt.push_back(truth_subleading_pt);
            m_L1L0Truth2_truthEta.push_back(truth_leading_eta);
            m_L1L0Truth2_truthEta.push_back(truth_subleading_eta);

          } 
          if(track_subleading_pt>4) {
            m_deltaZ4Truth = fabsf(trk1_z - trk2_z);
            m_subleadingL1L0Truth4_truthPt = truth_subleading_pt;
            m_subleadingL1L0Truth4_truthEta = truth_subleading_eta;
            m_L1L0Truth4_truthPt.push_back(truth_leading_pt);
            m_L1L0Truth4_truthPt.push_back(truth_subleading_pt);
            m_L1L0Truth4_truthEta.push_back(truth_leading_eta);
            m_L1L0Truth4_truthEta.push_back(truth_subleading_eta);
          }
<<<<<<< HEAD
      
        }
	
	//Same but now offline
	//Smearing
=======

          m_subleadingL1L0Truth_truthPt  = truth_subleading_pt;
          m_subleadingL1L0Truth_truthEta = truth_subleading_eta;
          m_subleadingL1L0Truth_trackPt  = track_subleading_pt;
          m_subleadingL1L0Truth_trackEta = track_subleading_eta;
          

          m_L1L0Truth_truthPt.push_back(truth_leading_pt);
          m_L1L0Truth_truthPt.push_back(truth_subleading_pt);
          m_L1L0Truth_truthEta.push_back(truth_leading_eta);
          m_L1L0Truth_truthEta.push_back(truth_subleading_eta);

      
        }

>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
        if(truthMatchedOffline.at(0).smearedTrackMatchedOffline && truthMatchedOffline.at(1).smearedTrackMatchedOffline){
          float smearedTrk1_z = truthMatchedOffline.at(0).smearedTrackZ0;
          float smearedTrk2_z = truthMatchedOffline.at(1).smearedTrackZ0;
          float leading_offline_pt = truthMatchedOffline.at(0).track->pt()/1e3;
          float subleading_offline_pt = truthMatchedOffline.at(1).track->pt()/1e3;
          float leading_offline_eta = truthMatchedOffline.at(0).track->eta();
          float subleading_offline_eta = truthMatchedOffline.at(1).track->eta();
<<<<<<< HEAD
	  
	  m_smearedDeltaZ = fabs(smearedTrk1_z - smearedTrk2_z);

#if SIGNAL_TEST == 1
	  m_smearedZMassTrack_offline = getSmearedZMassTrack(&truthMatchedOffline.at(0),&truthMatchedOffline.at(1),"Offline") ;
#endif
	  
	  m_subleadingL1L0OfflineSmeared_truthPt = truth_subleading_pt;
	  m_subleadingL1L0OfflineSmeared_truthEta = truth_subleading_eta;
	  m_L1L0Offlinesmeared_offlinePt.push_back(leading_offline_pt);
	  m_L1L0Offlinesmeared_offlinePt.push_back(subleading_offline_pt);
	  m_L1L0Offlinesmeared_offlineEta.push_back(leading_offline_eta);
	  m_L1L0Offlinesmeared_offlineEta.push_back(subleading_offline_eta);

	  //Set Data for smeared tracks
	  if(smeared_track_subleading_pt>1) {
            m_smearedDeltaZ1 = m_smearedDeltaZ ;
            m_subleadingL1L0OfflineSmeared1_truthPt = truth_subleading_pt;
            m_subleadingL1L0OfflineSmeared1_truthEta = truth_subleading_eta;
            m_L1L0Offline1smeared_offlinePt.push_back(leading_offline_pt);
            m_L1L0Offline1smeared_offlinePt.push_back(subleading_offline_pt);
            m_L1L0Offline1smeared_offlineEta.push_back(leading_offline_eta);
            m_L1L0Offline1smeared_offlineEta.push_back(subleading_offline_eta);

          }
	  if(smeared_track_subleading_pt>2) {
            m_smearedDeltaZ2 = m_smearedDeltaZ ;
            m_subleadingL1L0OfflineSmeared2_truthPt = truth_subleading_pt;
            m_subleadingL1L0OfflineSmeared2_truthEta = truth_subleading_eta;
            m_L1L0Offline2smeared_offlinePt.push_back(leading_offline_pt);
            m_L1L0Offline2smeared_offlinePt.push_back(subleading_offline_pt);
            m_L1L0Offline2smeared_offlineEta.push_back(leading_offline_eta);
            m_L1L0Offline2smeared_offlineEta.push_back(subleading_offline_eta);

          }
	  if(smeared_track_subleading_pt>4) {
            m_smearedDeltaZ4 = m_smearedDeltaZ ;
=======
           if(smeared_track_subleading_pt>4) {
            m_smearedDeltaZ4 = fabs(smearedTrk1_z - smearedTrk2_z);
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
            m_subleadingL1L0OfflineSmeared4_truthPt = truth_subleading_pt;
            m_subleadingL1L0OfflineSmeared4_truthEta = truth_subleading_eta;
            m_L1L0Offline4smeared_offlinePt.push_back(leading_offline_pt);
            m_L1L0Offline4smeared_offlinePt.push_back(subleading_offline_pt);
            m_L1L0Offline4smeared_offlineEta.push_back(leading_offline_eta);
            m_L1L0Offline4smeared_offlineEta.push_back(subleading_offline_eta);

          }
        }

        if(truthMatchedOffline.at(0).trackMatchedOffline && truthMatchedOffline.at(1).trackMatchedOffline){
          
          float trk1_z = truthMatchedOffline.at(0).track->z0();
          float trk2_z = truthMatchedOffline.at(1).track->z0();
          float leading_offline_pt = truthMatchedOffline.at(0).track->pt()/1e3;
          float subleading_offline_pt = truthMatchedOffline.at(1).track->pt()/1e3;
          float leading_offline_eta = truthMatchedOffline.at(0).track->eta();
          float subleading_offline_eta = truthMatchedOffline.at(1).track->eta();

<<<<<<< HEAD
#if SIGNAL_TEST == 1
	  m_ZMassTrackCluster = getZMassTrackCluster(truthMatchedOffline.at(0),truthMatchedOffline.at(1)) ;
	  m_smearedZMassTrack_same = getSmearedZMassTrack(&truthMatchedOffline.at(0),&truthMatchedOffline.at(1),"Same") ;
#endif

          m_deltaZ = fabsf(trk1_z - trk2_z);

          m_subleadingL1L0Offline_truthPt  = truth_subleading_pt;
          m_subleadingL1L0Offline_truthEta = truth_subleading_eta;
          m_subleadingL1L0Offline_trackPt  = track_subleading_pt;
          m_subleadingL1L0Offline_trackEta = track_subleading_eta;
          m_subleadingL1L0Offline_offlinePt  = truthMatchedOffline.at(1).track->pt()/1e3;
          m_subleadingL1L0Offline_offlineEta = truthMatchedOffline.at(1).track->eta();

          m_L1L0Offline_offlinePt.push_back(leading_offline_pt);
          m_L1L0Offline_offlinePt.push_back(subleading_offline_pt);
          m_L1L0Offline_offlineEta.push_back(leading_offline_eta);
          m_L1L0Offline_offlineEta.push_back(subleading_offline_eta);

          if(track_subleading_pt>1) {
            m_deltaZ1 = m_deltaZ ;
=======
          m_deltaZ = fabsf(trk1_z - trk2_z);

          if(track_subleading_pt>1) {
            m_deltaZ1 = fabsf(trk1_z - trk2_z);
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
            m_subleadingL1L0Offline1_truthPt = truth_subleading_pt;
            m_subleadingL1L0Offline1_truthEta = truth_subleading_eta;
            m_L1L0Offline1_offlinePt.push_back(leading_offline_pt);
            m_L1L0Offline1_offlinePt.push_back(subleading_offline_pt);
            m_L1L0Offline1_offlineEta.push_back(leading_offline_eta);
            m_L1L0Offline1_offlineEta.push_back(subleading_offline_eta);
          }

          if(track_subleading_pt>2){
<<<<<<< HEAD
            m_deltaZ2 = m_deltaZ ;
=======
            m_deltaZ2 = fabsf(trk1_z - trk2_z);
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
            m_subleadingL1L0Offline2_truthPt = truth_subleading_pt;
            m_subleadingL1L0Offline2_truthEta = truth_subleading_eta;
            m_L1L0Offline2_offlinePt.push_back(leading_offline_pt);
            m_L1L0Offline2_offlinePt.push_back(subleading_offline_pt);
            m_L1L0Offline2_offlineEta.push_back(leading_offline_eta);
            m_L1L0Offline2_offlineEta.push_back(subleading_offline_eta);

          } 
          if(track_subleading_pt>4) {
<<<<<<< HEAD
            m_deltaZ4 = m_deltaZ ;
=======
            m_deltaZ4 = fabsf(trk1_z - trk2_z);
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
            m_subleadingL1L0Offline4_truthPt = truth_subleading_pt;
            m_subleadingL1L0Offline4_truthEta = truth_subleading_eta;
            m_L1L0Offline4_offlinePt.push_back(leading_offline_pt);
            m_L1L0Offline4_offlinePt.push_back(subleading_offline_pt);
            m_L1L0Offline4_offlineEta.push_back(leading_offline_eta);
            m_L1L0Offline4_offlineEta.push_back(subleading_offline_eta);
          }

<<<<<<< HEAD
=======
         

          m_subleadingL1L0Offline_truthPt  = truth_subleading_pt;
          m_subleadingL1L0Offline_truthEta = truth_subleading_eta;
          m_subleadingL1L0Offline_trackPt  = track_subleading_pt;
          m_subleadingL1L0Offline_trackEta = track_subleading_eta;
          m_subleadingL1L0Offline_offlinePt  = truthMatchedOffline.at(1).track->pt()/1e3;
          m_subleadingL1L0Offline_offlineEta = truthMatchedOffline.at(1).track->eta();

          m_L1L0Offline_offlinePt.push_back(leading_offline_pt);
          m_L1L0Offline_offlinePt.push_back(subleading_offline_pt);
          m_L1L0Offline_offlineEta.push_back(leading_offline_eta);
          m_L1L0Offline_offlineEta.push_back(subleading_offline_eta);

>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905

        }
      }
    }

    if(truthMatchedOffline.size()>0){
      if(truthMatchedOffline.at(0).track!=0){
        float truth_leading_pt  = truthMatchedOffline.at(0).truth->pt()/1e3;
        float truth_leading_eta = truthMatchedOffline.at(0).truth->eta();
        float track_leading_pt  = truthMatchedOffline.at(0).smearedTrackPt;
        float track_leading_eta = truthMatchedOffline.at(0).smearedTrackEta;

        if(truthMatchedOffline.at(0).trackMatchedSingleTruth){
          m_leadingL1L0Truth_truthPt  = truth_leading_pt;
          m_leadingL1L0Truth_truthEta = truth_leading_eta;
          m_leadingL1L0Truth_trackPt  = track_leading_pt;
          m_leadingL1L0Truth_trackEta = track_leading_eta;
        }
        if(truthMatchedOffline.at(0).trackMatchedSingleOffline){
          m_nPassningL1++;
          //if(truthMatchedOffline.at(0).track->pt()/1e3 > 20 && truthMatchedOffline.at(0).track->pt()/1e3 < 50)std::cout<<truthMatchedOffline.at(0).track->pt()/1e3<<std::endl;
          m_leadingL1L0Offline_truthPt  = truth_leading_pt;
          m_leadingL1L0Offline_truthEta = truth_leading_eta;
          m_leadingL1L0Offline_trackPt  = track_leading_pt;
          m_leadingL1L0Offline_trackEta = track_leading_eta;
          m_leadingL1L0Offline_offlinePt  = truthMatchedOffline.at(0).track->pt()/1e3;
          m_leadingL1L0Offline_offlineEta = truthMatchedOffline.at(0).track->eta();
        }
        if(truthMatchedOffline.at(0).smearedTrackMatchedSingleTruth){
          if(track_leading_pt > 4){
            m_leadingL1L0TruthSmeared4_truthPt = truth_leading_pt;
            m_leadingL1L0TruthSmeared4_truthEta = truth_leading_eta;
          }
        }
        if(truthMatchedOffline.at(0).smearedTrackMatchedSingleOffline){
          if(track_leading_pt > 4){
            m_L1L0Offline4smeared_offlinePt.push_back(truthMatchedOffline.at(0).track->pt()/1e3);
            m_L1L0Offline4smeared_offlineEta.push_back(truthMatchedOffline.at(0).track->eta());
          }
        } 
      }
    } 
  
  }
<<<<<<< HEAD
 
  /*for (auto& x: mymap) {
    std::cout << x.first << ": " << x.second << '\n';
    }*/
=======
  







>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  
  m_eventTree->Fill();
  resetVariables();
  return StatusCode::SUCCESS;
}


<<<<<<< HEAD
bool TwoEgammaEfficiency::isParentFromZ(const xAOD::TruthParticle * particle){
  const xAOD::TruthVertex* prodvtx = particle->prodVtx();
  if(prodvtx==0) return false;
  //if(particle->isPhoton()) return false;
  const xAOD::TruthParticle * parent  = prodvtx->incomingParticle(0);
  if(parent==0){
    return false;
  }
  else if(parent->isZ()){
    return true;
  }
  else {
    return isParentFromZ(parent);
  }
  
}

=======
 bool TwoEgammaEfficiency::isParentFromZ(const xAOD::TruthParticle * particle){
    const xAOD::TruthVertex* prodvtx = particle->prodVtx();
    if(prodvtx==0) return false;
    //if(particle->isPhoton()) return false;
    const xAOD::TruthParticle * parent  = prodvtx->incomingParticle(0);
      if(parent==0){
        return false;
      }
      else if(parent->isZ()){
        return true;
      }
      else {
        return isParentFromZ(parent);
      }

  }
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
bool TwoEgammaEfficiency::isLastChild(const xAOD::TruthParticle * particle){
  int nChildren = particle->nChildren();
  if(nChildren>0) return false;
  else return true;
}

bool TwoEgammaEfficiency::isFromZ(const xAOD::TruthParticle *truth){
  bool isfromz = false;
  
  const xAOD::TruthVertex* prodvtx = truth->prodVtx();
  if(prodvtx==0)return false;
<<<<<<< HEAD
  const xAOD::TruthParticle * parent = prodvtx->incomingParticle(0);
  if(parent==0)return false;
  if(parent->isZ()) isfromz =  true;
=======
  const xAOD::TruthParticle * parent  = prodvtx->incomingParticle(0);
  if(parent==0)return false;
  if(parent->isZ())isfromz =  true;
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  else isfromz = false;
  return isfromz;
}

<<<<<<< HEAD
#if SIGNAL_TEST == 1
//Assumes it has match-uses track
float TwoEgammaEfficiency::getZMassTrack(const truthOfflineContainer& c1 , const truthOfflineContainer& c2){
  const FourMom_t P1 = c1.track->p4() , P2 = c2.track->p4() ;
  const FourMom_t P = P1+P2 ;
  return P.Mag()*0.001 ;
}

//Uses energy from cluster
float TwoEgammaEfficiency::getZMassTrackCluster(const truthOfflineContainer& c1 , const truthOfflineContainer& c2){
  const FourMom_t P1 = c1.track->p4() , P2 = c2.track->p4() ;
  const FourMom_t P = P1+P2 ;
  const double E = c1.cluster->energy()+c2.cluster->energy() ;
  const double m2 = E*E-P.P() ;
  
  if (m2>0) return sqrt(m2)*0.001 ;
  else return -sqrt(m2)*0.001 ;
}

//Assumes it has match - uses track
float TwoEgammaEfficiency::getSmearedZMassTrack(truthOfflineContainer* c1 , truthOfflineContainer* c2, std::string mode ){
  xAOD::TrackParticle *mytrack1 , *mytrack2 ;
  truthOfflineContainer ctc1 , ctc2 ;

  //Copies track used for the smearing
  if ( mode == "Truth" ){ 
    mytrack1 = new xAOD::TrackParticle() ; mytrack2 = new xAOD::TrackParticle() ;
    *mytrack1 = *c1->smearedTrack_truth ; *mytrack2 = *c2->smearedTrack_truth ;
  }
  else if ( mode == "Offline" ){ 
    mytrack1 = new xAOD::TrackParticle() ; mytrack2 = new xAOD::TrackParticle() ;
    *mytrack1 = *c1->smearedTrack_offline ; *mytrack2 = *c2->smearedTrack_offline ;
  }
  else if ( mode == "Same" ){
    mytrack1 = new xAOD::TrackParticle() ; mytrack2 = new xAOD::TrackParticle() ;
    
    ctc1.getChangedCopy(c1);
    ctc2.getChangedCopy(c2);

    c1 = &ctc1 ; c2 = &ctc2 ;

    *mytrack1 = *c1->track ; *mytrack2 = *c2->track ;
    
  }
  else { std::cout << "\nINVALID OPTION FOR getSmearedZMassTrack\n" ; return 0. ; }
  
  const float p1 = sqrt(PR(c1->smearedTrackPt*1000)+PR((mytrack1->p4()).Pz())) ,
    p2 = sqrt(PR(c2->smearedTrackPt*1000)+PR((mytrack2->p4()).Pz())) ;

  //Defining new parameters to new tracks
  mytrack1->setDefiningParameters(mytrack1->d0(),c1->smearedTrackZ0,c1->smearedTrackPhi,getThetaFromEta(c1->smearedTrackEta),mytrack1->charge()/p1) ;
  mytrack2->setDefiningParameters(mytrack2->d0(),c2->smearedTrackZ0,c2->smearedTrackPhi,getThetaFromEta(c2->smearedTrackEta),mytrack2->charge()/p2) ;

  const FourMom_t P1(mytrack1->p4()), P2(mytrack2->p4()), P = P1+P2 ;
  
  delete mytrack1 ;
  delete mytrack2 ;

  return P.Mag()*0.001 ;

}

#endif

void TwoEgammaEfficiency::resetVariables(){
  //Variables not used were commented
  
  
  m_leadingTruthPt=DEFAULT_PT;
  m_leadingTruthEta=DEFAULT_ETA;
  m_leadingOfflinePt=DEFAULT_PT;
  m_leadingOfflineEta=DEFAULT_ETA;
  m_leadingOffline_offlinePt=DEFAULT_PT;
  m_leadingOffline_offlineEta=DEFAULT_ETA;
  //L0 efficiency wrt truth
  //m_leadingL0Truth_truthPt=DEFAULT_PT;
  //m_leadingL0Truth_truthEta=DEFAULT_ETA;
  m_leadingL0EM10Truth_truthPt=DEFAULT_PT;
  m_leadingL0EM10Truth_truthEta=DEFAULT_ETA;
  m_leadingL0EM20Truth_truthPt=DEFAULT_PT;
  m_leadingL0EM20Truth_truthEta=DEFAULT_ETA;
  //m_leadingL02EM10Truth_truthPt=DEFAULT_PT;
  //m_leadingL02EM10Truth_truthEta=DEFAULT_ETA;
  //L0 efficiency wrt offline
  //m_leadingL0Offline_truthPt=DEFAULT_PT;
  //m_leadingL0Offline_truthEta=DEFAULT_ETA;
  //m_leadingL0Offline_offlinePt=DEFAULT_PT;
  //m_leadingL0Offline_offlineEta=DEFAULT_ETA;
  m_leadingL0EM10Offline_truthPt=DEFAULT_PT;
  m_leadingL0EM10Offline_truthEta=DEFAULT_ETA;
  m_leadingL0EM20Offline_truthPt=DEFAULT_PT;
  m_leadingL0EM20Offline_truthEta=DEFAULT_ETA;
  //m_leadingL02EM10Offline_truthPt=DEFAULT_PT;
  //m_leadingL02EM10Offline_truthEta=DEFAULT_ETA;
  m_leadingL0EM10Offline_offlinePt=DEFAULT_PT;
  m_leadingL0EM10Offline_offlineEta=DEFAULT_ETA;
  m_leadingL0EM20Offline_offlinePt=DEFAULT_PT;
  m_leadingL0EM20Offline_offlineEta=DEFAULT_ETA;
  //m_leadingL02EM10Offline_offlinePt=-1;
  //m_leadingL02EM10Offline_offlineEta=-5;
  //Double electron histograms

  //Offline efficiency
  m_subleadingTruthPt=DEFAULT_PT;
  m_subleadingTruthEta=DEFAULT_ETA;
  m_subleadingOfflinePt=DEFAULT_PT;
  m_subleadingOfflineEta=DEFAULT_ETA;
  m_subleadingOffline_offlinePt=DEFAULT_PT;
  m_subleadingOffline_offlineEta=DEFAULT_ETA;
  //L0 efficiency wrt truth
  //m_subleadingL0Truth_truthPt=-1;
  //m_subleadingL0Truth_truthEta=-5;
  //m_subleadingL0EM10Truth_truthPt=DEFAULT_PT;
  //m_subleadingL0EM10Truth_truthEta=DEFAULT_ETA;
  //m_subleadingL0EM20Truth_truthPt=DEFAULT_PT;
  //m_subleadingL0EM20Truth_truthEta=DEFAULT_ETA;
  m_subleadingL02EM10Truth_truthPt=DEFAULT_PT;
  m_subleadingL02EM10Truth_truthEta=DEFAULT_ETA;
  m_subleadingL02EM20Truth_truthPt=DEFAULT_PT;
  m_subleadingL02EM20Truth_truthEta=DEFAULT_ETA;
  //L0 efficiency wrt offline
  //m_subleadingL0Offline_truthPt=-1;
  //m_subleadingL0Offline_truthEta=-5;
  //m_subleadingL0Offline_offlinePt=-1;
  //m_subleadingL0Offline_offlineEta=-5;
  m_subleadingL0EM10Offline_truthPt=DEFAULT_PT;
  m_subleadingL0EM10Offline_truthEta=DEFAULT_ETA;
  //m_subleadingL0EM20Offline_truthPt=DEFAULT_PT;
  //m_subleadingL0EM20Offline_truthEta=DEFAULT_ETA;
  m_subleadingL02EM10Offline_truthPt=DEFAULT_PT;
  m_subleadingL02EM10Offline_truthEta=DEFAULT_ETA;
  m_subleadingL02EM20Offline_truthPt=DEFAULT_PT;
  m_subleadingL02EM20Offline_truthEta=DEFAULT_ETA;
  //m_subleadingL0EM10Offline_offlineEta=0;
  //m_subleadingL0EM20Offline_offlinePt=-1;
  //m_subleadingL0EM20Offline_offlineEta=-5;
  m_subleadingL02EM10Offline_offlinePt=DEFAULT_PT;
  m_subleadingL02EM10Offline_offlineEta=DEFAULT_ETA;
  m_subleadingL02EM20Offline_offlinePt=DEFAULT_PT;
  m_subleadingL02EM20Offline_offlineEta=DEFAULT_ETA;
  //L1 efficiency wrt L0(truth-matched) - leading pT
  m_leadingL1L0Truth_truthPt=DEFAULT_PT;
  m_leadingL1L0Truth_truthEta=DEFAULT_ETA;
  m_leadingL1L0Truth_trackPt=DEFAULT_PT;
  m_leadingL1L0Truth_trackEta=DEFAULT_ETA;

  //m_leadingL1L0Truth1_trackPt=-1;
  //m_leadingL1L0Truth1_trackEta=-5;
  //m_leadingL1L0Truth2_trackPt=-1;
  //m_leadingL1L0Truth2_trackEta=-5;
  //m_leadingL1L0Truth4_trackPt=-1;
  //m_leadingL1L0Truth4_trackEta=-5;

  //m_leadingL1L0Truth1_truthPt=-1;
  //m_leadingL1L0Truth1_truthEta=-5;
  //m_leadingL1L0Truth2_truthPt=-1;
  //m_leadingL1L0Truth2_truthEta=-5;
  //m_leadingL1L0Truth4_truthPt=-1;
  //m_leadingL1L0Truth4_truthEta=-5;

  //m_leadingL1L0TruthSmeared4_trackPt=-1;
  //m_leadingL1L0TruthSeared4_trackEta=-5;
  m_leadingL1L0TruthSmeared4_truthPt=DEFAULT_PT;
  m_leadingL1L0TruthSmeared4_truthEta=DEFAULT_ETA;


  //L1 efficiency wrt L0(offline-matched) - leading pT
  m_leadingL1L0Offline_truthPt=DEFAULT_PT;
  m_leadingL1L0Offline_truthEta=DEFAULT_ETA;
  m_leadingL1L0Offline_offlinePt=DEFAULT_PT;
  m_leadingL1L0Offline_offlineEta=DEFAULT_ETA;
  m_leadingL1L0Offline_trackPt=DEFAULT_PT;
  m_leadingL1L0Offline_trackEta=DEFAULT_ETA;

  //m_leadingL1L0Offline1_trackPt=-1;
  //m_leadingL1L0Offline1_trackEta=-5;
  //m_leadingL1L0Offline2_trackPt=-1;
  //m_leadingL1L0Offline2_trackEta=-5;
  //m_leadingL1L0Offline4_trackPt=-1;
  //m_leadingL1L0Offline4_trackEta=-5;

  //m_leadingL1L0Offline1_truthPt=-1;
  //m_leadingL1L0Offline1_truthEta=-5;
  //m_leadingL1L0Offline2_truthPt=-1;
  //m_leadingL1L0Offline2_truthEta=-5;
  //m_leadingL1L0Offline4_truthPt=-1;
  //m_leadingL1L0Offline4_truthEta=-5;

  //m_leadingL1L0OfflineSmeared4_trackPt=-1;
  //m_leadingL1L0OfflineSeared4_trackEta=-5;
  //m_leadingL1L0OfflineSmeared4_truthPt=-1;
  //m_leadingL1L0OfflineSmeared4_truthEta=-5;


  //L1 efficiency wrt L0(truth-matched) - subleading pT
  m_subleadingL1L0Truth_truthPt=DEFAULT_PT;
  m_subleadingL1L0Truth_truthEta=DEFAULT_ETA;
  m_subleadingL1L0Truth_trackPt=DEFAULT_PT;
  m_subleadingL1L0Truth_trackEta=DEFAULT_ETA;

  m_subleadingL1L0Truth1_truthPt=DEFAULT_PT;
  m_subleadingL1L0Truth1_truthEta=DEFAULT_ETA;
  m_subleadingL1L0Truth2_truthPt=DEFAULT_PT;
  m_subleadingL1L0Truth2_truthEta=DEFAULT_ETA;
  m_subleadingL1L0Truth4_truthPt=DEFAULT_PT;
  m_subleadingL1L0Truth4_truthEta=DEFAULT_ETA;



  //m_subleadingL1L0TruthSmeared4_truthPt=-1;
  //m_subleadingL1L0TruthSmeared4_truthEta=-5;
  //L1 efficiency wrt L0(offline-matched) - subleading pT
  m_subleadingL1L0Offline_truthPt=DEFAULT_PT;
  m_subleadingL1L0Offline_truthEta=DEFAULT_ETA;
  m_subleadingL1L0Offline_offlinePt=DEFAULT_PT;
  m_subleadingL1L0Offline_offlineEta=DEFAULT_ETA;
  m_subleadingL1L0Offline_trackPt=DEFAULT_PT;
  m_subleadingL1L0Offline_trackEta=DEFAULT_ETA;
=======
void TwoEgammaEfficiency::resetVariables(){


  m_minDr_eta=0;
  m_minDr_phi=0;
  m_minDr_pt=0;



  m_leadingTruthPt=0;
  m_leadingTruthEta=0;
  m_leadingOfflinePt=0;
  m_leadingOfflineEta=0;
  m_leadingOffline_offlinePt=0;
  m_leadingOffline_offlineEta=0;
  //L0 efficiency wrt truth
  m_leadingL0Truth_truthPt=0;
  m_leadingL0Truth_truthEta=0;
  m_leadingL0EM10Truth_truthPt=0;
  m_leadingL0EM10Truth_truthEta=0;
  m_leadingL0EM20Truth_truthPt=0;
  m_leadingL0EM20Truth_truthEta=0;
  m_leadingL02EM10Truth_truthPt=0;
  m_leadingL02EM10Truth_truthEta=0;
  //L0 efficiency wrt offline
  m_leadingL0Offline_truthPt=0;
  m_leadingL0Offline_truthEta=0;
  m_leadingL0Offline_offlinePt=0;
  m_leadingL0Offline_offlineEta=0;
  m_leadingL0EM10Offline_truthPt=0;
  m_leadingL0EM10Offline_truthEta=0;
  m_leadingL0EM20Offline_truthPt=0;
  m_leadingL0EM20Offline_truthEta=0;
  m_leadingL02EM10Offline_truthPt=0;
  m_leadingL02EM10Offline_truthEta=0;
  m_leadingL0EM10Offline_offlinePt=0;
  m_leadingL0EM10Offline_offlineEta=0;
  m_leadingL0EM20Offline_offlinePt=0;
  m_leadingL0EM20Offline_offlineEta=0;
  m_leadingL02EM10Offline_offlinePt=0;
  m_leadingL02EM10Offline_offlineEta=0;
  //Double electron histograms

  //Offline efficiency
  m_subleadingTruthPt=0;
  m_subleadingTruthEta=0;
  m_subleadingOfflinePt=0;
  m_subleadingOfflineEta=0;
  m_subleadingOffline_offlinePt=0;
  m_subleadingOffline_offlineEta=0;
  //L0 efficiency wrt truth
  m_subleadingL0Truth_truthPt=0;
  m_subleadingL0Truth_truthEta=0;
  m_subleadingL0EM10Truth_truthPt=0;
  m_subleadingL0EM10Truth_truthEta=0;
  m_subleadingL0EM20Truth_truthPt=0;
  m_subleadingL0EM20Truth_truthEta=0;
  m_subleadingL02EM10Truth_truthPt=0;
  m_subleadingL02EM10Truth_truthEta=0;
  m_subleadingL02EM20Truth_truthPt=0;
  m_subleadingL02EM20Truth_truthEta=0;
  //L0 efficiency wrt offline
  m_subleadingL0Offline_truthPt=0;
  m_subleadingL0Offline_truthEta=0;
  m_subleadingL0Offline_offlinePt=0;
  m_subleadingL0Offline_offlineEta=0;
  m_subleadingL0EM10Offline_truthPt=0;
  m_subleadingL0EM10Offline_truthEta=0;
  m_subleadingL0EM20Offline_truthPt=0;
  m_subleadingL0EM20Offline_truthEta=0;
  m_subleadingL02EM10Offline_truthPt=0;
  m_subleadingL02EM10Offline_truthEta=0;
  m_subleadingL02EM20Offline_truthPt=0;
  m_subleadingL02EM20Offline_truthEta=0;
  m_subleadingL0EM10Offline_offlineEta=0;
  m_subleadingL0EM20Offline_offlinePt=0;
  m_subleadingL0EM20Offline_offlineEta=0;
  m_subleadingL02EM10Offline_offlinePt=0;
  m_subleadingL02EM10Offline_offlineEta=0;
  m_subleadingL02EM20Offline_offlinePt=0;
  m_subleadingL02EM20Offline_offlineEta=0;
  //L1 efficiency wrt L0(truth-matched) - leading pT
  m_leadingL1L0Truth_truthPt=0;
  m_leadingL1L0Truth_truthEta=0;
  m_leadingL1L0Truth_trackPt=0;
  m_leadingL1L0Truth_trackEta=0;

  m_leadingL1L0Truth1_trackPt=0;
  m_leadingL1L0Truth1_trackEta=0;
  m_leadingL1L0Truth2_trackPt=0;
  m_leadingL1L0Truth2_trackEta=0;
  m_leadingL1L0Truth4_trackPt=0;
  m_leadingL1L0Truth4_trackEta=0;

  m_leadingL1L0Truth1_truthPt=0;
  m_leadingL1L0Truth1_truthEta=0;
  m_leadingL1L0Truth2_truthPt=0;
  m_leadingL1L0Truth2_truthEta=0;
  m_leadingL1L0Truth4_truthPt=0;
  m_leadingL1L0Truth4_truthEta=0;

  m_leadingL1L0TruthSmeared4_trackPt=0;
  m_leadingL1L0TruthSeared4_trackEta=0;
  m_leadingL1L0TruthSmeared4_truthPt=0;
  m_leadingL1L0TruthSmeared4_truthEta=0;


  //L1 efficiency wrt L0(offline-matched) - leading pT
  m_leadingL1L0Offline_truthPt=0;
  m_leadingL1L0Offline_truthEta=0;
  m_leadingL1L0Offline_offlinePt=0;
  m_leadingL1L0Offline_offlineEta=0;
  m_leadingL1L0Offline_trackPt=0;
  m_leadingL1L0Offline_trackEta=0;

  m_leadingL1L0Offline1_trackPt=0;
  m_leadingL1L0Offline1_trackEta=0;
  m_leadingL1L0Offline2_trackPt=0;
  m_leadingL1L0Offline2_trackEta=0;
  m_leadingL1L0Offline4_trackPt=0;
  m_leadingL1L0Offline4_trackEta=0;

  m_leadingL1L0Offline1_truthPt=0;
  m_leadingL1L0Offline1_truthEta=0;
  m_leadingL1L0Offline2_truthPt=0;
  m_leadingL1L0Offline2_truthEta=0;
  m_leadingL1L0Offline4_truthPt=0;
  m_leadingL1L0Offline4_truthEta=0;

  m_leadingL1L0OfflineSmeared4_trackPt=0;
  m_leadingL1L0OfflineSeared4_trackEta=0;
  m_leadingL1L0OfflineSmeared4_truthPt=0;
  m_leadingL1L0OfflineSmeared4_truthEta=0;


  //L1 efficiency wrt L0(truth-matched) - subleading pT
  m_subleadingL1L0Truth_truthPt=0;
  m_subleadingL1L0Truth_truthEta=0;
  m_subleadingL1L0Truth_trackPt=0;
  m_subleadingL1L0Truth_trackEta=0;

  m_subleadingL1L0Truth1_truthPt=0;
  m_subleadingL1L0Truth1_truthEta=0;
  m_subleadingL1L0Truth2_truthPt=0;
  m_subleadingL1L0Truth2_truthEta=0;
  m_subleadingL1L0Truth4_truthPt=0;
  m_subleadingL1L0Truth4_truthEta=0;



  m_subleadingL1L0TruthSmeared4_truthPt=0;
  m_subleadingL1L0TruthSmeared4_truthEta=0;
  //L1 efficiency wrt L0(offline-matched) - subleading pT
  m_subleadingL1L0Offline_truthPt=0;
  m_subleadingL1L0Offline_truthEta=0;
  m_subleadingL1L0Offline_offlinePt=0;
  m_subleadingL1L0Offline_offlineEta=0;
  m_subleadingL1L0Offline_trackPt=0;
  m_subleadingL1L0Offline_trackEta=0;
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905




<<<<<<< HEAD
  //m_subleadingL1L0Offline1_trackPt=-1;
  //m_subleadingL1L0Offline1_trackEta=-5;
  //m_subleadingL1L0Offline2_trackPt=-1;
  //m_subleadingL1L0Offline2_trackEta=-5;
  //m_subleadingL1L0Offline4_trackPt=-1;
  //m_subleadingL1L0Offline4_trackEta=-5;

  m_subleadingL1L0Offline1_truthPt=DEFAULT_PT;
  m_subleadingL1L0Offline1_truthEta=DEFAULT_ETA;
  m_subleadingL1L0Offline2_truthPt=DEFAULT_PT;
  m_subleadingL1L0Offline2_truthEta=DEFAULT_ETA;
  m_subleadingL1L0Offline4_truthPt=DEFAULT_PT;
  m_subleadingL1L0Offline4_truthEta=DEFAULT_ETA;

  //m_subleadingL1L0OfflineSmeared4_trackPt=-1;
  //m_subleadingL1L0OfflineSeared4_trackEta=-5;

  m_subleadingL1L0OfflineSmeared_truthPt=DEFAULT_PT;
  m_subleadingL1L0OfflineSmeared_truthEta=DEFAULT_ETA;
  m_subleadingL1L0OfflineSmeared1_truthPt=DEFAULT_PT;
  m_subleadingL1L0OfflineSmeared1_truthEta=DEFAULT_ETA;
  m_subleadingL1L0OfflineSmeared2_truthPt=DEFAULT_PT;
  m_subleadingL1L0OfflineSmeared2_truthEta=DEFAULT_ETA;
  m_subleadingL1L0OfflineSmeared4_truthPt=DEFAULT_PT;
  m_subleadingL1L0OfflineSmeared4_truthEta=DEFAULT_ETA;


  m_subleadingSingleTruthPt=DEFAULT_OTHER;
=======
  m_subleadingL1L0Offline1_trackPt=0;
  m_subleadingL1L0Offline1_trackEta=0;
  m_subleadingL1L0Offline2_trackPt=0;
  m_subleadingL1L0Offline2_trackEta=0;
  m_subleadingL1L0Offline4_trackPt=0;
  m_subleadingL1L0Offline4_trackEta=0;

  m_subleadingL1L0Offline1_truthPt=0;
  m_subleadingL1L0Offline1_truthEta=0;
  m_subleadingL1L0Offline2_truthPt=0;
  m_subleadingL1L0Offline2_truthEta=0;
  m_subleadingL1L0Offline4_truthPt=0;
  m_subleadingL1L0Offline4_truthEta=0;

  m_subleadingL1L0OfflineSmeared4_trackPt=0;
  m_subleadingL1L0OfflineSeared4_trackEta=0;

  m_subleadingL1L0OfflineSmeared4_truthPt=0;
  m_subleadingL1L0OfflineSmeared4_truthEta=0;


  m_subleadingSingleTruthPt=-1;
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  
  m_L1L0Offline_offlinePt.clear();
  m_L1L0Offline_offlineEta.clear();
  m_L1L0Offline1_offlinePt.clear();
  m_L1L0Offline1_offlineEta.clear();
  m_L1L0Offline2_offlinePt.clear();
  m_L1L0Offline2_offlineEta.clear();
  m_L1L0Offline4_offlinePt.clear();
  m_L1L0Offline4_offlineEta.clear();
<<<<<<< HEAD
  m_L1L0Offlinesmeared_offlinePt.clear();
  m_L1L0Offlinesmeared_offlineEta.clear();
  m_L1L0Offline1smeared_offlinePt.clear();
  m_L1L0Offline1smeared_offlineEta.clear();
  m_L1L0Offline2smeared_offlinePt.clear();
  m_L1L0Offline2smeared_offlineEta.clear();
=======
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  m_L1L0Offline4smeared_offlinePt.clear();
  m_L1L0Offline4smeared_offlineEta.clear();

  m_L02EM10Offline_offlinePt.clear();
  m_L02EM10Offline_offlineEta.clear();



  m_L02EM10Truth_truthPt.clear();
  m_L02EM10Truth_truthEta.clear();

  m_L1L0Truth_truthPt.clear();
  m_L1L0Truth_truthEta.clear();
  m_L1L0Truth1_truthPt.clear();
  m_L1L0Truth1_truthEta.clear();
  m_L1L0Truth2_truthPt.clear();
  m_L1L0Truth2_truthEta.clear();
  m_L1L0Truth4_truthPt.clear();
  m_L1L0Truth4_truthEta.clear();
<<<<<<< HEAD
  m_L1L0Truthsmeared_truthPt.clear();
  m_L1L0Truthsmeared_truthEta.clear();
  m_L1L0Truth1smeared_truthPt.clear();
  m_L1L0Truth1smeared_truthEta.clear();
  m_L1L0Truth2smeared_truthPt.clear();
  m_L1L0Truth2smeared_truthEta.clear();
=======
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  m_L1L0Truth4smeared_truthPt.clear();
  m_L1L0Truth4smeared_truthEta.clear();

  m_trackClusterDeltaR.clear();
<<<<<<< HEAD
  m_trackClusterSmearedDeltaR.clear();
  //m_truthOfflineDeltaR.clear();
  //m_clusterOfflineDeltaR.clear();
  //m_clusterTruthDeltaR.clear();
  m_truthPt.clear();
  //m_truthE.clear();
  m_truthEta.clear();
  //m_offlinePt.clear();
  //m_offlineE.clear();
  //m_offlineEta.clear();


  m_offlinePt_Loose.clear();
  //m_offlineE_Loose.clear();
  m_offlineEta_Loose.clear();

  //m_offlinePt_Medium.clear();
  //m_offlineE_Medium.clear();
  //m_offlineEta_Medium.clear(); 

  //m_offlinePt_Tight.clear();
  //m_offlineE_Tight.clear();
  //m_offlineEta_Tight.clear(); 

  m_deltaZ=DEFAULT_OTHER;
  m_deltaZ1=DEFAULT_OTHER;
  m_deltaZ2=DEFAULT_OTHER;
  m_deltaZ4=DEFAULT_OTHER;
  m_smearedDeltaZ=DEFAULT_OTHER;
  m_smearedDeltaZ1=DEFAULT_OTHER;
  m_smearedDeltaZ2=DEFAULT_OTHER;
  m_smearedDeltaZ4=DEFAULT_OTHER;

  m_deltaZTruth=DEFAULT_OTHER;
  m_deltaZ1Truth=DEFAULT_OTHER;
  m_deltaZ2Truth=DEFAULT_OTHER;
  m_deltaZ4Truth=DEFAULT_OTHER;
  m_smearedDeltaZTruth=DEFAULT_OTHER;
  m_smearedDeltaZ1Truth=DEFAULT_OTHER;
  m_smearedDeltaZ2Truth=DEFAULT_OTHER;
  m_smearedDeltaZ4Truth=DEFAULT_OTHER;

#if SIGNAL_TEST == 1
  //Mass of Boson
  m_ZMassTrack = DEFAULT_OTHER ;
  m_ZMassTrackCluster = DEFAULT_OTHER ;
  m_smearedZMassTrack_truth = DEFAULT_OTHER ;
  m_smearedZMassTrack_offline = DEFAULT_OTHER ;
  m_smearedZMassTrack_same = DEFAULT_OTHER ;
#endif
  
  m_dR_failing2EM10=DEFAULT_OTHER;

  //More data to study
  m_nElectrons = 0 ;
#if GET_DELTAR == 1
  m_dRTrackTruth.clear() ;
  m_dRTrackCluster.clear();
#endif
  
  //Clear dR vectors
  m_dRMinTruthTrack.clear() ;
  m_dRMinTrackCluster.clear() ;
  m_dRMatchedTruthTrack.clear() ;
  m_dRMatchedTrackCluster.clear() ;

  m_smearedDRMinTruthTrack_truth.clear() ;
  m_smearedDRMinTruthTrack_offline.clear() ;
  m_smearedDRMinTruthTrack_truth_single.clear() ;
  m_smearedDRMinTruthTrack_offline_single.clear() ;

  m_smearedDRMatchedTruthTrack_truth.clear() ;
  m_smearedDRMatchedTruthTrack_offline.clear() ;
  m_smearedDRMatchedTruthTrack_truth_single.clear() ;
  m_smearedDRMatchedTruthTrack_offline_single.clear() ;

  m_Z0.clear();
  m_Phi.clear();
  m_Eta.clear();
  m_Pt.clear();
  m_smearedZ0.clear();
  m_smearedPhi.clear();
  m_smearedEta.clear();
  m_smearedPt.clear();

  m_nCluster = 0 ;
  m_nCluster10 = 0 ;
  m_nCluster20 = 0 ;
  
=======
  m_truthOfflineDeltaR.clear();
  m_clusterOfflineDeltaR.clear();
  m_clusterTruthDeltaR.clear();
  m_truthPt.clear();
  m_truthE.clear();
  m_truthEta.clear();
  m_offlinePt.clear();
  m_offlineE.clear();
  m_offlineEta.clear();


  m_offlinePt_Loose.clear();
  m_offlineE_Loose.clear();
  m_offlineEta_Loose.clear();

  m_offlinePt_Medium.clear();
  m_offlineE_Medium.clear();
  m_offlineEta_Medium.clear(); 

  m_offlinePt_Tight.clear();
  m_offlineE_Tight.clear();
  m_offlineEta_Tight.clear(); 

  m_minDr_all=-1;
  m_minDr_loose=-1;
  m_minDr_pt_all=-1;
  m_minDr_pt_loose=-1;
  m_minDr_res_all=-1;
  m_minDr_res_loose=-1;

  m_deltaZ=-1;
  m_deltaZ1=-1;
  m_deltaZ2=-1;
  m_deltaZ4=-1;
  m_smearedDeltaZ4=-1;

  m_deltaZTruth=-1;
  m_deltaZ1Truth=-1;
  m_deltaZ2Truth=-1;
  m_deltaZ4Truth=-1;
  m_smearedDeltaZ4Truth=-1;


  m_dR_failing2EM10=-1;

  m_dR1=-1;
  m_dR2=-1;
  m_dR3=-1;
  m_dZ=-1;
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
}


void TwoEgammaEfficiency::MatchCluster(truthOfflineContainer &tmo, std::vector<const xAOD::TrigEMCluster*> cluster, std::string trigger, bool verbose){
  float min_dR_truth   = 999999.;
  float min_dR_offline = 999999.;
<<<<<<< HEAD
  float max_dR_offline = 999999.;
  float max_pt_truth   = -1.;
  float max_pt_offline = -1.;
  bool aux = trigger == "EM10" ;

  for(auto &egp: cluster){
    float dR = deltaR2(egp->eta(), tmo.truth->eta(), egp->phi(), tmo.truth->phi());
=======
  float max_pt_truth   = -1.;
  float max_pt_offline = -1.;
  for(auto &egp: cluster){
    float dR = deltaR(egp->eta(), tmo.truth->eta(), egp->phi(), tmo.truth->phi());
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
    float egp_pt = egp->et();
    bool matchedTruth = false;
    bool matchedOffline = false;

    if(dR<0.1 && egp_pt > max_pt_truth){
      min_dR_truth = dR;
      max_pt_truth = egp_pt;
      matchedTruth = true;
    }

    if(tmo.track!=0){
<<<<<<< HEAD
      dR = deltaR2(egp->eta(), tmo.track->eta(), egp->phi(), tmo.track->phi());
      if(verbose) {
        std::cout<<"Cluster: eta = "<<egp->eta()<<" phi = "<<egp->phi()<<" pt = "<<egp->et()/1e3<<" dR = "<<dR<<std::endl;
      }
      if ( aux && dR < min_dR_offline ) min_dR_offline = dR ; //Changed so it could be bigger than 0.1
      if(dR<0.1 && egp_pt > max_pt_offline){
	max_dR_offline = dR ;
=======
      dR = deltaR(egp->eta(), tmo.track->eta(), egp->phi(), tmo.track->phi());
      if(verbose) {
        std::cout<<"Cluster: eta = "<<egp->eta()<<" phi = "<<egp->phi()<<" pt = "<<egp->et()/1e3<<" dR = "<<dR<<std::endl;
      }
      
      if(dR<0.1 && egp_pt > max_pt_offline){
        min_dR_offline=dR;
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
        max_pt_offline = egp_pt;
        matchedOffline = true;
      }
    }
    if(matchedTruth || matchedOffline){
      if(trigger == "EM10"){
        tmo.clusterMatchedEM10Offline = matchedOffline;
        tmo.clusterMatchedEM10Truth   = matchedTruth;
      }
      if(trigger == "EM20"){
        tmo.clusterMatchedEM20Offline = matchedOffline;
        tmo.clusterMatchedEM20Truth   = matchedTruth;
      }
      if(trigger == "2EM10"){
        tmo.clusterMatched2EM10Offline = matchedOffline;
        tmo.clusterMatched2EM10Truth   = matchedTruth;
      }
      if(trigger == "2EM20"){
        tmo.clusterMatched2EM20Offline = matchedOffline;
        tmo.clusterMatched2EM20Truth   = matchedTruth;
      }
      tmo.cluster = egp;
    }
  }
<<<<<<< HEAD
  if( aux && cluster.size() && tmo.track ) {
    m_dRMinTrackCluster.push_back(min_dR_offline) ;
    if(tmo.clusterMatchedEM10Offline) m_dRMatchedTrackCluster.push_back(max_dR_offline) ;
  }
=======
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
}




float TwoEgammaEfficiency::getElectronEfficiency(float ptMeV, float eta, std::string quality) 
//*********************************************************************
//---- Get the electron ID efficiency
{

  float ptGeV = ptMeV / 1000.;

  //---- define all the different efficiency maps
  const int nEtBins=6;
  const int netaBins=9;
 
  //---- Loose efficiencies
  const float ElEff_Step1p6_mu200_Loose[nEtBins][netaBins] = {
    {0.811966, 0.157915, 0.774224, 0.255651, 0.683, 0.673, 0.496, 0.549, 0.593 }, // Et[  7 -  15]
    {0.848566, 0.181479, 0.826112, 0.288839, 0.660, 0.736, 0.447, 0.622, 0.650 }, // Et[ 15 -  20]
    {0.880800, 0.196373, 0.857302, 0.276088, 0.676, 0.783, 0.475, 0.704, 0.714 }, // Et[ 20 -  30]
    {0.911897, 0.203653, 0.886948, 0.294688, 0.624, 0.831, 0.482, 0.766, 0.766 }, // Et[ 30 -  50]
    {0.929496, 0.222480, 0.907982, 0.294609, 0.705, 0.851, 0.537, 0.808, 0.812 }, // Et[ 50 -  80]
    {0.942924, 0.240126, 0.924407, 0.273356, 0.649, 0.855, 0.702, 0.890, 0.800 }, // Et[ 80 - 120]
  };
  //---- Medium efficiencies
  const float ElEff_Step1p6_mu200_Medium[nEtBins][netaBins] = { // Uses Tight Gold mu=200 for forward electrons
    {0.711072, 0.121269, 0.686265, 0.222915, 0.390, 0.494, 0.311, 0.343, 0.394 }, // Et[  7 -  15]
    {0.770084, 0.149739, 0.738592, 0.246606, 0.476, 0.538, 0.298, 0.395, 0.470 }, // Et[ 15 -  20]
    {0.806082, 0.152595, 0.778103, 0.253627, 0.497, 0.598, 0.301, 0.467, 0.495 }, // Et[ 20 -  30]
    {0.837797, 0.174947, 0.817937, 0.281170, 0.478, 0.652, 0.311, 0.538, 0.556 }, // Et[ 30 -  50]
    {0.904484, 0.202508, 0.876919, 0.288759, 0.527, 0.689, 0.340, 0.594, 0.595 }, // Et[ 50 -  80]
    {0.932139, 0.224329, 0.913220, 0.273356, 0.514, 0.671, 0.421, 0.589, 0.600 }, // Et[ 80 - 120]
  };
  //---- Tight efficiencies
  const float ElEff_Step1p6_mu200_Tight[nEtBins][netaBins] = {
    {0.502564, 0.0865130, 0.511916, 0.179267, 0.390, 0.494, 0.311, 0.343, 0.394 }, // Et[  7 -  15]
    {0.561509, 0.1045560, 0.549374, 0.172700, 0.476, 0.538, 0.298, 0.395, 0.470 }, // Et[ 15 -  20]
    {0.610466, 0.0885553, 0.575190, 0.182265, 0.497, 0.598, 0.301, 0.467, 0.495 }, // Et[ 20 -  30]
    {0.622935, 0.1266680, 0.625772, 0.221215, 0.478, 0.652, 0.311, 0.538, 0.556 }, // Et[ 30 -  50]
    {0.780432, 0.1690660, 0.752847, 0.250731, 0.527, 0.689, 0.340, 0.594, 0.595 }, // Et[ 50 -  80]
    {0.883690, 0.1974720, 0.850847, 0.242215, 0.514, 0.671, 0.421, 0.589, 0.600 }, // Et[ 80 - 120]
  };


  float *mapToUse = (float *) ElEff_Step1p6_mu200_Loose;

<<<<<<< HEAD
=======


>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  if(quality=="LHLoose") mapToUse = (float *) ElEff_Step1p6_mu200_Loose;
  else if(quality=="LHMedium") mapToUse = (float *) ElEff_Step1p6_mu200_Medium;
  else if(quality=="LHTight") mapToUse = (float *) ElEff_Step1p6_mu200_Tight;

  
  float efficiency = 1.;
  efficiency = interpolateElectronIDMap(mapToUse, ptGeV, eta);

  // HGTD hack to improve Electron ID/ISO efficiency
  // if (m_bUseHGTD0 || m_bUseHGTD1){
  //   if (fabs(eta) > 2.4 && fabs(eta) < 3.6)
  //     efficiency = 1.2*efficiency;
  // }



  if(efficiency>1.0) efficiency = 1.0;

  return efficiency;

}





//*********************************************************************
float TwoEgammaEfficiency::interpolateElectronIDMap(float *ptEtaMap,float pT,float eta)
//*********************************************************************
{
  //---- get efficiency value from the 2D maps: linear interpolation in pT and no interpolation in eta

  const int netaBins=9+1;
  const float etaBins[netaBins]={0.00,1.37,1.52,2.37,2.47,2.50,3.16,3.35,4.00,4.90};
  const int nEtBins=6+1;
  const float EtBins[nEtBins]={7.0,15.0,20.0,30.0,50.0,80.0,120.00};

  //---- if pt is out of range, use values estimated at the boundaries
  if(pT < 7) pT = 7.1;
  if(pT >=120) pT =119.; 
  if( fabs(eta)>4.9 ) return 0.;	//---- no electrons after |eta|=4.9


  //--- find associated bin eta
  int etaBin =0;
  for( int ietaBin = 0 ; ietaBin < netaBins ; ietaBin++ ){
    if( fabs(eta) < etaBins[ietaBin] ) {etaBin = ietaBin-1; break;}
  }


  //--- linear interpolation in pT
  int EtBin=0.;
  for(int ipT=0;ipT<nEtBins-1;ipT++){
    float binCenter=(EtBins[ipT+1]-EtBins[ipT])/2.+EtBins[ipT];
    //	printf("bin center =%f \n",binCenter);
    if(pT<=binCenter) {EtBin=ipT; break;}
  }
  if(EtBin==0) EtBin=1;
  if(pT>(EtBins[nEtBins-1]-EtBins[nEtBins -2])/2.+EtBins[nEtBins -2]) EtBin = nEtBins -2;

  float y0=ptEtaMap[etaBin+(EtBin-1)*(netaBins-1)];
  float y1=ptEtaMap[etaBin+EtBin*(netaBins-1)];
  float x0=(EtBins[EtBin]-EtBins[EtBin-1])/2.+EtBins[EtBin-1];
  float x1=(EtBins[EtBin+1]-EtBins[EtBin])/2.+EtBins[EtBin];
  
  float value = y0 +(y1-y0)/(x1-x0) * (pT-x0);
  //   printf("ID Interpolation: pt=%f eta=%f ptBin=%d etaBin=%d y0=%f y1=%f x0=%f x1=%f eff_int=%f \n",
  //   		pT, eta, EtBin,etaBin,y0,y1,x0,x1,value);

  return value;
}
