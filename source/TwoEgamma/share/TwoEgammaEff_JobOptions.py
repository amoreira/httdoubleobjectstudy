import AthenaPoolCnvSvc.ReadAthenaPool
<<<<<<< HEAD
from os import walk
=======
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
MessageSvc.defaultLimit = 9999999  # all messages 
algseq = CfgMgr.AthSequencer("AthAlgSeq", RequireOffline=9, Test=7) #gets the main AthSequencer
algseq += CfgMgr.TwoEgammaEfficiency()
svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["MYSTREAM DATAFILE='mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786.testFile.root' OPT='RECREATE'"]

algseq.TwoEgammaEfficiency.OfflinePtCut = 0000.
algseq.TwoEgammaEfficiency.OfflineTruthDrCut = 0.1
algseq.TwoEgammaEfficiency.TrackPtCut = 0.
algseq.TwoEgammaEfficiency.TrackConeDr = 0.1
algseq.TwoEgammaEfficiency.ElectronQuality = "LHLoose"
algseq.TwoEgammaEfficiency.OfflineOnly = False
<<<<<<< HEAD

LIMIT = 100
L2 = 0
f = []
test = 3 #Minus for background, and the rest for signal
mypath_sign = "/lstore/calo/aluisa/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9589/"
mypath_back = "/lstore/calo/aluisa/mc15_14TeV.147910.Pythia8_AU2CT10_jetjet_JZ0W.recon.AOD.e2403_s3142_s3143_r9589/"

#Additional files for pileup study
mypath_add = "/lstore/calo/aluisa/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r10023/" 

#Remove .part files of list
def rm_fileList( list ) :
  for w in list :
    if w[-5:] == '.part':
        list.remove(w)

#gets list of files to global variable f[]
def getList( dir ) :
  global f
  for (dirpath, dirnames, filenames) in walk(dir):
    f.extend(filenames)
    break
  
  #Add directory location
  f = [dir+s for s in f]
  
#Background
#All background data
if test == -3 or test == -4 :

  getList(mypath_back)

  if test == -3 :
    svcMgr.EventSelector.InputCollections = f ;
  else :
    svcMgr.EventSelector.InputCollections = f[L2:LIMIT] ;

#Initial data for background
elif test == -2 :

  f = ['AOD.11442181._004395.pool.root.1','AOD.11442181._005820.pool.root.1','AOD.11442181._006270.pool.root.1','AOD.11442181._009453.pool.root.1','AOD.11442181._010967.pool.root.1','AOD.11442181._011971.pool.root.1','AOD.11442181._012138.pool.root.1','AOD.11442181._012508.pool.root.1','AOD.11442181._013860.pool.root.1','AOD.11442181._015426.pool.root.1']        

  f = [mypath_back+s for s in f]
  svcMgr.EventSelector.InputCollections = f      

#Background single test
elif test == -1 :
  svcMgr.EventSelector.InputCollections = [mypath_back+'AOD.11442181._004395.pool.root.1']


#Signal
#Signal single test
elif test == 0 :
  svcMgr.EventSelector.InputCollections = [mypath_sign+'AOD.11442204._001064.pool.root.1']

#Inicial data for signal 
elif test == 1:
  
  f = ['AOD.11442204._001064.pool.root.1','AOD.11442204._001219.pool.root.1','AOD.11442204._003820.pool.root.1','AOD.11442204._006820.pool.root.1','AOD.11442204._007070.pool.root.1','AOD.11442204._009868.pool.root.1','AOD.11442204._010621.pool.root.1','AOD.11442204._010865.pool.root.1','AOD.11442204._011163.pool.root.1','AOD.11442204._011203.pool.root.1']
  
  f = [mypath_sign+s for s in f]
  svcMgr.EventSelector.InputCollections = f

#All signal data
elif test == 2 or test == 3 :
  
  getList(mypath_sign)  
  rm_fileList(f) #Removes files .part of f
  
  if test == 2 :
    svcMgr.EventSelector.InputCollections = f ;
  else :
    svcMgr.EventSelector.InputCollections = f[L2:LIMIT] ;

#Signal data with low pile-up
elif test == 4 :

  getList(mypath_add)
  svcMgr.EventSelector.InputCollections = f ;

else :
  print "No option avaiable"
=======
test = False

#if test:
#  svcMgr.EventSelector.InputCollections = ['/eos/user/l/lwilkins/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786//AOD.12482036._003464.pool.root.1',
#                                          '/eos/user/l/lwilkins/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786//AOD.12482036._000601.pool.root.1']
#else:

#  svcMgr.EventSelector.InputCollections = [ '/eos/user/l/lwilkins/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786//AOD.12482036._003464.pool.root.1',
#                                          '/eos/user/l/lwilkins/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786//AOD.12482036._000601.pool.root.1',
#                                          '/eos/user/l/lwilkins/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786//AOD.12482036._000638.pool.root.1',
#                                          '/eos/user/l/lwilkins/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786//AOD.12482036._000032.pool.root.1',
#                                          '/eos/user/l/lwilkins/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786//AOD.12482036._004104.pool.root.1',
#                                          '/eos/user/l/lwilkins/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786//AOD.12482036._003256.pool.root.1',
#                                          '/eos/user/l/lwilkins/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786//AOD.12482036._004007.pool.root.1',
#                                          '/eos/user/l/lwilkins/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786//AOD.11936924._000100.pool.root.1',
#                                          '/eos/user/l/lwilkins/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786//AOD.12482036._000564.pool.root.1']
if test:
  svcMgr.EventSelector.InputCollections = ['/lstore/calo/aluisa/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9589/AOD.11442204._001064.pool.root.1']
else:

  svcMgr.EventSelector.InputCollections = ['/lstore/calo/aluisa/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9589/AOD.11442204._001064.pool.root.1']





                          


             # '/afs/cern.ch/user/l/lwilkins/cernBox/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s2630_s2206_r7769/AOD.08047223._000329.pool.root.1',
                                          # '/afs/cern.ch/user/l/lwilkins/cernBox/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s2630_s2206_r7769/AOD.08047223._000445.pool.root.1',
                                          # '/afs/cern.ch/user/l/lwilkins/cernBox/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s2630_s2206_r7769/AOD.08047223._000482.pool.root.1',
                                          # '/afs/cern.ch/user/l/lwilkins/cernBox/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s2630_s2206_r7769/AOD.08047223._001002.pool.root.1',
                                          # '/afs/cern.ch/user/l/lwilkins/cernBox/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s2630_s2206_r7769/AOD.08047223._001117.pool.root.1',
                                          # '/afs/cern.ch/user/l/lwilkins/cernBox/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s2630_s2206_r7769/AOD.08047223._001218.pool.root.1',
                                          # '/afs/cern.ch/user/l/lwilkins/cernBox/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s2630_s2206_r7769/AOD.08047223._001239.pool.root.1',
                                          # '/afs/cern.ch/user/l/lwilkins/cernBox/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s2630_s2206_r7769/AOD.08047223._001357.pool.root.1',
                                          # '/afs/cern.ch/user/l/lwilkins/cernBox/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s2630_s2206_r7769/AOD.08047223._001435.pool.root.1',
                                          # '/afs/cern.ch/user/l/lwilkins/cernBox/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s2630_s2206_r7769/AOD.08047223._001563.pool.root.1',
                                          # '/afs/cern.ch/user/l/lwilkins/cernBox/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s2630_s2206_r7769/AOD.08047223._001962.pool.root.1',
                                          # '/afs/cern.ch/user/l/lwilkins/cernBox/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s2630_s2206_r7769/AOD.08047223._002209.pool.root.1',
                                          # '/afs/cern.ch/user/l/lwilkins/cernBox/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s2630_s2206_r7769/AOD.08047223._002361.pool.root.1',
                                          # '/afs/cern.ch/user/l/lwilkins/cernBox/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s2630_s2206_r7769/AOD.08047223._002414.pool.root.1',
                                          # '/afs/cern.ch/user/l/lwilkins/cernBox/Data/serviceTask/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s2630_s2206_r7769/AOD.08047223._002417.pool.root.1']
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
