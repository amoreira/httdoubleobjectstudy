#include "Event.h"


Event::Event(){
  m_leadingTruthPt=0;
  m_leadingTruthEta=0;
  m_leadingOfflinePt=0;
  m_leadingOfflineEta=0;
  //L0 efficiency wrt truth
  m_leadingL0TruthPt=0;
  //L0 efficiency wrt offline
  m_leadingL0OfflinePt=0;
  //Double electron histograms
  //Offline efficiency
  m_subleadingTruthPt=0;
  m_subleadingTruthEta=0;
  m_subleadingOfflinePt=0;
  m_subleadingOfflineEta=0;
  //L0 efficiency wrt truth
  m_subleadingL0TruthPt=0;
  //L0 efficiency wrt offline
  m_subleadingL0OfflinePt=0;
  m_subleadingL0OfflineEta=0;
  //L1 efficiency wrt L0(truth-matched) - leading pT
  m_leadingL1L0TruthPt=0;
  //L1 efficiency wrt L0(offline-matched) - leading pT
  m_leadingL1L0OfflinePt=0;
  //L1 efficiency wrt L0(truth-matched) - subleading pT
  m_subleadingL1L0TruthPt=0;
  //L1 efficiency wrt L0(offline-matched) - subleading pT
  m_subleadingL1L0OfflinePt=0;

}

Event::~Event(){}

void Event::reset(){
  m_leadingTruthPt=0;
  m_leadingTruthEta=0;
  m_leadingOfflinePt=0;
  m_leadingOfflineEta=0;
  //L0 efficiency wrt truth
  m_leadingL0TruthPt=0;
  //L0 efficiency wrt offline
  m_leadingL0OfflinePt=0;
  //Double electron histograms
  //Offline efficiency
  m_subleadingTruthPt=0;
  m_subleadingTruthEta=0;
  m_subleadingOfflinePt=0;
  m_subleadingOfflineEta=0;
  //L0 efficiency wrt truth
  m_subleadingL0TruthPt=0;
  //L0 efficiency wrt offline
  m_subleadingL0OfflinePt=0;
  m_subleadingL0OfflineEta=0;
  //L1 efficiency wrt L0(truth-matched) - leading pT
  m_leadingL1L0TruthPt=0;
  //L1 efficiency wrt L0(offline-matched) - leading pT
  m_leadingL1L0OfflinePt=0;
  //L1 efficiency wrt L0(truth-matched) - subleading pT
  m_subleadingL1L0TruthPt=0;
  //L1 efficiency wrt L0(offline-matched) - subleading pT
  m_subleadingL1L0OfflinePt=0;

}
