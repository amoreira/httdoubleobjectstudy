#ifndef LW_EVENT_H
#define LW_EVENT_H

#include "TObject.h"
#include "Rtypes.h"


class Event : public TObject{
 public:
  
  Event();
  virtual ~Event();
  void reset();
  float m_leadingTruthPt=0;
  float m_leadingTruthEta=0;
  float m_leadingOfflinePt=0;
  float m_leadingOfflineEta=0;
  //L0 efficiency wrt truth
  float m_leadingL0TruthPt=0;
  //L0 efficiency wrt offline
  float m_leadingL0OfflinePt=0;
  //Double electron histograms
  //Offline efficiency
  float m_subleadingTruthPt=0;
  float m_subleadingTruthEta=0;
  float m_subleadingOfflinePt=0;
  float m_subleadingOfflineEta=0;
  //L0 efficiency wrt truth
  float m_subleadingL0TruthPt=0;
  //L0 efficiency wrt offline
  float m_subleadingL0OfflinePt=0;
  float m_subleadingL0OfflineEta=0;
  //L1 efficiency wrt L0(truth-matched) - leading pT
  float m_leadingL1L0TruthPt=0;
  //L1 efficiency wrt L0(offline-matched) - leading pT
  float m_leadingL1L0OfflinePt=0;
  //L1 efficiency wrt L0(truth-matched) - subleading pT
  float m_subleadingL1L0TruthPt=0;
  //L1 efficiency wrt L0(offline-matched) - subleading pT
  float m_subleadingL1L0OfflinePt=0;
    
  ClassDef(Event,1);
  
};

#endif 