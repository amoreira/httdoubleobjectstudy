#include <GaudiKernel/DeclareFactoryEntries.h>

//#include <MyAnalysis/MyxAODAnalysis.h>
#include "../TwoEgammaEfficiency.h"

DECLARE_ALGORITHM_FACTORY (TwoEgammaEfficiency)

DECLARE_FACTORY_ENTRIES( TwoEgamma )
{
  DECLARE_ALGORITHM( TwoEgammaEfficiency );
}

