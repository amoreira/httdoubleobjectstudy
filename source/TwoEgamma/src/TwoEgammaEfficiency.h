#ifndef TWOEGAMMA_TWOEGAMMAEFFICIENCY_H
#define TWOEGAMMA_TWOEGAMMAEFFICIENCY_H 1

<<<<<<< HEAD
#include "../src/TTSmear.h"

=======
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
#include "AthenaBaseComps/AthAlgorithm.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTrigCalo/TrigEMClusterContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertexContainer.h"
#include "TRandom3.h"
#include "GaudiKernel/ToolHandle.h" //included under assumption you'll want to use some tools! Remove if you don't!
//#include "Event.h"

<<<<<<< HEAD
#define DEFAULT_PT -5
#define DEFAULT_ETA -3
#define DEFAULT_OTHER -2
#define SIGNAL_TEST 1

#define GET_DELTAR 0 //Or it will coonsume a lot of memory

#define PR(v) (v*v) 
=======
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905

class TTree;
class TH1F;
class TFile;
class TwoEgammaEvent;
class TRandom3;

<<<<<<< HEAD
typedef xAOD::TrackParticle::FourMom_t FourMom_t ; 

=======
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
struct truthOfflineContainer{
  const xAOD::TruthParticle * truth    = 0;
  const xAOD::Electron      * electron = 0;
  const xAOD::TrigEMCluster * cluster  = 0;
  const xAOD::TrackParticle * track    = 0;
<<<<<<< HEAD
  const xAOD::TrackParticle * smearedTrack_truth   = 0 ;
  const xAOD::TrackParticle * smearedTrack_offline = 0 ;
  const xAOD::TrackParticle * smearedTrack_truth_single   = 0 ;
  const xAOD::TrackParticle * smearedTrack_offline_single = 0 ;
=======
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  bool clusterMatchedEratioOffline     = false;
  bool clusterMatchedEratioTruth       = false;
  bool clusterMatchedEM10Offline       = false;
  bool clusterMatchedEM10Truth         = false;
  bool clusterMatchedEM20Offline       = false;
  bool clusterMatchedEM20Truth         = false;
  bool clusterMatched2EM10Offline      = false;
  bool clusterMatched2EM10Truth        = false;
  bool clusterMatched2EM20Offline      = false;
  bool clusterMatched2EM20Truth        = false;
  bool clusterMatchedRetaOffline       = false;
  bool clusterMatchedRetaTruth         = false;
  bool clusterMatchedEtOffline         = false;
  bool clusterMatchedEtTruth           = false;
  bool trackMatchedTruth               = false;
  bool trackMatchedOffline             = false;
  bool trackMatchedSingleTruth         = false;
  bool trackMatchedSingleOffline       = false;
  bool smearedTrackMatchedTruth        = false;
  bool smearedTrackMatchedOffline      = false;
<<<<<<< HEAD
  bool smearedTrackMatchedSingleTruth  = false;
  bool smearedTrackMatchedSingleOffline= false;

  float smearedTrackPt                 = 0. ;
  float smearedTrackEta                = 0. ;
  float smearedTrackPhi                = 0. ;
  float smearedTrackZ0                 = 0. ;

  inline void setCluster(const xAOD::TrigEMCluster * cl){cluster = cl;}

  //To avoid saving the already calculated values
  void getChangedCopy(truthOfflineContainer* c){
    track = c->track ;

    float trkPt = track->pt()*0.001;
    float trkEta = track->eta();
    float trkPhi = track->phi();
    float trkZ0  = track->z0();

    //Not the best way - but eh
    static TRandom3 R;
    R.SetSeed( (int)(track->pt() + 0.5) ); // in MeV
    double smearIPt = 0, smearEta = 0, smearPhi = 0, smearZ0 = 0;
    smearEta = R.Gaus( TTSmear::L1TTmean_reta_vs_eta(trkEta), TTSmear::L1TTsigma_reta(trkPt, trkEta) );
    smearPhi = R.Gaus( TTSmear::L1TTmean_rphi_vs_eta(trkEta), TTSmear::L1TTsigma_rphi(trkPt, trkEta) );
    smearIPt = R.Gaus( TTSmear::L1TTmean_ript_vs_eta(trkEta), TTSmear::L1TTsigma_ript(trkPt, trkEta) );
    smearZ0  = R.Gaus( TTSmear::L1TTmean_rzed_vs_eta(trkEta), TTSmear::L1TTsigma_rzed(trkPt, trkEta) );
      
    float smearedTrkEta = trkEta + smearEta;
    float smearedTrkPhi = trkPhi + smearPhi;
    float smearedTrkPt  = 1. / ( smearIPt + (1./trkPt) );
    float smearedTrkZ0  = trkZ0 + smearZ0;

    smearedTrackPt  = smearedTrkPt;
    smearedTrackEta = smearedTrkEta;
    smearedTrackPhi = smearedTrkPhi;
    smearedTrackZ0  = smearedTrkZ0;   
    
  }
};

=======
  bool smearedTrackMatchedSingleTruth         = false;
  bool smearedTrackMatchedSingleOffline       = false;

  float smearedTrackPt                 = 0;
  float smearedTrackEta                = 0;
  float smearedTrackPhi                = 0;
  float smearedTrackZ0                 = 0;
  inline void setCluster(const xAOD::TrigEMCluster * cl){cluster = cl;}
};


>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
class TwoEgammaEfficiency: public ::AthAlgorithm { 
 public: 
  TwoEgammaEfficiency( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~TwoEgammaEfficiency(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

  inline float deltaPhi1(const float& phi1, const float& phi2) { 
    float dphi=fabsf(phi1-phi2);
    dphi=fabsf(M_PI-dphi);
    dphi=fabsf(M_PI-dphi);
    return dphi;
  }

  inline float phiMpiPi(float x){
<<<<<<< HEAD
    if(fabsf(x)>10*M_PI) return x ;
=======
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
    while (x >= M_PI) x -= 2*M_PI;
    while (x < -M_PI) x += 2*M_PI;
    return x;
  }

  inline float deltaPhi2(const float& phi1, const float& phi2){
    return phiMpiPi(phi1-phi2);
  }

  inline float deltaR(const float& eta1, const float& eta2, const float& phi1, const float& phi2) { 
    float dphi=fabsf(phi1-phi2);
    dphi=fabsf(M_PI-dphi);
    dphi=fabsf(M_PI-dphi);
    
    float deta = fabsf(eta1-eta2);
    float dr = std::sqrt(pow(deta,2) + pow(dphi,2));
    return dr;
  }

  inline float deltaR2(const float& eta1, const float& eta2, const float& phi1, const float& phi2) { 
    float dphi=deltaPhi2(phi1,phi2);
    
    float deta = eta1-eta2;
    float dr = std::sqrt(pow(deta,2) + pow(dphi,2));
    return dr;
  }

  bool isFromZ(const xAOD::TruthParticle *truth);
  bool isParentFromZ(const xAOD::TruthParticle * particle);
  bool isLastChild(const xAOD::TruthParticle * particle);
  void resetVariables();
  void MatchCluster(truthOfflineContainer &tmo, std::vector<const xAOD::TrigEMCluster*> cluster, std::string trigger, bool verbose);
  float getElectronEfficiency(float ptMeV, float eta, std::string quality);
  float interpolateElectronIDMap(float *ptEtaMap,float pT,float eta); 

<<<<<<< HEAD
#if SIGNAL_TEST == 1
  //mass of boson
  float getZMassTrack( const truthOfflineContainer& c1 , const truthOfflineContainer& c2 );
  float getZMassTrackCluster( const truthOfflineContainer& c1 , const truthOfflineContainer& c2 );
  float getSmearedZMassTrack( truthOfflineContainer* c1 , truthOfflineContainer* c2 , std::string mode = "" );
  inline float getThetaFromEta( float eta ) { return 2*atan(exp(-eta)); }
#endif
  
  bool m_requireOffline;
  //float m_test;
=======

  bool m_requireOffline;
  float m_test;
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905

 private: 
 
  TRandom3 m_random;
  float m_offlinePtCut;
  float m_offlineTruthDrCut;
  float m_trackConeDr;
  float m_trackPtCut;
  bool m_offlineOnly;
  std::string m_electronQuality;
<<<<<<< HEAD
  //int m_fromZcount;
  //int m_electronQualityPass;
  //int m_nElectrons;

  float m_nEvents;
  float m_nPassingOffline;
  //float m_nPassingOfflineSingle;
  //float m_nPassing2L0;
  //float m_nPassing1L0;
  float m_nPassningL1;
  //float m_nPassningL1Z0_0p4;
  //float m_nPassningL1Z0_0p3;
  //float m_nPassningL1Z0_0p2;
  //float m_nPassningL1Z0_0p1;
=======
  int m_fromZcount;
  int m_electronQualityPass;
  int m_nElectrons;

  float m_dR1;
  float m_dR2;
  float m_dR3;
  float m_dZ;

  float m_nEvents;
  float m_nPassingOffline;
  float m_nPassingOfflineSingle;
  float m_nPassing2L0;
  float m_nPassing1L0;
  float m_nPassningL1;
  float m_nPassningL1Z0_0p4;
  float m_nPassningL1Z0_0p3;
  float m_nPassningL1Z0_0p2;
  float m_nPassningL1Z0_0p1;
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905

  float m_binning[21] = {0,5,10,15,20,25,30,35,40,50,60,70,80,90,100,120,140,160,170,180,200};
  float m_bins = 20;


  TTree * m_eventTree;
   
<<<<<<< HEAD
  //float m_trackEta;
  //float m_trackPhi;
  //float m_trackZ0;
  //float m_trackPt;
  //float m_diLepInvM;
  //float m_matchedOfflinePt;

  //float m_offlineEta;
  //float m_offlinePhi;
  //float m_offlinePt;
  //float m_offlineZ0;

  //float m_clusterEta;
  //float m_clusterPhi;
  //float m_clusterPt;
  //float m_clusterZ0; 

  float m_leadingTruthPt;
  float m_leadingTruthEta;
  float m_leadingOfflinePt;
  float m_leadingOfflineEta;
  float m_leadingOffline_offlinePt;
  float m_leadingOffline_offlineEta;
  //L0 efficiency wrt truth
  //float m_leadingL0Truth_truthPt=0;
  //float m_leadingL0Truth_truthEta=0;
  float m_leadingL0EM10Truth_truthPt;
  float m_leadingL0EM10Truth_truthEta;
  float m_leadingL0EM20Truth_truthPt;
  float m_leadingL0EM20Truth_truthEta;
  //float m_leadingL02EM10Truth_truthPt=0;
  //float m_leadingL02EM10Truth_truthEta=0;
  //L0 efficiency wrt offline
  //float m_leadingL0Offline_truthPt=0;
  //float m_leadingL0Offline_truthEta=0;
  //float m_leadingL0Offline_offlinePt=0;
  //float m_leadingL0Offline_offlineEta=0;
  float m_leadingL0EM10Offline_truthPt;
  float m_leadingL0EM10Offline_truthEta;
  float m_leadingL0EM20Offline_truthPt;
  float m_leadingL0EM20Offline_truthEta;
  //float m_leadingL02EM10Offline_truthPt=0;
  //float m_leadingL02EM10Offline_truthEta=0;
  float m_leadingL0EM10Offline_offlinePt;
  float m_leadingL0EM10Offline_offlineEta;
  float m_leadingL0EM20Offline_offlinePt;
  float m_leadingL0EM20Offline_offlineEta;
  //float m_leadingL02EM10Offline_offlinePt=0;
  //float m_leadingL02EM10Offline_offlineEta=0;
=======
  float m_trackEta;
  float m_trackPhi;
  float m_trackZ0;
  float m_trackPt;
  float m_diLepInvM;
  float m_matchedOfflinePt;

  //float m_offlineEta;
  float m_offlinePhi;
  //float m_offlinePt;
  float m_offlineZ0;

  float m_clusterEta;
  float m_clusterPhi;
  float m_clusterPt;
  float m_clusterZ0;


  float m_minDr_eta;
  float m_minDr_phi;
  float m_minDr_pt;
 

  float m_leadingTruthPt=0;
  float m_leadingTruthEta=0;
  float m_leadingOfflinePt=0;
  float m_leadingOfflineEta=0;
  float m_leadingOffline_offlinePt=0;
  float m_leadingOffline_offlineEta=0;
  //L0 efficiency wrt truth
  float m_leadingL0Truth_truthPt=0;
  float m_leadingL0Truth_truthEta=0;
  float m_leadingL0EM10Truth_truthPt=0;
  float m_leadingL0EM10Truth_truthEta=0;
  float m_leadingL0EM20Truth_truthPt=0;
  float m_leadingL0EM20Truth_truthEta=0;
  float m_leadingL02EM10Truth_truthPt=0;
  float m_leadingL02EM10Truth_truthEta=0;
  //L0 efficiency wrt offline
  float m_leadingL0Offline_truthPt=0;
  float m_leadingL0Offline_truthEta=0;
  float m_leadingL0Offline_offlinePt=0;
  float m_leadingL0Offline_offlineEta=0;
  float m_leadingL0EM10Offline_truthPt=0;
  float m_leadingL0EM10Offline_truthEta=0;
  float m_leadingL0EM20Offline_truthPt=0;
  float m_leadingL0EM20Offline_truthEta=0;
  float m_leadingL02EM10Offline_truthPt=0;
  float m_leadingL02EM10Offline_truthEta=0;
  float m_leadingL0EM10Offline_offlinePt=0;
  float m_leadingL0EM10Offline_offlineEta=0;
  float m_leadingL0EM20Offline_offlinePt=0;
  float m_leadingL0EM20Offline_offlineEta=0;
  float m_leadingL02EM10Offline_offlinePt=0;
  float m_leadingL02EM10Offline_offlineEta=0;
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905

  //Double electron histograms

  //Offline efficiency
<<<<<<< HEAD
  float m_subleadingTruthPt;
  float m_subleadingTruthEta;
  float m_subleadingOfflinePt;
  float m_subleadingOfflineEta;
  float m_subleadingOffline_offlinePt;
  float m_subleadingOffline_offlineEta;
  //L0 efficiency wrt truth
  //float m_subleadingL0Truth_truthPt=0;
  //float m_subleadingL0Truth_truthEta=0;
  //float m_subleadingL0EM10Truth_truthPt=0;
  //float m_subleadingL0EM10Truth_truthEta=0;
  //float m_subleadingL0EM20Truth_truthPt=0;
  //float m_subleadingL0EM20Truth_truthEta=0;
  float m_subleadingL02EM10Truth_truthPt;
  float m_subleadingL02EM10Truth_truthEta;
  float m_subleadingL02EM20Truth_truthPt;
  float m_subleadingL02EM20Truth_truthEta;

  //L0 efficiency wrt offline
  //float m_subleadingL0Offline_truthPt=0;
  //float m_subleadingL0Offline_truthEta=0;
  //float m_subleadingL0Offline_offlinePt=0;
  //float m_subleadingL0Offline_offlineEta=0;
  float m_subleadingL0EM10Offline_truthPt;
  float m_subleadingL0EM10Offline_truthEta;
  //float m_subleadingL0EM20Offline_truthPt=0;
  //float m_subleadingL0EM20Offline_truthEta=0;
  float m_subleadingL02EM10Offline_truthPt;
  float m_subleadingL02EM10Offline_truthEta;
  float m_subleadingL02EM20Offline_truthPt;
  float m_subleadingL02EM20Offline_truthEta;
  //float m_subleadingL0EM10Offline_offlineEta=0;
  //float m_subleadingL0EM20Offline_offlinePt=0;
  //float m_subleadingL0EM20Offline_offlineEta=0;
  float m_subleadingL02EM10Offline_offlinePt;
  float m_subleadingL02EM10Offline_offlineEta;
  float m_subleadingL02EM20Offline_offlinePt;
  float m_subleadingL02EM20Offline_offlineEta;
=======
  float m_subleadingTruthPt=0;
  float m_subleadingTruthEta=0;
  float m_subleadingOfflinePt=0;
  float m_subleadingOfflineEta=0;
  float m_subleadingOffline_offlinePt=0;
  float m_subleadingOffline_offlineEta=0;
  //L0 efficiency wrt truth
  float m_subleadingL0Truth_truthPt=0;
  float m_subleadingL0Truth_truthEta=0;
  float m_subleadingL0EM10Truth_truthPt=0;
  float m_subleadingL0EM10Truth_truthEta=0;
  float m_subleadingL0EM20Truth_truthPt=0;
  float m_subleadingL0EM20Truth_truthEta=0;
  float m_subleadingL02EM10Truth_truthPt=0;
  float m_subleadingL02EM10Truth_truthEta=0;
  float m_subleadingL02EM20Truth_truthPt=0;
  float m_subleadingL02EM20Truth_truthEta=0;

  //L0 efficiency wrt offline
  float m_subleadingL0Offline_truthPt=0;
  float m_subleadingL0Offline_truthEta=0;
  float m_subleadingL0Offline_offlinePt=0;
  float m_subleadingL0Offline_offlineEta=0;
  float m_subleadingL0EM10Offline_truthPt=0;
  float m_subleadingL0EM10Offline_truthEta=0;
  float m_subleadingL0EM20Offline_truthPt=0;
  float m_subleadingL0EM20Offline_truthEta=0;
  float m_subleadingL02EM10Offline_truthPt=0;
  float m_subleadingL02EM10Offline_truthEta=0;
  float m_subleadingL02EM20Offline_truthPt=0;
  float m_subleadingL02EM20Offline_truthEta=0;
  float m_subleadingL0EM10Offline_offlineEta=0;
  float m_subleadingL0EM20Offline_offlinePt=0;
  float m_subleadingL0EM20Offline_offlineEta=0;
  float m_subleadingL02EM10Offline_offlinePt=0;
  float m_subleadingL02EM10Offline_offlineEta=0;
  float m_subleadingL02EM20Offline_offlinePt=0;
  float m_subleadingL02EM20Offline_offlineEta=0;
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  std::vector<float> m_L02EM10Offline_offlinePt;
  std::vector<float> m_L02EM10Offline_offlineEta;

  std::vector<float> m_L1L0Offline_offlinePt;
  std::vector<float> m_L1L0Offline_offlineEta;
  std::vector<float> m_L1L0Offline1_offlinePt;
  std::vector<float> m_L1L0Offline1_offlineEta;
  std::vector<float> m_L1L0Offline2_offlinePt;
  std::vector<float> m_L1L0Offline2_offlineEta;
  std::vector<float> m_L1L0Offline4_offlinePt;
  std::vector<float> m_L1L0Offline4_offlineEta;
<<<<<<< HEAD
  std::vector<float> m_L1L0Offlinesmeared_offlinePt;
  std::vector<float> m_L1L0Offlinesmeared_offlineEta;
  std::vector<float> m_L1L0Offline1smeared_offlinePt;
  std::vector<float> m_L1L0Offline1smeared_offlineEta;
  std::vector<float> m_L1L0Offline2smeared_offlinePt;
  std::vector<float> m_L1L0Offline2smeared_offlineEta;
=======
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  std::vector<float> m_L1L0Offline4smeared_offlinePt;
  std::vector<float> m_L1L0Offline4smeared_offlineEta;




  std::vector<float> m_L02EM10Truth_truthPt;
  std::vector<float> m_L02EM10Truth_truthEta;

  std::vector<float> m_L1L0Truth_truthPt;
  std::vector<float> m_L1L0Truth_truthEta;
  std::vector<float> m_L1L0Truth1_truthPt;
  std::vector<float> m_L1L0Truth1_truthEta;
  std::vector<float> m_L1L0Truth2_truthPt;
  std::vector<float> m_L1L0Truth2_truthEta;
  std::vector<float> m_L1L0Truth4_truthPt;
  std::vector<float> m_L1L0Truth4_truthEta;
<<<<<<< HEAD
  std::vector<float> m_L1L0Truthsmeared_truthPt;
  std::vector<float> m_L1L0Truthsmeared_truthEta;
  std::vector<float> m_L1L0Truth1smeared_truthPt;
  std::vector<float> m_L1L0Truth1smeared_truthEta;
  std::vector<float> m_L1L0Truth2smeared_truthPt;
  std::vector<float> m_L1L0Truth2smeared_truthEta;
=======
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
  std::vector<float> m_L1L0Truth4smeared_truthPt;
  std::vector<float> m_L1L0Truth4smeared_truthEta;

  //L1 efficiency wrt L0(truth-matched) - leading pT
<<<<<<< HEAD
  float m_leadingL1L0Truth_truthPt;
  float m_leadingL1L0Truth_truthEta;
  float m_leadingL1L0Truth_trackPt;
  float m_leadingL1L0Truth_trackEta;


  //float m_leadingL1L0Truth1_trackPt=0;
  //float m_leadingL1L0Truth1_trackEta=0;
  //float m_leadingL1L0Truth2_trackPt=0;
  //float m_leadingL1L0Truth2_trackEta=0;
  //float m_leadingL1L0Truth4_trackPt=0;
  //float m_leadingL1L0Truth4_trackEta=0;

  //float m_leadingL1L0Truth1_truthPt=0;
  //float m_leadingL1L0Truth1_truthEta=0;
  //float m_leadingL1L0Truth2_truthPt=0;
  //float m_leadingL1L0Truth2_truthEta=0;
  //float m_leadingL1L0Truth4_truthPt=0;
  //float m_leadingL1L0Truth4_truthEta=0;

  //float m_leadingL1L0TruthSmeared4_trackPt=0;
  //float m_leadingL1L0TruthSeared4_trackEta=0;

  float m_leadingL1L0TruthSmeared4_truthPt;
  float m_leadingL1L0TruthSmeared4_truthEta;

  //L1 efficiency wrt L0(offline-matched) - leading pT
  float m_leadingL1L0Offline_truthPt;
  float m_leadingL1L0Offline_truthEta;
  float m_leadingL1L0Offline_offlinePt;
  float m_leadingL1L0Offline_offlineEta;
  float m_leadingL1L0Offline_trackPt;
  float m_leadingL1L0Offline_trackEta;

  //float m_leadingL1L0Offline1_trackPt=0;
  //float m_leadingL1L0Offline1_trackEta=0;
  //float m_leadingL1L0Offline2_trackPt=0;
  //float m_leadingL1L0Offline2_trackEta=0;
  //float m_leadingL1L0Offline4_trackPt=0;
  //float m_leadingL1L0Offline4_trackEta=0;

  //float m_leadingL1L0Offline1_truthPt=0;
  //float m_leadingL1L0Offline1_truthEta=0;
  //float m_leadingL1L0Offline2_truthPt=0;
  //float m_leadingL1L0Offline2_truthEta=0;
  //float m_leadingL1L0Offline4_truthPt=0;
  //float m_leadingL1L0Offline4_truthEta=0;

  //float m_leadingL1L0OfflineSmeared4_trackPt=0;
  //float m_leadingL1L0OfflineSeared4_trackEta=0;

  //float m_leadingL1L0OfflineSmeared4_truthPt=0;
  //float m_leadingL1L0OfflineSmeared4_truthEta=0;

  //L1 efficiency wrt L0(truth-matched) - subleading pT
  float m_subleadingL1L0Truth_truthPt;
  float m_subleadingL1L0Truth_truthEta;
  float m_subleadingL1L0Truth_trackPt;
  float m_subleadingL1L0Truth_trackEta;
  //L1 efficiency wrt L0(offline-matched) - subleading pT
  float m_subleadingL1L0Offline_truthPt;
  float m_subleadingL1L0Offline_truthEta;
  float m_subleadingL1L0Offline_offlinePt;
  float m_subleadingL1L0Offline_offlineEta;
  float m_subleadingL1L0Offline_trackPt;
  float m_subleadingL1L0Offline_trackEta;
=======
  float m_leadingL1L0Truth_truthPt=0;
  float m_leadingL1L0Truth_truthEta=0;
  float m_leadingL1L0Truth_trackPt=0;
  float m_leadingL1L0Truth_trackEta=0;


  float m_leadingL1L0Truth1_trackPt=0;
  float m_leadingL1L0Truth1_trackEta=0;
  float m_leadingL1L0Truth2_trackPt=0;
  float m_leadingL1L0Truth2_trackEta=0;
  float m_leadingL1L0Truth4_trackPt=0;
  float m_leadingL1L0Truth4_trackEta=0;

  float m_leadingL1L0Truth1_truthPt=0;
  float m_leadingL1L0Truth1_truthEta=0;
  float m_leadingL1L0Truth2_truthPt=0;
  float m_leadingL1L0Truth2_truthEta=0;
  float m_leadingL1L0Truth4_truthPt=0;
  float m_leadingL1L0Truth4_truthEta=0;

  float m_leadingL1L0TruthSmeared4_trackPt=0;
  float m_leadingL1L0TruthSeared4_trackEta=0;

  float m_leadingL1L0TruthSmeared4_truthPt=0;
  float m_leadingL1L0TruthSmeared4_truthEta=0;

  //L1 efficiency wrt L0(offline-matched) - leading pT
  float m_leadingL1L0Offline_truthPt=0;
  float m_leadingL1L0Offline_truthEta=0;
  float m_leadingL1L0Offline_offlinePt=0;
  float m_leadingL1L0Offline_offlineEta=0;
  float m_leadingL1L0Offline_trackPt=0;
  float m_leadingL1L0Offline_trackEta=0;

  float m_leadingL1L0Offline1_trackPt=0;
  float m_leadingL1L0Offline1_trackEta=0;
  float m_leadingL1L0Offline2_trackPt=0;
  float m_leadingL1L0Offline2_trackEta=0;
  float m_leadingL1L0Offline4_trackPt=0;
  float m_leadingL1L0Offline4_trackEta=0;

  float m_leadingL1L0Offline1_truthPt=0;
  float m_leadingL1L0Offline1_truthEta=0;
  float m_leadingL1L0Offline2_truthPt=0;
  float m_leadingL1L0Offline2_truthEta=0;
  float m_leadingL1L0Offline4_truthPt=0;
  float m_leadingL1L0Offline4_truthEta=0;

  float m_leadingL1L0OfflineSmeared4_trackPt=0;
  float m_leadingL1L0OfflineSeared4_trackEta=0;

  float m_leadingL1L0OfflineSmeared4_truthPt=0;
  float m_leadingL1L0OfflineSmeared4_truthEta=0;

  //L1 efficiency wrt L0(truth-matched) - subleading pT
  float m_subleadingL1L0Truth_truthPt=0;
  float m_subleadingL1L0Truth_truthEta=0;
  float m_subleadingL1L0Truth_trackPt=0;
  float m_subleadingL1L0Truth_trackEta=0;
  //L1 efficiency wrt L0(offline-matched) - subleading pT
  float m_subleadingL1L0Offline_truthPt=0;
  float m_subleadingL1L0Offline_truthEta=0;
  float m_subleadingL1L0Offline_offlinePt=0;
  float m_subleadingL1L0Offline_offlineEta=0;
  float m_subleadingL1L0Offline_trackPt=0;
  float m_subleadingL1L0Offline_trackEta=0;
>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905

  // std::vector<float> m_L1L0Offline_offlinePt;
  // std::vector<float> m_L1L0Offline_offlineEta;

<<<<<<< HEAD
  //float m_subleadingL1L0Offline1_trackPt=0;
  //float m_subleadingL1L0Offline1_trackEta=0;
  //float m_subleadingL1L0Offline2_trackPt=0;
  //float m_subleadingL1L0Offline2_trackEta=0;
  //float m_subleadingL1L0Offline4_trackPt=0;
  //float m_subleadingL1L0Offline4_trackEta=0;

  float m_subleadingL1L0Offline1_truthPt;
  float m_subleadingL1L0Offline1_truthEta;
  float m_subleadingL1L0Offline2_truthPt;
  float m_subleadingL1L0Offline2_truthEta;
  float m_subleadingL1L0Offline4_truthPt;
  float m_subleadingL1L0Offline4_truthEta;

  float m_subleadingL1L0Truth1_truthPt;
  float m_subleadingL1L0Truth1_truthEta;
  float m_subleadingL1L0Truth2_truthPt;
  float m_subleadingL1L0Truth2_truthEta;
  float m_subleadingL1L0Truth4_truthPt;
  float m_subleadingL1L0Truth4_truthEta;

  //float m_subleadingL1L0TruthSmeared4_trackPt=0;
  //float m_subleadingL1L0TruthSeared4_trackEta=0;

  //float m_subleadingL1L0TruthSmeared4_truthPt=0;
  //float m_subleadingL1L0TruthSmeared4_truthEta=0;

  
  //float m_subleadingL1L0OfflineSmeared4_trackPt=0;
  //float m_subleadingL1L0OfflineSeared4_trackEta=0;

  float m_subleadingL1L0OfflineSmeared_truthPt;
  float m_subleadingL1L0OfflineSmeared_truthEta;
  float m_subleadingL1L0OfflineSmeared1_truthPt;
  float m_subleadingL1L0OfflineSmeared1_truthEta;
  float m_subleadingL1L0OfflineSmeared2_truthPt;
  float m_subleadingL1L0OfflineSmeared2_truthEta;
  float m_subleadingL1L0OfflineSmeared4_truthPt;
  float m_subleadingL1L0OfflineSmeared4_truthEta;


  std::vector<float> m_trackClusterDeltaR;
  std::vector<float> m_trackClusterSmearedDeltaR;
  //std::vector<float> m_truthOfflineDeltaR;
  //std::vector<float> m_clusterOfflineDeltaR;
  //std::vector<float> m_clusterTruthDeltaR;
  float m_deltaZ;
  float m_deltaZ1;
  float m_deltaZ2;
  float m_deltaZ4;
  float m_smearedDeltaZ;
  float m_smearedDeltaZ1;
  float m_smearedDeltaZ2;
  float m_smearedDeltaZ4;

  float m_deltaZTruth;
  float m_deltaZ1Truth;
  float m_deltaZ2Truth;
  float m_deltaZ4Truth;
  float m_smearedDeltaZTruth;
  float m_smearedDeltaZ1Truth;
  float m_smearedDeltaZ2Truth;
  float m_smearedDeltaZ4Truth;

  float m_subleadingSingleTruthPt;

  //debug variables

  std::vector<float> m_truthPt;
  //std::vector<float> m_truthE;
  std::vector<float> m_truthEta;
  
  //std::vector<float> m_offlinePt;
  //std::vector<float> m_offlineE;
  //std::vector<float> m_offlineEta;
  
  std::vector<float> m_offlinePt_Loose;
  //std::vector<float> m_offlineE_Loose;
  std::vector<float> m_offlineEta_Loose;
  
  //std::vector<float> m_offlinePt_Medium;
  //std::vector<float> m_offlineE_Medium;
  //std::vector<float> m_offlineEta_Medium; 
  
  //std::vector<float> m_offlinePt_Tight;
  //std::vector<float> m_offlineE_Tight;
  //std::vector<float> m_offlineEta_Tight; 

  float m_dR_failing2EM10;
  float m_avIntPerCrossing; //Average interaction per crossing for all the BCID

#if SIGNAL_TEST == 1
  //Mass of Z Boson
  float m_ZMassTrack , m_ZMassTrackCluster ;
  float m_smearedZMassTrack_truth , m_smearedZMassTrack_offline , m_smearedZMassTrack_same ;
#endif 

  //Because is an important variable but consumes a lot of memory
#if GET_DELTAR == 1
  std::vector<float> m_dRTrackTruth ;
  std::vector<float> m_dRTrackCluster ;
#endif
  
  //dR to the closet track and cluster
  std::vector<float> m_dRMinTruthTrack ;
  std::vector<float> m_dRMinTrackCluster ;
  std::vector<float> m_dRMatchedTruthTrack ;
  std::vector<float> m_dRMatchedTrackCluster ;

  //dR for the smeared tracks
  std::vector<float> m_smearedDRMinTruthTrack_truth , m_smearedDRMinTruthTrack_offline , 
    m_smearedDRMinTruthTrack_truth_single , m_smearedDRMinTruthTrack_offline_single ;
  std::vector<float> m_smearedDRMatchedTruthTrack_truth , m_smearedDRMatchedTruthTrack_offline , 
    m_smearedDRMatchedTruthTrack_truth_single , m_smearedDRMatchedTruthTrack_offline_single ;

  //Last attempt for a happy ending 
  std::vector<float> m_Eta , m_Phi , m_Z0, m_Pt ;
  std::vector<float> m_smearedEta , m_smearedPhi , m_smearedZ0, m_smearedPt ;

  //Counting EM clusters
  int m_nCluster20 ;
  int m_nCluster10 ;
  int m_nCluster ;
  int m_nElectrons ;

  int m_nTruth = 0;
  int m_nReco = 0;
  int m_nTruthMatched ;
  int m_nClusterMatched ;
  int m_nClusterMatched20, m_nClusterMatched10 ;

  //Variables to check how many smeared tracks are equal to tracks
  int m_nEqualTruth = 0 , m_nEqualOffline = 0 ,
    m_nEqualTruthSingle = 0 , m_nEqualOfflineSingle = 0 , 
    m_nTotalTruth = 0 , m_nTotalOffline = 0 , 
    m_nTotalTruthSingle = 0 , m_nTotalOfflineSingle = 0 ;
=======
  float m_subleadingL1L0Offline1_trackPt=0;
  float m_subleadingL1L0Offline1_trackEta=0;
  float m_subleadingL1L0Offline2_trackPt=0;
  float m_subleadingL1L0Offline2_trackEta=0;
  float m_subleadingL1L0Offline4_trackPt=0;
  float m_subleadingL1L0Offline4_trackEta=0;

  float m_subleadingL1L0Offline1_truthPt=0;
  float m_subleadingL1L0Offline1_truthEta=0;
  float m_subleadingL1L0Offline2_truthPt=0;
  float m_subleadingL1L0Offline2_truthEta=0;
  float m_subleadingL1L0Offline4_truthPt=0;
  float m_subleadingL1L0Offline4_truthEta=0;

  float m_subleadingL1L0Truth1_truthPt=0;
  float m_subleadingL1L0Truth1_truthEta=0;
  float m_subleadingL1L0Truth2_truthPt=0;
  float m_subleadingL1L0Truth2_truthEta=0;
  float m_subleadingL1L0Truth4_truthPt=0;
  float m_subleadingL1L0Truth4_truthEta=0;

  float m_subleadingL1L0TruthSmeared4_trackPt=0;
  float m_subleadingL1L0TruthSeared4_trackEta=0;

  float m_subleadingL1L0TruthSmeared4_truthPt=0;
  float m_subleadingL1L0TruthSmeared4_truthEta=0;

  
  float m_subleadingL1L0OfflineSmeared4_trackPt=0;
  float m_subleadingL1L0OfflineSeared4_trackEta=0;

  float m_subleadingL1L0OfflineSmeared4_truthPt=0;
  float m_subleadingL1L0OfflineSmeared4_truthEta=0;


  std::vector<float> m_trackClusterDeltaR;
  std::vector<float> m_truthOfflineDeltaR;
  std::vector<float> m_clusterOfflineDeltaR;
  std::vector<float> m_clusterTruthDeltaR;
  float m_deltaZ=-1;
  float m_deltaZ1=-1;
  float m_deltaZ2=-1;
  float m_deltaZ4=-1;
  float m_smearedDeltaZ4=-1;

 
  float m_deltaZTruth=-1;
  float m_deltaZ1Truth=-1;
  float m_deltaZ2Truth=-1;
  float m_deltaZ4Truth=-1;
  float m_smearedDeltaZ4Truth=-1;

  float m_subleadingSingleTruthPt=-1;

 //debug variables

 std::vector<float> m_truthPt;
 std::vector<float> m_truthE;
 std::vector<float> m_truthEta;
 
 std::vector<float> m_offlinePt;
 std::vector<float> m_offlineE;
 std::vector<float> m_offlineEta;

 std::vector<float> m_offlinePt_Loose;
 std::vector<float> m_offlineE_Loose;
 std::vector<float> m_offlineEta_Loose;

 std::vector<float> m_offlinePt_Medium;
 std::vector<float> m_offlineE_Medium;
 std::vector<float> m_offlineEta_Medium; 

 std::vector<float> m_offlinePt_Tight;
 std::vector<float> m_offlineE_Tight;
 std::vector<float> m_offlineEta_Tight; 

 float m_dR_failing2EM10 = -1;

  float m_minDr_all = 0;
  float m_minDr_loose = 0;
  float m_minDr_pt_all = 0;
  float m_minDr_pt_loose = 0;

  float m_minDr_res_all = 0;
  float m_minDr_res_loose = 0;


  float m_matchedEvents;

  int m_nTruth = 0;
  int m_nReco = 0;



>>>>>>> b896a2d0948fce83a01d1b089ef0c5fc7f0df905
}; 

#endif //> !TWOEGAMMA_TWOEGAMMAEFFICIENCY_H
