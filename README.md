# HTT study on the vertex resolution on double electron tracks

## Get code, compile and run
These instructions are general. They should work out of the box on lxplus or any local system (like the Fermi machines at LIP)

**First time:**
- login into the fermi machines
- add the following lines to your .bashrc file:
    - export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    - alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
- setupATLAS
- git clone https://gitlab.cern.ch/amoreira/httdoubleobjectstudy.git
- cd httdoubleobjectdtudy
- mkdir build
- mkdir run
- cd build
- asetup 21.2,AthAnalysis,latest
- cmake ../source
- make
- source */setup.sh
- cd ../run
- athena.py TwoEgamma/TwoEgammaEff_JobOptions.py

**Following times:**
- login into the fermi machines
- setupATLAS
- cd httdoubleobjectstudy/build
- asetup --restore
- make
- cd ../run
- athena.py TwoEgamma/TwoEgammaEff_JobOptions.py

## (Very) Basic structure of the code

- **Main file**: httdoubleobjectstudy/source/TwoEgamma/Root/TwoEgammaEfficiency.cxx
    - Start by taking a look at this file
    - It has three main functions: initialize(), finalize() and execute()
    - As the name indicates, execute() is the function that actually takes the input sample and does stuff
- **Configuration file**: httdoubleobjectstudy/source/TwoEgamma/share/TwoEgammaEff_JobOptions.py
    - This file allows you to specify, for example, over which samples to run over

