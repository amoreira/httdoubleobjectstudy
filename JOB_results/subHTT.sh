#!/bin/bash
# --- Start Batch System Options ---
# Allow the taks to inherit the submitter environment
#...$ -V

#...$ -pe mcore 1
#...$ -l container=True
#...$ -v CONTAINER=CENTOS6
#...$ -l gpu,release=el7

#...$ -q lips

# Transfer input file (MyMacro.c) to the execution machine
#$ -v SGEIN1=/home/t3atlas/ev19u055/httdoubleobjectstudy/build:build
#$ -v SGEIN2=/home/t3atlas/ev19u055/httdoubleobjectstudy/run:run
#$ -v SGEIN3=/home/t3atlas/ev19u055/httdoubleobjectstudy/Results_Efficiency/firstPlots.cxx:Results_Efficiency/firstPlots.cxx
#$ -v SGEIN4=/home/t3atlas/ev19u055/httdoubleobjectstudy/Results_Pileup/pileup.cxx:Results_Pileup/pileup.cxx
#$ -v SGEIN5=/home/t3atlas/ev19u055/httdoubleobjectstudy/Results_ZMass/zmass.cxx:Results_ZMass/zmass.cxx
#$ -v SGEIN6=/home/t3atlas/ev19u055/httdoubleobjectstudy/Results_MoreGraphs/ncluster.cxx:Results_MoreGraphs/ncluster.cxx
#...$ -v SGEIN7=/home/t3atlas/ev19u055/httdoubleobjectstudy/Results_MoreGraphs/color2Dgraph.cxx:Results_MoreGraphs/color2Dgraph.cxx
#$ -v SGEOUT1=run/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786.testFile.root
#$ -v SGEOUT2=Results_Efficiency/
#$ -v SGEOUT3=Results_Pileup/
#$ -v SGEOUT4=Results_ZMass/
#$ -v SGEOUT5=Results_MoreGraphs/

#$ -e log_err

# Send me an e-mail when finished
#$ -m e
#$ -M filipe.d.cruz@tecnico.ulisboa.pt

# --- Commands ---
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

cd build
#setupATLAS
asetup --restore
source */setup.sh

cd ../run
athena.py TwoEgamma/TwoEgammaEff_JobOptions.py

#Get plots
cd ../Results_Efficiency
root -l -b -q firstPlots.cxx

cd ../Results_Pileup
root -l -b -q pileup.cxx

cd ../Results_ZMass
root -l -b -q zmass.cxx

cd ../Results_MoreGraphs
#root -l -b -q color2Dgraph.cxx
root -l -b -q ncluster.cxx

exit 0
