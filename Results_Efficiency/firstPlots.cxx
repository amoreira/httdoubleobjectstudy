#define JOB_RES 0
#define LOG_SCALE 0

#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TImage.h"
#include "TGraph.h"
#include "TLegend.h"

#include <string>
#include <iostream>

#if JOB_RES == 1
const char file[] = "../JOB_results/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786.testFile.root" ;
#elif JOB_RES == -1
const char file[] = "../mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786.testFile.root" ;
#else
const char file[] = "../run/mc15_14TeV.147806.PowhegPythia8_AU2CT10_Zee.recon.AOD.e1564_s3142_s3143_r9786.testFile.root" ;
#endif

//Separates window range of calculation range
const double range[2] = {0,2.} ; //Visualization range
const double total_range[2] = {0.,10.}; //Valid data range
const double dx = 0.1 ; //Size of each bin

const int NBINS = (int) (total_range[1]-total_range[0])/dx ;
const int NBinMin = (int)(range[0]-total_range[0])/dx + 1 , NBinMax = NBINS-(int)(total_range[1]-range[1])/dx;
const int NRANGEBINS = NBinMax - NBinMin + 1 ;

const char name_change[3] = {'1','2','4'} ;
std::string name[2]={"deltaZ0","smearedDeltaZ0"};
std::string name_added[2] = {"","Truth"};
int pos[3] = {6,13,17} ;

/*void print (double * x, int N = NRANGEBINS){
  for(int i = 0 ; i < N ; i++ ) std::cout << i << '-' << x[i] <<std::endl;
  }*/

//Change points
inline void change_points(double* rd,double* nd, int N = NRANGEBINS){
  for (int i = 0 ; i < N ; i++ ) rd[i] = nd[i] ;
}

//calulates efficiency points - moved
inline void calc_effi(double* y1 , double *y2, TH1F *h1, TH1F *h2 ){
  double integral[2] ;

  integral[0] = h1->Integral(1,NBINS) ;
  integral[1] = h2->Integral(1,NBINS) ;
  
  for( int k = NBinMin ; k <= NBinMax ; k++ ){ //Points for the efficiency graphs
    y1[k-NBinMin] = h1->Integral(NBinMin,k)/integral[0];
    y2[k-NBinMin] = h2->Integral(NBinMin,k)/integral[1];
  }
}

//Create a plot of a given par of histograms - moved
void create_hist(std::string* aux, TH1F *h1, TTree *tree , TLegend *leg  , TCanvas* c1 ){
  h1->SetTitle(aux[4].c_str());
#if LOG_SCALE == 1
  gPad->SetLogy();
#endif
  tree->Draw(aux[0].c_str());
  tree->Draw(aux[1].c_str(),"","SAME");
  leg->Draw();
  c1->SaveAs(aux[2].c_str());
}

//Create graphs - moved
void create_graphs(std::string* aux , TGraph *g1 , TGraph *g2, TLegend *leg , TCanvas *c1 , double *y1 , double *y2 ){
  g1->SetTitle(string("Efficiency-"+aux[4]).c_str());
#if LOG_SCALE == 1
  gPad->SetLogy(0);
#endif
  change_points(g1->GetY(),y1,NRANGEBINS);
  change_points(g2->GetY(),y2,NRANGEBINS);
  g1->Draw("AC*");
  g2->Draw("C*");
  leg->Draw();
  c1->SaveAs(aux[3].c_str());
}

//Main function
void firstPlots(){
  std::string aux[5] ;
  double y1[NRANGEBINS] , y2[NRANGEBINS], x[NRANGEBINS];
  int i, j ;

  //Canvas for plot display
  TCanvas *c1 = new TCanvas("c1","DeltaZ Graphs",800,500);
  
  //Where is the data
  TFile *_file0 = TFile::Open(file);
  if (!_file0) return ;
  
  //Tree in TFile
  TTree *tree = (TTree*) _file0->Get("eventTree") ;
  if (!tree) return ;
  
  //Histograms
  TH1F *h1 = new TH1F("h1","",NBINS,total_range[0],total_range[1]) ;
  TH1F *h2 = new TH1F("h2","",NBINS,total_range[0],total_range[1]) ;
  h1->SetLineColor(kBlue);
  h2->SetLineColor(kRed);
  h1->SetXTitle("#Delta z (mm)");
  h1->SetYTitle("Counts");
  h1->GetXaxis()->SetRangeUser(range[0],range[1]);
  h1->SetStats(kFALSE);

  //For the efficiency plots
  TGraph *g1 =new TGraph(NRANGEBINS),  *g2 =new TGraph(NRANGEBINS);
  g1->SetLineColor(kBlue);
  g2->SetLineColor(kRed);
  g1->GetXaxis()->SetTitle("#Delta z (mm)");
  g1->GetYaxis()->SetTitle("Efficiency");
  g1->GetYaxis()->SetRangeUser(0.,1.1);
  g1->GetXaxis()->SetLimits(range[0],range[1]);
  
  //Create legend 
  TLegend *leg = new TLegend(0.75,0.7,0.98,0.45);
  leg->SetHeader("Legend","C");
  leg->AddEntry(h1,"Tracks","l");
  leg->AddEntry(h2,"Smeared Tracks","l");

  //Obtain x values for the efficiency plot
  for ( i = 1 , x[0] = range[0]+dx/2.; i < NRANGEBINS ; i++ ) x[i] = x[i-1] + dx ;
  
  //Change x values of graphs
  change_points(g1->GetX(),x,NRANGEBINS);
  change_points(g2->GetX(),x,NRANGEBINS);
  
  //Create histograms in function of delta z
  for( j = 0 ; j < 2 ; j++ ){
    aux[4] = name[0]+name_added[j];
    aux[0] = aux[4]+">>h1" ;
    aux[1] = name[1]+name_added[j]+">>h2" ;
    aux[2] = aux[4]+".png" ;
    aux[3] = "Efficiency-"+aux[2];
  
    for ( i = 0 ; i < 3 ; i++ ){
      aux[4][pos[0]] = name_change[i] ;
      aux[0][pos[0]] = name_change[i] ;
      aux[1][pos[1]] = name_change[i] ;
      aux[2][pos[0]] = name_change[i] ;
      aux[3][pos[2]] = name_change[i] ;

      //Create histograms with counts vs delta z
      create_hist(aux,h1,tree,leg,c1);
      
      //Calculates efficiency points
      calc_effi(y1,y2,h1,h2) ;

      //Create efficiency graphs
      create_graphs(aux,g1,g2,leg,c1,y1,y2);

    }
    
    //For the geral case
    aux[0].erase(pos[0],1);
    aux[1].erase(pos[1],1);
    aux[2].erase(pos[0],1);
    aux[4].erase(pos[0],1);
    aux[3].erase(pos[2],1);

    create_hist(aux,h1,tree,leg,c1);
    calc_effi(y1,y2,h1,h2) ;
    create_graphs(aux,g1,g2,leg,c1,y1,y2);
  }
  
  delete h1 ;
  delete h2 ;
  delete tree ;
  delete g1 ;
  delete g2 ; //Get efficiency plots
  _file0->Close("R");
  delete c1 ;
}
